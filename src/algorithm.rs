use crate::asn1::encode::EncodeTo;
use crate::asn1::encode;
use crate::asn1::decode::DecodeFrom;
use crate::x509_error::X509Error;
use crate::asn1::decode;
use std::fmt;
use json::error::{JSONError, JSONErrorKind};
use json::JSON;
use crate::ecdsa::named_curve::NamedCurve;
use crate::oid::ObjectIdentifier;
use crate::oid;

#[derive(Debug)]
pub struct AlgorithmIdentifier {
    pub algorithm_id: ObjectIdentifier,
    pub parameters: AlgorithmParameters,
}

impl EncodeTo for AlgorithmIdentifier {
    fn encode(&self) -> Box<[u8]> {
        let mut result = encode::encode_object_identifier(&self.algorithm_id).into_vec();
        let parameters = match &self.parameters {
            AlgorithmParameters::Ecdsa(curve) => curve.encode(),
            AlgorithmParameters::None => encode::encode_null(),
        };
        result.extend_from_slice(&parameters);
        encode::encode_sequence(&result)
    }
}

impl DecodeFrom for AlgorithmIdentifier {
    fn decode(bytes: &[u8]) -> Result<(AlgorithmIdentifier, usize), X509Error> {
        let (bytes, total_encoded_size) = decode::uncover_sequence(bytes)?;
        let (algorithm_id, _, bytes) = decode::decode_object_identifier(bytes)?;

        let parameters = match &algorithm_id as &[u64] {
            oid::ELLIPTIC_CURVE_PUBLIC_KEY => {
                let curve = NamedCurve::decode(bytes)?.0;
                AlgorithmParameters::Ecdsa(curve)
            },
            _ => AlgorithmParameters::None,
        };

        Ok((AlgorithmIdentifier {
            algorithm_id,
            parameters,
        }, total_encoded_size))
    }
}

impl fmt::Display for AlgorithmIdentifier {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let width = f.width().unwrap_or_default();
        let id = oid::oid_to_human_readable(&self.algorithm_id);

        f.write_str(&format!("{}Algorithm: {}\n", "\t".repeat(width), id))?;
        write!(f, "{:<1}", self.parameters)
    }
}

impl TryFrom<JSON> for AlgorithmIdentifier {
    type Error = JSONError;

    fn try_from(value: JSON) -> Result<Self, Self::Error> {
        let mut props = value.resolve_into_object()?;
        let algorithm_id = props.remove( "algorithm").ok_or(JSONErrorKind::Resolve)?;
        let algorithm_id = oid::from_json(algorithm_id)?;
        let parameters = match &algorithm_id as &[u64] {
            oid::ELLIPTIC_CURVE_PUBLIC_KEY => {
                let curve_oid = oid::from_json(props.remove( "parameters").ok_or(JSONErrorKind::Resolve)?)?;
                let curve = NamedCurve::from_oid(&curve_oid).map_err(|_|JSONErrorKind::Resolve)?;
                AlgorithmParameters::Ecdsa(curve)
            },
            _ => AlgorithmParameters::None,
        };
        Ok(AlgorithmIdentifier{
            algorithm_id,
            parameters,
        })
    }
}

#[derive(Debug, PartialEq)]
pub enum AlgorithmParameters {
    None,
    Ecdsa(NamedCurve),
//    RsaSsaPss { hash_algorithm: Box<AlgorithmIdentifier>, mask_gen_algorithm: Box<AlgorithmIdentifier>, salt_length: u8, trailer_field: u8 }
}

impl fmt::Display for AlgorithmParameters {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let tab = "\t".repeat(f.width().unwrap_or_default());

        if self == &AlgorithmParameters::None {
            return Ok(())
        }

        write!(f, "{}Parameters: ", tab)?;

        match self {
            AlgorithmParameters::None => Ok(()),
            AlgorithmParameters::Ecdsa(curve) => write!(f, "Elliptic curve {:?}\n", curve),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_from_json() {
        let text = format!("{{\"algorithm\":\"{}\"}}", oid::SHA256_WITH_RSA_ENCRYPTION_HUMAN);
        let result:AlgorithmIdentifier = JSON::parse(&text).unwrap().try_into().unwrap();
        assert_eq!(&result.algorithm_id as &[u64], oid::SHA256_WITH_RSA_ENCRYPTION);
    }
}