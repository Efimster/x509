use crate::tbs_certificate::TbsCertificate;
use crate::algorithm::AlgorithmIdentifier;
use crate::asn1::encode::EncodeTo;
use crate::asn1::encode;
use crate::asn1::decode::DecodeFrom;
use crate::asn1::decode;
use crate::x509_error::*;
use std::fmt;
use json::error::JSONError;
use json::JSON;
use crate::signature::Signature;
use crate::ecdsa::ecdsa_signature::EcdsaSignature;

#[derive(Debug)]
pub struct Certificate {
    pub tbs_certificate: TbsCertificate,
    pub signature_algorithm: AlgorithmIdentifier,
    pub signature: Signature,
}

impl EncodeTo for Certificate {
    fn encode(&self) -> Box<[u8]> {
        let mut result = self.tbs_certificate.encode().into_vec();
        self.signature_algorithm.encode_to(&mut result);
        let bit_string = match &self.signature {
            Signature::ECDSA(ecdsa_signature) =>  encode::encode_bit_string(&ecdsa_signature.encode()),
            Signature::RSA(rsa_signature) => encode::encode_bit_string(&rsa_signature),
            Signature::None => Box::new([]),
        };

        result.extend_from_slice(&bit_string);
        encode::encode_sequence(&result)
    }
}

impl DecodeFrom for Certificate {
    fn decode(bytes:&[u8]) -> Result<(Certificate, usize), X509Error> {
        let (bytes, certificate_encoded_size) = decode::uncover_sequence(bytes)?;
        let (tbs_certificate, bytes) = TbsCertificate::decode_cut(bytes)?;
        let (signature_algorithm, bytes) = AlgorithmIdentifier::decode_cut(bytes)?;
        let (signature_bytes, _, _) = decode::decode_bit_string(bytes)?;
        let signature_value = match &signature_algorithm.algorithm_id as &[u64] {
            [1, 2, 840, 10045, 4, 3, 2] => {
                let (signature, _) = EcdsaSignature::decode(&signature_bytes)?;
                Signature::ECDSA(signature)
            },
            _ => Signature::RSA(signature_bytes),
        };

        Ok((Certificate{
            tbs_certificate,
            signature_algorithm,
            signature: signature_value,
        }, certificate_encoded_size))
    }
}

impl fmt::Display for Certificate {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}\n\n
Signature algorithm:\n{:<1}
\n{}
        ", self.tbs_certificate, self.signature_algorithm, self.signature)
    }
}

impl TryFrom<JSON> for Certificate {
    type Error = JSONError;

    fn try_from(value: JSON) -> Result<Self, Self::Error> {
        let mut props = value.resolve_into_object()?;
        let tbs_certificate = JSON::remove_property(&mut props,"tbsCertificate")?.try_into()?;
        let signature_algorithm = JSON::remove_property(&mut props,"signatureAlgorithm")?.try_into()?;
        Ok(Certificate {
            tbs_certificate,
            signature_algorithm,
            signature: Signature::None,
        })
    }
}