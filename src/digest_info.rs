use crate::algorithm::AlgorithmIdentifier;
use crate::asn1::encode::EncodeTo;
use crate::asn1::decode::DecodeFrom;
use crate::x509_error::X509Error;
use crate::asn1::decode;
use crate::asn1::encode;

pub struct DigestInfo {
    pub digest_algorithm: AlgorithmIdentifier,
    pub digest: Box<[u8]>,
}

impl EncodeTo for DigestInfo {
    fn encode(&self) -> Box<[u8]> {
        let mut result = self.digest_algorithm.encode().into_vec();
        result.extend_from_slice(&encode::encode_octet_string(&self.digest));
        encode::encode_sequence(&result)
    }
}

impl DecodeFrom for DigestInfo {
    fn decode(bytes: &[u8]) -> Result<(DigestInfo, usize), X509Error> {
        let (bytes, total_encoded_size) = decode::uncover_sequence(bytes)?;
        let (digest_algorithm, bytes) = AlgorithmIdentifier::decode_cut(bytes)?;
        let (digest, _, _) = decode::decode_octet_string(bytes)?;

        Ok((DigestInfo {
            digest_algorithm,
            digest,
        }, total_encoded_size))
    }
}