use crate::attribute::Name;
use crate::oid::{ObjectIdentifier, oid_to_human_readable};
use crate::asn1::encode::EncodeTo;
use crate::asn1::decode::DecodeFrom;
use crate::asn1::{encode, decode};
use crate::x509_error::{X509Error, X509ErrorKind};
use std::str;
use core::fmt;
use crate::universal_type::UniversalType;

#[derive(Debug, Clone)]
pub enum GeneralName {
    OtherName,
    Rfc822Name(String),
    DNSName(String),
    X400Address,
    DirectoryName(Name),
    EdiPartyName,
    UniformResourceIdentifier(String),
    IPAddress(Box<[u8]>),
    RegisteredID(ObjectIdentifier),
}

impl EncodeTo for GeneralName {
    fn encode(&self) -> Box<[u8]> {
        let mut result = match self {
            GeneralName::Rfc822Name(name) => encode::encode_ia5_string(name.as_bytes()),
            GeneralName::DNSName(name) => encode::encode_ia5_string(name.as_bytes()),
            GeneralName::UniformResourceIdentifier(name) => encode::encode_ia5_string(name.as_bytes()),
            GeneralName::DirectoryName(name) => name.encode(),
            GeneralName::IPAddress(address) => encode::encode_octet_string(&address),
            GeneralName::RegisteredID(identifier) => encode::encode_object_identifier(&identifier),
            _ => Box::new([])
        };
        encode::encode_implicit(self.get_tag_number(), &mut result);
        result
    }
}

impl DecodeFrom for GeneralName {
    fn decode(bytes: &[u8]) -> Result<(GeneralName, usize), X509Error> {
        let (_, tag_complexity, tag_number, length, encoded_size) = decode::decode_tag_and_length(bytes)?;
        let total_encoded_size = length + encoded_size;
        let mut bytes = (&bytes[..total_encoded_size]).to_vec();

        let name = match tag_number {
            0 => GeneralName::OtherName,
            1 => {
                bytes[0] = encode::encode_tag_universal_identifier(tag_complexity, UniversalType::IA5String);
                let (str_bytes, _, _) = decode::decode_ia5_string(&bytes)?;
                let name = str::from_utf8(&str_bytes)?;
                GeneralName::Rfc822Name(name.to_string())
            },
            2 => {
                bytes[0] = encode::encode_tag_universal_identifier(tag_complexity, UniversalType::IA5String);
                let (str_bytes, _, _) = decode::decode_ia5_string(&bytes)?;
                let name = str::from_utf8(&str_bytes)?;
                GeneralName::DNSName(name.to_string())
            },
            3 => GeneralName::X400Address,
            4 => {
                bytes[0] = encode::encode_tag_universal_identifier(tag_complexity, UniversalType::Sequence);
                let (name, _) = Name::decode(&bytes)?;
                GeneralName::DirectoryName(name)
            },
            5 => GeneralName::EdiPartyName,
            6 => {
                bytes[0] = encode::encode_tag_universal_identifier(tag_complexity, UniversalType::IA5String);
                let (str_bytes, _, _) = decode::decode_ia5_string(&bytes)?;
                let name = str::from_utf8(&str_bytes)?;
                GeneralName::UniformResourceIdentifier(name.to_string())
            },
            7 => {
                bytes[0] = encode::encode_tag_universal_identifier(tag_complexity, UniversalType::OctetString);
                let (bytes, _, _) = decode::decode_octet_string(&bytes)?;
                GeneralName::IPAddress(bytes)
            },
            8 => {
                bytes[0] = encode::encode_tag_universal_identifier(tag_complexity, UniversalType::ObjectIdentifier);
                let (identifier, _, _) = decode::decode_object_identifier(&bytes)?;
                GeneralName::RegisteredID(identifier)
            }
            _ => return Err(X509ErrorKind::Decode.into()),
        };

        Ok((name, total_encoded_size))
    }
}

impl GeneralName {
    pub fn get_tag_number(&self) -> u64 {
        match self {
            GeneralName::OtherName => 0,
            GeneralName::Rfc822Name(_) => 1,
            GeneralName::DNSName(_) => 2,
            GeneralName::X400Address => 3,
            GeneralName::DirectoryName(_) => 4,
            GeneralName::EdiPartyName => 5,
            GeneralName::UniformResourceIdentifier(_) => 6,
            GeneralName::IPAddress(_) => 7,
            GeneralName::RegisteredID(_) => 8,
        }
    }
}

impl fmt::Display for GeneralName {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let string = match self {
            GeneralName::OtherName => "<some other name>".to_string(),
            GeneralName::Rfc822Name(name) => format!("Rfc822Name : {}", name),
            GeneralName::DNSName(name) => format!("DNS : {}", name),
            GeneralName::X400Address => "<some X400Address>".to_string(),
            GeneralName::DirectoryName(name) => format!("Directory string : {}", name),
            GeneralName::EdiPartyName => "<some EdiPartyName>".to_string(),
            GeneralName::UniformResourceIdentifier(name) => format!("URI : {}", name),
            GeneralName::IPAddress(address) => format!("{:?}", address),
            GeneralName::RegisteredID(identifier) => format!("Registered Id : {}", oid_to_human_readable(identifier)),
        };
        write!(f, "{}", string)
    }
}

//pub struct AnotherName{
//    pub type_id: ObjectIdentifier,
//    pub value: Box<[u8]>,
//}
//
//pub struct EDIPartyName {
//    pub name_assigner: String,
//    pub party_name: String,
//}

