use crate::x509_error::{X509Error, X509ErrorKind};

//const END_OF_CONTENT:u8 = 0;
const BOOLEAN:u8 = 1;
const INTEGER:u8 = 2;
const BIT_STRING:u8 = 3;
const OCTET_STRING:u8 = 4;
const NULL:u8 = 5;
const OBJECT_IDENTIFIER:u8 = 6;
//const OBJECT_DESCRIPTOR:u8 = 7;
//const EXTERNAL:u8 = 8;
//const REAL:u8 = 9;
//const ENUMERATED:u8 = 10;
//const EMBEDDED_PDV:u8 = 11;
const UTF8_STRING:u8 = 12;
//const RELATIVE_OID:u8 = 13;
const SEQUENCE:u8 = 16;
const SET:u8 = 17;
//const NUMERIC_STRING:u8 = 18;
const PRINTABLE_STRING:u8 = 19;
//const T61_STRING:u8 = 20;
//const VIDEOTEX_STRING:u8 = 21;
const IA5_STRING:u8 = 22;
const UTC_TIME:u8 = 23;
const GENERALIZED_TIME:u8 = 24;
//const GRAPHIC_STRING:u8 = 25;
const VISIBLE_STRING:u8 = 26;
//const GENERAL_STRING:u8 = 27;
//const UNIVERSAL_STRING:u8 = 28;
//const CHARACTER_STRING:u8 = 29;
//const BMP_STRING:u8 = 30;

#[derive(PartialEq)]
pub enum UniversalType {
    Boolean,
    Integer,
    BitString,
    OctetString,
    Null,
    ObjectIdentifier,
    IA5String,
    Utf8String,
    Sequence,
    Set,
    PrintableString,
    UtcTime,
    GeneralizedTime,
    VisibleString,
}

impl UniversalType {
    pub fn to_u8(&self) -> u8 {
        match self {
            UniversalType::Boolean => BOOLEAN,
            UniversalType::Integer => INTEGER,
            UniversalType::BitString => BIT_STRING,
            UniversalType::OctetString => OCTET_STRING,
            UniversalType::Null => NULL,
            UniversalType::ObjectIdentifier => OBJECT_IDENTIFIER,
            UniversalType::IA5String => IA5_STRING,
            UniversalType::Utf8String => UTF8_STRING,
            UniversalType::Sequence => SEQUENCE,
            UniversalType::Set => SET,
            UniversalType::PrintableString => PRINTABLE_STRING,
            UniversalType::UtcTime => UTC_TIME,
            UniversalType::GeneralizedTime => GENERALIZED_TIME,
            UniversalType::VisibleString => VISIBLE_STRING,
        }
    }

    pub fn from_u64(value: u64) -> Result<UniversalType, X509Error> {

        if value > 31 {
            return Err(X509ErrorKind::Decode.into());
        }

        let value = value as u8;

        match value {
            BOOLEAN => Ok(UniversalType::Boolean),
            INTEGER => Ok(UniversalType::Integer),
            BIT_STRING => Ok(UniversalType::BitString),
            OCTET_STRING => Ok(UniversalType::OctetString),
            NULL => Ok(UniversalType::Null),
            OBJECT_IDENTIFIER => Ok(UniversalType::ObjectIdentifier),
            UTF8_STRING => Ok(UniversalType::Utf8String),
            SEQUENCE => Ok(UniversalType::Sequence),
            SET => Ok(UniversalType::Set),
            PRINTABLE_STRING => Ok(UniversalType::PrintableString),
            IA5_STRING => Ok(UniversalType::IA5String),
            UTC_TIME => Ok(UniversalType::UtcTime),
            GENERALIZED_TIME => Ok(UniversalType::GeneralizedTime),
            VISIBLE_STRING => Ok(UniversalType::VisibleString),
            _ => Err(X509ErrorKind::Decode.into())
        }
    }
}



