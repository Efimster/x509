use x509::signature::Signature;
use x509::public_key::PublicKey;
use x509::x509_error::X509ErrorKind;
use x509::rsa::rsa_primitives::{rsassa_pkcs_verify, rsassa_pkcs_sign};
use crate::Command;
use crate::Context;
use bigint::BigInt;
use crate::process_command_line;
use x509::rsa::rsa_private_key::RSAPrivateKey;
use x509::rsa::rsa_public_key::RSAPublicKey;
use x509::algorithm::AlgorithmIdentifier;
use x509::algorithm::AlgorithmParameters;
use x509::public_key_info::SubjectPublicKeyInfo;
use x509::asn1::encode::EncodeTo;
use x509::private_key::PrivateKey;
use x509::oid;
use x509::utils::file_util::write_pem_file;
use std::error::Error;
use std::{io, fs};
use std::io::Write;
use x509::x509_error::X509Error;

pub fn rsa_verify_certificate(signature: &Signature, public_key:&PublicKey, signature_algorithm_id:&[u64],  message:&[u8]) -> Result<bool, X509Error> {

    let signature = match signature {
        Signature::RSA(bytes) => bytes,
        _ => return Err(X509ErrorKind::RsaSignatureExpected.into()),
    };

    let public_key = match public_key {
        PublicKey::RSA(key) => key,
        _ => return Err(X509ErrorKind::RsaPublicKeyExpected.into()),
    };

    let sha_algorithm = match signature_algorithm_id {
        oid::SHA256_WITH_RSA_ENCRYPTION => oid::SHA256,
        oid::SHA384_WITH_RSA_ENCRYPTION => oid::SHA384,
        oid::SHA512_WITH_RSA_ENCRYPTION => oid::SHA512,
        oid::SHA224_WITH_RSA_ENCRYPTION => oid::SHA224,
        oid::SHA1_WITH_RSA_ENCRYPTION => oid::SHA1,
        _ => return Err(X509ErrorKind::UnsupportedSignatureAlgorithm.into()),
    };

    rsassa_pkcs_verify(public_key, message, &signature, sha_algorithm)
}

pub fn execute_generate_rsa_keys_command(command: &Command, context: &mut Context) -> Result<(), Box<dyn Error>> {
    let primes_count = command.get_argument_nth_parameter_usize("primes-count", 0).unwrap_or(2);
    let bits: usize = command.get_argument_nth_parameter_usize("bits", 0).unwrap_or(2048);
    let mut primes: Vec<BigInt> = Vec::new();

    match command.arguments.get("prime-files") {
        Some(params) => {
            for filepath in params.iter() {
                process_command_line(&format!("read-prime --file {}", filepath), context).unwrap();
                primes.push(context.prime.as_ref().unwrap().clone());
            }
        },
        None => (),
    }
    if primes_count > primes.len() {
        let primes_count = primes_count - primes.len();
        let sum: usize = primes.iter().map(|prime| prime.bits_count()).sum();
        let bits: usize = bits - sum;
        let prime_bits = bits / primes_count;
        let mut leftover_bytes = (bits % primes_count) >> 3;
        if (bits % primes_count) & 0b111 > 0 {
            leftover_bytes += 1;
        }

        for _ in 0..primes_count {
            let add_bits = (if leftover_bytes > 0 { 1usize } else { 0 }) << 3;
            process_command_line(&format!("generate-prime --bits {} --dump", prime_bits + add_bits), context)?;
            primes.push(context.prime.as_ref().unwrap().clone());
            if leftover_bytes > 0 {
                leftover_bytes -= 1;
            }
        }
    }


    for i in 0usize..primes.len() {
        println!("prime ({}) {:02x}", i, &primes[i]);
    }

    let private_key = RSAPrivateKey::from_primes(&primes);
    let public_key = RSAPublicKey {
        modulus: private_key.modulus.clone(),
        exponent: private_key.public_exponent.clone(),
    };

    context.public_key = Some(PublicKey::RSA(public_key.clone()));
    context.private_key = Some(PrivateKey::RSA(private_key.clone()));

    println!("< Public key {}", &public_key);
    println!("< Private key {}", &private_key);

    if let Some(filepath) = command.get_argument_nth_parameter("file-pub-key", 0) {
        println!("< Saving public key to {}", filepath);
        let algorithm = AlgorithmIdentifier {
            algorithm_id: Box::from(oid::RSA_ENCRYPTION),
            parameters: AlgorithmParameters::None,
        };
        let public_key_info = SubjectPublicKeyInfo {
            algorithm,
            public_key: PublicKey::RSA(public_key),
        };
        let encoded_public_key = public_key_info.encode();
        write_pem_file(filepath, "RSA PUBLIC KEY", &encoded_public_key)?;
    }

    if let Some(filepath) = command.get_argument_nth_parameter("file-private-key", 0) {
        println!("< Saving private key to {}", filepath);
        let encoded_private_key = context.private_key.as_ref().unwrap().encode();
        write_pem_file(filepath, "RSA PRIVATE KEY", &encoded_private_key)?;
    }

    Ok(())
}

pub fn sign(private_key: &RSAPrivateKey, algorithm: &[u64], message: &[u8]) -> Result<Box<[u8]>, X509Error> {
    let sha_algorithm = match &algorithm as &[u64] {
        oid::SHA256_WITH_RSA_ENCRYPTION => oid::SHA256,
        oid::SHA384_WITH_RSA_ENCRYPTION => oid::SHA384,
        oid::SHA512_WITH_RSA_ENCRYPTION => oid::SHA512,
        oid::SHA224_WITH_RSA_ENCRYPTION => oid::SHA224,
        oid::SHA1_WITH_RSA_ENCRYPTION => oid::SHA1,
        _ => return Err(X509ErrorKind::UnsupportedSignatureAlgorithm.into()),
    };
    let signature = rsassa_pkcs_sign(private_key, &message, sha_algorithm)?;
    Ok(signature)
}

pub fn execute_generate_prime_command(command: &Command, context: &mut Context) -> Result<(), Box<dyn Error>> {
    let bits: usize = command.get_argument_nth_parameter_usize("bits", 0).unwrap_or(1024);
    print!("< {} prime generation could take some time...", bits);
    io::stdout().flush().ok();
    let prime = bigint::primality::random_k_bit_prime(bits);
    println!(".. done");
    if let Some(filepath) = command.get_argument_nth_parameter("file", 0) {
        let bytes: &[u8] = prime.as_bytes(true);
        fs::write(filepath, bytes)?;
    }

    if command.contains_argument("dump") {
        println!("< prime {:02x}", &prime);
    }
    context.prime = Some(prime);
    Ok(())
}