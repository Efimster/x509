use std::{error::Error, fmt};

#[allow(dead_code)]
#[derive(Clone, Debug)]
pub enum CliError {
    MissingParameter,
    ParseCommandLine,
    MissingContext,
}

impl Error for CliError {}

impl fmt::Display for CliError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &self {
            CliError::MissingParameter => write!(f, "missing command parameter"),
            CliError::ParseCommandLine => write!(f, "error command line"),
            CliError::MissingContext => write!(f, "missing context"),
        } 
    }
}
