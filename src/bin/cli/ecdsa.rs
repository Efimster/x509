use sha::{sha1, sha256, sha512};
use ecc::domain_parameters::EccDomainParameters;
use x509::ecdsa::named_curve::NamedCurve;
use x509::public_key::PublicKey;
use x509::signature::Signature;
use x509::x509_error::X509ErrorKind;
use bigint::BigInt;
use x509::algorithm::AlgorithmIdentifier;
use x509::algorithm::AlgorithmParameters;
use x509::public_key_info::SubjectPublicKeyInfo;
use crate::Command;
use crate::Context;
use x509::private_key::PrivateKey;
use x509::asn1::encode::EncodeTo;
use x509::ecdsa::ecdsa_signature::EcdsaSignature;
use x509::ecdsa::ecdsa_primitives;
use x509::oid;
use x509::utils::file_util::write_pem_file;
use x509::x509_error::X509Error;

pub fn ecdsa_verify_public_key(signature:&Signature, public_key: &PublicKey, curve:&NamedCurve, message:&[u8]) -> Result<bool, X509Error> {
    let signature = match signature {
        Signature::ECDSA(signature) => signature,
        _ => return Err(X509ErrorKind::EcdsaSignatureExpected.into()),
    };

    let public_key = match public_key {
        PublicKey::ECDSA(point) => point,
        _ => return Err(X509ErrorKind::EcdsaPublicKeyExpected.into()),
    };
    let (digest, domain_parameters) = match curve {
        NamedCurve::Secp192r1 => (Box::from(&sha1::encode(message) as &[u8]) as Box<[u8]>, EccDomainParameters::new_p_192()),
        NamedCurve::Secp256r1 => (Box::from(&sha256::encode(message) as &[u8]) as Box<[u8]>, EccDomainParameters::new_p_256()),
        NamedCurve::Secp521r1 => (Box::from(&sha512::encode(message) as &[u8]) as Box<[u8]>, EccDomainParameters::new_p_521()),
    };

    let digest = ecc::data_conversion::octets_to_integer(&digest);
    let signature_r_part = ecc::data_conversion::octets_to_integer(&signature.r);
    let signature_s_part = ecc::data_conversion::octets_to_integer(&signature.s);
    Ok(ecdsa_primitives::ecdsa_verify_p(&signature_r_part, &signature_s_part, public_key, &domain_parameters,
    &digest))
}

pub fn execute_generate_ecdsa_keys_command(command: &Command, context: &mut Context) -> Result<(), X509Error> {
    let bits: usize = command.get_argument_nth_parameter_usize("bits", 0).unwrap_or(256);
    let (domain_parameters, curve) = match bits {
        192 => (EccDomainParameters::new_p_192(), NamedCurve::Secp192r1),
        256 => (EccDomainParameters::new_p_256(), NamedCurve::Secp256r1),
        // 384 => (EccDomainParameters::new_p_521(), NamedCurve::Secp384r1),
        512 => (EccDomainParameters::new_p_521(), NamedCurve::Secp521r1),
        _ => return Err(X509ErrorKind::EcdsaUnsupportedCurve.into()),
    };

    let user_input = command.get_argument_nth_parameter("user-input", 0).map(|s|s.as_bytes());
    let (private_key, public_key) = ecdsa_primitives::ecdsa_generate_keys(&domain_parameters, bits, user_input);
    context.public_key = Some(PublicKey::ECDSA(public_key.clone()));
    context.private_key = Some(PrivateKey::ECDSA(private_key.clone()));

    println!("< Public key {:?}", &public_key);
    println!("< Private key {:?}", &private_key);

    if let Some(filepath) = command.get_argument_nth_parameter("file-pub-key", 0) {
        println!("< Saving public key to {}", filepath);
        let algorithm = AlgorithmIdentifier {
            algorithm_id: Box::from(oid::ELLIPTIC_CURVE_PUBLIC_KEY),
            parameters: AlgorithmParameters::Ecdsa(curve),
        };
        let public_key_info = SubjectPublicKeyInfo {
            algorithm,
            public_key: PublicKey::ECDSA(public_key),
        };
        let encoded_public_key = public_key_info.encode();
        write_pem_file(filepath, "EC PUBLIC KEY", &encoded_public_key)?;
    }

    if let Some(filepath) = command.get_argument_nth_parameter("file-private-key", 0) {
        println!("< Saving private key to {}", filepath);
        let encoded_private_key = PrivateKey::ECDSA(private_key).encode();
        write_pem_file(filepath, "EC PRIVATE KEY", &encoded_private_key)?;
    }

    Ok(())
}

pub fn sign(private_key:&BigInt, algorithm:&[u64], message:&[u8], user_input:Option<&[u8]>) -> Result<EcdsaSignature, X509Error> {
    let (digest, domain_parameters, secret_key_length_bits) = match &algorithm as &[u64] {
        oid::ECDSA_SIGNATURE_WITH_SHA_256 => {
            (ecc::data_conversion::octets_to_integer(&sha256::encode(&message)),
                ecc::domain_parameters::EccDomainParameters::new_p_256(), 256)
        },
        oid::ECDSA_SIGNATURE_WITH_SHA_1 => {
            (ecc::data_conversion::octets_to_integer(&sha1::encode(&message)),
             ecc::domain_parameters::EccDomainParameters::new_p_192(), 192)
        },
        // oid::ECDSA_SIGNATURE_WITH_SHA_384 => {
        //     (ecc::data_conversion::octets_to_integer(&sha1::encode(&message)),
        //      ecc::domain_parameters::EccDomainParameters::new_p_384(), 384)
        // },
        oid::ECDSA_SIGNATURE_WITH_SHA_512 => {
            (ecc::data_conversion::octets_to_integer(&sha512::encode(&message)),
             ecc::domain_parameters::EccDomainParameters::new_p_521(), 512)
        },
        _ => return Err(X509ErrorKind::UnsupportedSignatureAlgorithm.into()),
    };

    let (k, k_inverse) = ecdsa_primitives::ecdsa_generate_secret_number_and_inverse(
        &domain_parameters.point_prime_order, secret_key_length_bits, user_input);
    let (r, s) = ecdsa_primitives::ecdsa_sign(private_key, &domain_parameters,
      &digest, &k, &k_inverse);
    let signature = EcdsaSignature {r:ecc::data_conversion::integer_to_octets(&r), s:ecc::data_conversion::integer_to_octets(&s)};
    Ok(signature)
}
