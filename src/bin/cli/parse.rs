use std::iter::Peekable;
use crate::Command;
use crate::CliError;
use std::collections::HashMap;

pub fn parse_command_string(string:&str) -> Result<Option<Command>, CliError> {
    parse_command_line(&mut string.split(" ").into_iter().peekable())
}

pub fn parse_command_line<'a, I>(args: &mut Peekable<I>) -> Result<Option<Command>, CliError>
    where I: Iterator<Item=&'a str>
{
    let command = match args.next() {
        None => return Ok(None),
        Some(value) => value.to_string(),
    };

    let mut arguments = HashMap::new();
    arguments.insert("".to_string(), parse_argument_values(args));
    loop {
        match parse_command_argument(args)? {
            None => break,
            Some((name, values)) => {
                arguments.insert(name, values);
            }
        }
    }
    Ok(Some(Command { name: command, arguments }))
}

fn parse_command_argument<'a, I>(args: &mut Peekable<I>) -> Result<Option<(String, Box<[String]>)>, CliError>
    where I: Iterator<Item=&'a str>
{
    match args.next() {
        None => Ok(None),
        Some(ref value) if value.starts_with("-") => {
            let values = parse_argument_values(args);
            let key_index: usize = if value.starts_with("--") { 2 } else { 1 };
            Ok(Some(((&value[key_index..]).to_string(), values)))
        }
        Some(_) => Err(CliError::ParseCommandLine)
    }
}

fn parse_argument_values<'a, I>(args: &mut Peekable<I>) -> Box<[String]>
    where I: Iterator<Item=&'a str>
{
    let mut result = vec![];
    loop {
        match args.peek() {
            Some(value) => {
                if !value.starts_with("-") {
                    result.push(args.next().unwrap().to_string());
                } else {
                    break;
                }
            }
            _ => break,
        }
    }

    result.into_boxed_slice()
}