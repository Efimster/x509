extern crate x509;
mod parse;
mod ocsp;
mod ecdsa;
mod rsa;
mod cli_error;

use std::error::Error;
use std::fs;
use std::env;
use std::collections::HashMap;
use std::io;
use std::io::BufRead;
use crate::parse::*;
use bigint::BigInt;
use x509::asn1::encode::EncodeTo;
use x509::certificate::Certificate;
use x509::asn1::decode::DecodeFrom;
use x509::public_key_info::SubjectPublicKeyInfo;
use x509::algorithm::*;
use x509::public_key::PublicKey;
use json::JSON;
use sha::sha1;
use x509::extensions::authority_key_id::AuthorityKeyIdentifierExtension;
use x509::extensions::extension::StandardExtension;
use x509::extensions::extension::Extension;
use x509::extensions::subject_key_id::SubjectKeyIdentifierExtension;
use x509::x509_error::X509ErrorKind;
use crate::rsa::{execute_generate_rsa_keys_command, execute_generate_prime_command};
use x509::private_key::PrivateKey;
use crate::ecdsa::execute_generate_ecdsa_keys_command;
use x509::signature::Signature;
use x509::oid;
use x509::utils::file_util::{read_pem_file, read_certificate_file, write_certificate_file};
use crate::cli_error::CliError;
use std::path::Path;
use std::ffi::OsStr;


#[derive(Debug)]
pub struct Command {
    name: String,
    arguments: HashMap<String, Box<[String]>>,
}

impl Command {
    pub fn get_argument_nth_parameter_usize(&self, argument:&str, nth:usize) -> Option<usize> {
        match self.arguments.get(argument) {
            Some(params) if params.len() > nth => usize::from_str_radix(&params[nth], 10).ok(),
            _ => None,
        }
    }

    pub fn get_argument_nth_parameter(&self, argument: &str, nth: usize) -> Option<&str> {
        match self.arguments.get(argument) {
            Some(params) if params.len() > nth => Some(&params[nth]),
            _ => None,
        }
    }

    pub fn contains_argument(&self, argument: &str) -> bool {
        self.arguments.contains_key(argument)
    }
}



pub struct Context {
    certificate: Option<Certificate>,
    public_key: Option<PublicKey>,
    prime: Option<BigInt>,
    private_key: Option<PrivateKey>,
}


fn main() {
    let mut context = Context {
        certificate: None,
        public_key: None,
        private_key: None,
        prime: None,
    };
    let command_line_arguments:Vec<String> = env::args().skip(1).collect::<Vec<String>>();
    let command_line_arguments = command_line_arguments.iter().map(|s|&s[..]).peekable();
    let mut command = match parse_command_line(&mut command_line_arguments.peekable()) {
        Ok(Some(command)) => command,
        _ => Command {name:"help".to_string(), arguments:HashMap::new()}
    };

    loop  {
        match process_command(&command, &mut context) {
            Err(err) => { println!("< Error: {:?}", err); },
            _ => (),
        }
        let line = io::stdin().lock().lines().next().unwrap().unwrap();
        match parse_command_line(&mut line.split(" ").into_iter().peekable()) {
            Ok(Some(value)) => command = value,
            _ => { println!("< Cannot parse command line");},
        }
    }
}

fn process_command_line(line:&str, context:&mut Context) -> Result<(), Box<dyn Error>>{
    let command = parse_command_string(&line).unwrap();
    process_command(&command.unwrap(), context)
}

fn process_command(command:&Command, context:&mut Context) -> Result<(), Box<dyn Error>>{
    match command.name.as_ref() {
        "exit" | "q" => std::process::exit(0),
        "help" => execute_help_command()?,
        "read-certificate" => execute_read_certificate_command(command, context)?,
        "read-public-key" => execute_read_public_key_command(command, context)?,
        "read-private-key" => execute_read_private_key_command(command, context)?,
        "dump-certificate" =>execute_dump_certificate_command(context)?,
        "dump-public-key" => execute_dump_public_key_command(context)?,
        "verify" => execute_verify_certificate_command(command, context)?,
        "read-prime" => execute_read_prime_command(command, context)?,
        "generate-prime" => execute_generate_prime_command(command, context)?,
        "generate-rsa-keys" => execute_generate_rsa_keys_command(command, context)?,
        "generate-certificate" => execute_generate_certificate_command(command, context)?,
        "generate-ecdsa-keys" => execute_generate_ecdsa_keys_command(command, context)?,
        _ => {
            println!("< Unknown `{}` command", command.name);
            execute_help_command()?;
        },
    }
    Ok(())
}

fn execute_read_certificate_command(command: &Command, context: &mut Context) -> Result<(), Box<dyn Error>> {
    let filepath = command.get_argument_nth_parameter("file", 0).ok_or(CliError::MissingParameter)?;

    let content = read_certificate_file(filepath)?;
    let file_extension = Path::new(filepath).extension().and_then(OsStr::to_str);

    let certificate = match file_extension {
        Some(extension) if extension == "json" => {
            let json = JSON::parse(&fs::read_to_string(filepath)?)?;
            json.try_into()?
        },
        _ => Certificate::decode(&content)?.0
    };

    context.certificate =  Some(certificate);

    if command.arguments.contains_key("dump") {
        execute_dump_certificate_command(context)?;
    }

    if command.arguments.contains_key("verify") {
        if let Some(filepath) = command.get_argument_nth_parameter("signer-certificate", 0) {
            process_command_line(&format!("verify --signer-certificate {}", filepath), context)?;
        }
        else {
            process_command_line("verify", context)?;
        }
    }

    Ok(())
}

fn execute_help_command() -> Result<(), Box<dyn Error>>{
    println!("\
< read-certificate -file filepath [--dump] [--verify] [--signer-certificate signer_certificate.cer]  -->  reads public certificate from file
< read-prime -file filepath [--dump]
< read-public-key -file filepath [--dump]
< read-private-key [rsa|ecdsa] -file filepath [--dump]
< dump-certificate --> dumps certificate in context to standard output
< verify [--signer-certificate signer_certificate.cer] -->  verifies certificate in context
< generate-prime --bits 1024 [--dump] [--file filepath]
< generate-rsa-keys --bits 2048 [--primes-count 2] [--prime-files file1.prime file2.prime] \
[--file-pub-key savepublickey.pem] [--file-private-key saveprivatekey.pem]
< generate-certificate --json res/certificate.json [--public-key generated/pubkey.pem] \
[--private-key generated/privatekey.pem] --dump [--file saveto.cer] [--verify] [--signer-certificate generated/trytlsca.cer]
< generate-ecdsa-keys --bits 256 [--file-pub-key savepublickey.pem] [--file-private-key saveprivatekey.pem]
\
    ");
    Ok(())
}

fn execute_dump_certificate_command(context: &Context) -> Result<(), Box<dyn Error>> {
    println!("{}", context.certificate.as_ref().ok_or(CliError::MissingContext)?);
    Ok(())
}

fn execute_verify_certificate_command(command: &Command, context: &Context) -> Result<(), Box<dyn Error>> {
    let certificate = context.certificate.as_ref().ok_or(CliError::MissingContext)?;
    let signer_certificate = match command.get_argument_nth_parameter("signer-certificate", 0) {
        Some(filepath) => {
            let content = read_certificate_file(filepath)?;
            Some(Certificate::decode(&content)?.0)
        },
        None => None,
    };
    let public_key_info = match signer_certificate {
        Some(ref certificate) => &certificate.tbs_certificate.subject_public_key_info,
        None => &certificate.tbs_certificate.subject_public_key_info,
    };

    let message: &[u8] = &certificate.tbs_certificate.der_encoded;
    let result = match &public_key_info.algorithm.algorithm_id as &[u64] {
        oid::RSA_ENCRYPTION => {
            let algorithm_id = &certificate.signature_algorithm.algorithm_id;
            rsa::rsa_verify_certificate(&certificate.signature, &public_key_info.public_key, algorithm_id, message)
        },
        oid::ELLIPTIC_CURVE_PUBLIC_KEY => match &public_key_info.algorithm.parameters {
            AlgorithmParameters::Ecdsa(curve) => {
                let result = ecdsa::ecdsa_verify_public_key(&certificate.signature, &public_key_info.public_key, curve, message)?;
                Ok(result)
            }
            _ =>
                Err(X509ErrorKind::EllipticCurveMismatch.into()),
        },
        _ => Err(X509ErrorKind::UnsupportedPublicKeyAlgorithm.into()),
    };

    match result {
        Ok(result) if  result => {
            println!("VERIFICATION: SUCCESSFUL");
            Ok(())
        },
        Ok(_)  => {
            println!("VERIFICATION: FAILED");
            Ok(())
        },
        Err(err) => {
            println!("VERIFICATION: FAILED");
            Err(err.into())
        },
    }
}

fn execute_dump_public_key_command(context: &Context) -> Result<(), CliError> {
    println!("{}", context.public_key.as_ref().ok_or(CliError::MissingContext)?);
    Ok(())
}

fn execute_read_public_key_command(command: &Command, context: &mut Context) -> Result<(), Box<dyn Error>> {
    let filepath = command.get_argument_nth_parameter("file", 0).ok_or(CliError::MissingParameter)?;
    let signature_schema: &str = command.get_argument_nth_parameter("", 0).get_or_insert("rsa");
    let content = match signature_schema {
        "rsa" => read_pem_file(filepath, "RSA PUBLIC KEY")?,
        "ecdsa" => read_pem_file(filepath, "EC PUBLIC KEY")?,
        _ => return Err(X509ErrorKind::UnsupportedSignatureAlgorithm.into_boxed_error()),
    };
    context.public_key = Some(SubjectPublicKeyInfo::decode(&content)?.0.public_key);

    if command.arguments.contains_key("dump") {
        execute_dump_public_key_command(context)?;
    }
    Ok(())
}

fn execute_read_prime_command(command: &Command, context: &mut Context) -> Result<(), Box<dyn Error>> {
    let filepath = command.get_argument_nth_parameter("file", 0).ok_or(CliError::MissingParameter)?;
    let content = fs::read(filepath)?;
    context.prime = Some(BigInt::from(&content as &[u8]));

    if command.contains_argument("dump") {
        println!("< prime {:02x}", context.prime.as_ref().unwrap());
    }
    Ok(())
}

fn execute_read_private_key_command(command: &Command, context: &mut Context) -> Result<(), Box<dyn Error>> {
    let filepath = command.get_argument_nth_parameter("file", 0).ok_or(CliError::MissingParameter)?;
    let signature_schema:&str = command.get_argument_nth_parameter("", 0).get_or_insert("rsa");
    let private_key = match signature_schema {
        "rsa" => {
            let content = read_pem_file(filepath, "RSA PRIVATE KEY")?;
            PrivateKey::decode_rsa(&content)?.0
        },
        "ecdsa" => {
            let content = read_pem_file(filepath, "EC PRIVATE KEY")?;
            PrivateKey::decode_ecdsa(&content)?.0
        },
        _ => {
            eprintln!("< Fail to read private key. {} signature schema is unknown", signature_schema);
            return Err(CliError::MissingParameter.into());
        }
    };
    context.private_key = Some(private_key.clone());

    if command.arguments.contains_key("dump") {
        println!("< Private key: {}", &private_key)
    }
    Ok(())
}

fn execute_generate_certificate_command(command: &Command, context: &mut Context) -> Result<(), Box<dyn Error>> {
    let filepath = command.get_argument_nth_parameter("json", 0).ok_or(CliError::MissingParameter)?;
    let json = JSON::parse(&fs::read_to_string(filepath)?)?;
    let mut certificate:Certificate = json.try_into().expect("< Can't parse the certificate from JSON");
    let public_key_schema = match &certificate.tbs_certificate.subject_public_key_info.algorithm.algorithm_id as &[u64] {
        oid::ELLIPTIC_CURVE_PUBLIC_KEY => "ecdsa",
        oid::RSA_ENCRYPTION => "rsa",
        _ => return Err(X509ErrorKind::UnsupportedPublicKeyAlgorithm.into_boxed_error()),
    };

    let private_key_schema = match &certificate.signature_algorithm.algorithm_id as &[u64] {
        oid::ECDSA_SIGNATURE_WITH_SHA_256 | oid::ECDSA_SIGNATURE_WITH_SHA_1 => "ecdsa",
        oid::SHA256_WITH_RSA_ENCRYPTION | oid::SHA1_WITH_RSA_ENCRYPTION => "rsa",
        _ => return Err(X509ErrorKind::UnsupportedSignatureAlgorithm.into_boxed_error()),
    };

    if let Some(filepath) = command.get_argument_nth_parameter("public-key", 0) {
        process_command_line(&format!("read-public-key {} --file {}", public_key_schema, filepath), context)?;
    }

    if let Some(filepath) = command.get_argument_nth_parameter("private-key", 0) {
        process_command_line(&format!("read-private-key {} --file {}", private_key_schema, filepath), context)?;
    }
    match context.public_key {
        Some(ref key) => {
            if key.to_oid() != certificate.tbs_certificate.subject_public_key_info.algorithm.algorithm_id {
                return Err(X509ErrorKind::UnsupportedPublicKeyAlgorithm.into_boxed_error());
            }
            certificate.tbs_certificate.subject_public_key_info.public_key = key.clone();
        },
        None => return Err(X509ErrorKind::UnsupportedPublicKeyAlgorithm.into_boxed_error())
    }
    let encoded_subject_public_key_info = certificate.tbs_certificate.subject_public_key_info.encode();
    let subject_key_id = sha1::encode(&encoded_subject_public_key_info);
    let mut extensions = certificate.tbs_certificate.extensions.to_vec();
    extensions.push(create_authority_key_identifier_extension(subject_key_id));
    extensions.push(create_subject_key_identifier_extension(subject_key_id));
    certificate.tbs_certificate.extensions = extensions.into_boxed_slice();
    if let Some(ref private_key) = context.private_key {
        let tbs_certificate_der_encoded = certificate.tbs_certificate.encode();
        let signature = match private_key {
            PrivateKey::RSA(key) => {
                let signature = rsa::sign(key, &certificate.signature_algorithm.algorithm_id,
                    &tbs_certificate_der_encoded)?;
                Signature::RSA(signature)
            },
            PrivateKey::ECDSA(key) => {
                let signature = ecdsa::sign(key,
                &certificate.signature_algorithm.algorithm_id, &tbs_certificate_der_encoded, None)?;
                Signature::ECDSA(signature)
            },
            PrivateKey::None => Signature::None,
        };
        certificate.signature = signature;
        certificate.tbs_certificate.der_encoded = tbs_certificate_der_encoded;
    }

    if let Some(filepath) = command.get_argument_nth_parameter("file", 0) {
        write_certificate_file(filepath, &certificate.encode())?
    }

    context.certificate = Some(certificate);

    if command.arguments.contains_key("dump") {
        println!("{}", context.certificate.as_ref().unwrap())
    }

    if command.arguments.contains_key("verify") {
        match command.get_argument_nth_parameter("signer-certificate", 0) {
            Some(filepath) => process_command_line(&format!("verify --signer-certificate {}", filepath), context)?,
            None => process_command_line("verify", context)?,
        }
    }

    Ok(())
}

fn create_authority_key_identifier_extension(subject_key_id:[u8; 20]) -> Extension{
    Extension {
        id: Box::new([2, 5, 29, 35]),
        critical: false,
        value: StandardExtension::AuthorityKeyIdentifier(AuthorityKeyIdentifierExtension {
            key_identifier: Box::new(subject_key_id)
        })
    }
}

fn create_subject_key_identifier_extension(subject_key_id: [u8; 20]) -> Extension {
    Extension {
        id: Box::new([2, 5, 29, 14]),
        critical: false,
        value: StandardExtension::SubjectKeyIdentifier(SubjectKeyIdentifierExtension {
            key_identifier: Box::new(subject_key_id)
        })
    }
}