use crate::asn1::encode::EncodeTo;
use crate::asn1::decode::DecodeFrom;
use crate::x509_error::{X509Error, X509ErrorKind};
use json::error::JSONErrorKind;
use json::JSON;
use json::error::JSONError;
use crate::asn1::encode;
use crate::asn1::decode;

const V1: u8 = 0;
const V2: u8 = 1;
const V3: u8 = 2;

#[derive(Debug)]
pub enum Version {
    V1,
    V2,
    V3,
}

impl EncodeTo for Version {
    fn encode(&self) -> Box<[u8]> {
        encode::encode_integer(self.into())
    }
}

impl DecodeFrom for Version {
    fn decode(bytes: &[u8]) -> Result<(Version, usize), X509Error> {
        let (version, total_size, _) = decode::decode_integer(bytes)?;
        let version = version.try_into()?;
        Ok((version, total_size))
    }
}

impl TryFrom<JSON> for Version{
    type Error = JSONError;

    fn try_from(value: JSON) -> Result<Self, Self::Error> {
        value.resolve_into_number()
            .map(|item| item as i64)?.try_into()
            .map_err(|_|JSONErrorKind::Resolve.into())
    }
}

impl TryFrom<i64> for Version {
    type Error = X509Error;

    fn try_from(value: i64) -> Result<Self, Self::Error> {
        if value > 2 || value < 0 {
            return Err(X509ErrorKind::Decode.into());
        }
        let value = value as u8;
        match value {
            V1 => Ok(Version::V1),
            V2 => Ok(Version::V2),
            V3 => Ok(Version::V3),
            _ => Err(X509ErrorKind::Decode.into()),
        }
    }
}

impl From<&Version> for i64 {
    fn from(value: &Version) -> Self {
        (match value {
            Version::V1 => V1,
            Version::V2 => V2,
            Version::V3 => V3,
        }) as i64
    }
}