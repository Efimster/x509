use crate::asn1::decode::DecodeFrom;
use crate::x509_error::X509Error;
use crate::asn1::decode;
use crate::asn1::encode::EncodeTo;
use crate::asn1::encode;
use std::fmt;
use crate::display;
use crate::asn1::tag_class::TagClass;
use crate::asn1::tag_complexity::TagComplexity;

#[derive(Debug, Clone)]
pub struct AuthorityKeyIdentifierExtension {
    pub key_identifier:Box<[u8]>,
}

impl EncodeTo for AuthorityKeyIdentifierExtension {
    fn encode(&self) -> Box<[u8]> {
        let key_identifier = encode::encode_bytes(TagClass::ContextSpecific,
        TagComplexity::Primitive, 0, &self.key_identifier);
        encode::encode_sequence(&key_identifier)
    }
}

impl DecodeFrom for AuthorityKeyIdentifierExtension {
    fn decode(bytes: &[u8]) -> Result<(AuthorityKeyIdentifierExtension, usize), X509Error> {
        let (bytes, total_encoded_size) = decode::uncover_sequence(bytes)?;
        let mut key_identifier: Box<[u8]> = Box::new([]);

        let (tag_class, _, tag_number, _, _) = decode::decode_tag_and_length(bytes)?;
        if tag_number == 0 && tag_class == TagClass::ContextSpecific {
            let (result, _, _) = decode::decode_bytes(bytes, tag_class, tag_number)?;
            key_identifier = result;
        }

        Ok((AuthorityKeyIdentifierExtension { key_identifier }, total_encoded_size))
    }
}

impl fmt::Display for AuthorityKeyIdentifierExtension {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Key: {}", display::display_octets(&self.key_identifier, 20))
    }
}