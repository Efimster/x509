use std::fmt;
use crate::asn1::encode;
use crate::asn1::decode;
use crate::x509_error::{X509Error, X509ErrorKind};
use crate::asn1::decode::DecodeFrom;
use crate::asn1::encode::EncodeTo;
use json::error::JSONErrorKind;
use json::JSON;
use json::error::JSONError;
use std::str::FromStr;
use crate::oid::ObjectIdentifier;
use crate::oid;

#[derive(Debug, Clone)]
pub struct ExtendedKeyUsageExtension {
    key_purpose: Box<[KeyPurpose]>,
}

#[derive(Debug, Clone)]
pub enum KeyPurpose {
    ServerAuth,
    ClientAuth,
    CodeSigning,
    EmailProtection,
    TimeStamping,
    OCSPSigning,
    Any,
    Other(ObjectIdentifier),
}

impl FromStr for KeyPurpose {
    type Err = X509Error;

    fn from_str(s: &str) -> Result<KeyPurpose, X509Error> {
        match s {
            "serverAuth" => Ok(KeyPurpose::ServerAuth),
            "clientAuth" => Ok(KeyPurpose::ClientAuth),
            "codeSigning" => Ok(KeyPurpose::CodeSigning),
            "emailProtection" => Ok(KeyPurpose::EmailProtection),
            "timeStamping" => Ok(KeyPurpose::TimeStamping),
            "ocspSigning" => Ok(KeyPurpose::OCSPSigning),
            "any" => Ok(KeyPurpose::Any),
            _ => Err(X509ErrorKind::Decode.into()),
        }
    }
}

impl From<&[u64]> for KeyPurpose {
    fn from(identifier: &[u64]) -> KeyPurpose{
        match identifier {
            oid::KEY_PURPOSE_SERVER_AUTH => KeyPurpose::ServerAuth,
            oid::KEY_PURPOSE_CLIENT_AUTH => KeyPurpose::ClientAuth,
            oid::KEY_PURPOSE_CODE_SIGNING => KeyPurpose::CodeSigning,
            oid::KEY_PURPOSE_EMAIL_PROTECTION => KeyPurpose::EmailProtection,
            oid::KEY_PURPOSE_TIME_STAMPING => KeyPurpose::TimeStamping,
            oid::KEY_PURPOSE_OCSP_SIGNIN => KeyPurpose::OCSPSigning,
            oid::ANY_EXTENDED_KEY_USAGE => KeyPurpose::Any,
            identifier => KeyPurpose::Other(Box::from(identifier)),
        }
    }
}

impl KeyPurpose {
    pub fn to_oid(&self) -> ObjectIdentifier {
        let identifier = match self {
            KeyPurpose::ServerAuth => oid::KEY_PURPOSE_SERVER_AUTH,
            KeyPurpose::ClientAuth => oid::KEY_PURPOSE_CLIENT_AUTH,
            KeyPurpose::CodeSigning => oid::KEY_PURPOSE_CODE_SIGNING,
            KeyPurpose::EmailProtection => oid::KEY_PURPOSE_EMAIL_PROTECTION,
            KeyPurpose::TimeStamping => oid::KEY_PURPOSE_TIME_STAMPING,
            KeyPurpose::OCSPSigning => oid::KEY_PURPOSE_OCSP_SIGNIN,
            KeyPurpose::Any => oid::ANY_EXTENDED_KEY_USAGE,
            KeyPurpose::Other(identifier) => identifier,
        };
        Box::from(identifier)
    }
}

impl EncodeTo for ExtendedKeyUsageExtension {
    fn encode(&self) -> Box<[u8]> {
        let mut result = Vec::new();
        for i in 0 .. self.key_purpose.len() {
            let identifier = self.key_purpose[i].to_oid();
            result.extend_from_slice(&encode::encode_object_identifier(&identifier));
        }
        encode::encode_sequence(&result)
    }
}

impl DecodeFrom for ExtendedKeyUsageExtension {
    fn decode(bytes: &[u8]) -> Result<(ExtendedKeyUsageExtension, usize), X509Error> {
        let (mut bytes, total_encoded_size) = decode::uncover_sequence(bytes)?;
        let mut purposes = Vec::new();
        while bytes.len() > 0 {
            let (oid, size, _) = decode::decode_object_identifier(bytes)?;
            purposes.push(KeyPurpose::from(&oid as &[u64]));
            bytes = &bytes[size..];
        }
        Ok((ExtendedKeyUsageExtension { key_purpose: purposes.into_boxed_slice() }, total_encoded_size))
    }
}

impl fmt::Display for ExtendedKeyUsageExtension {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Purpose: {:?}", self.key_purpose)
    }
}

impl TryFrom<JSON> for ExtendedKeyUsageExtension {
    type Error = JSONError;

    fn try_from(value: JSON) -> Result<Self, Self::Error> {
        let key_purpose = value.resolve_into_iter()?
            .map(|item| item.resolve_into_string()
                .map(|item|KeyPurpose::from_str(&item).map_err(|_|JSONErrorKind::Resolve))
            )
            .collect::<Result<Vec<_>, _>>()?
            .into_iter()
            .collect::<Result<Box<[_]>, _>>()?;

        Ok(ExtendedKeyUsageExtension {
            key_purpose
        })
    }
}


