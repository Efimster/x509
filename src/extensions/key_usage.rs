use std::fmt;
use crate::asn1::encode;
use crate::asn1::decode;
use crate::asn1::decode::DecodeFrom;
use crate::asn1::encode::EncodeTo;
use std::mem;
use json::error::JSONErrorKind;
use json::JSON;
use json::error::JSONError;
use std::str::FromStr;
use crate::x509_error::{X509Error, X509ErrorKind};

#[derive(Debug, Clone)]
pub struct KeyUsageExtension {
    key_usage:Box<[KeyUsage]>,
}

#[derive(Debug, Clone)]
pub enum KeyUsage{
    DigitalSignature,
    NonRepudiation,
    KeyEncipherment,
    DataEncipherment,
    KeyAgreement,
    KeyCertSign,
    CRLSign,
    EncipherOnly,
    DecipherOnly,
}

impl KeyUsage {
    pub fn from_byte(value:u8) -> KeyUsage{
        match value {
            0 => KeyUsage::DigitalSignature,
            1 => KeyUsage::NonRepudiation,
            2 => KeyUsage::KeyEncipherment,
            3 => KeyUsage::DataEncipherment,
            4 => KeyUsage::KeyAgreement,
            5 => KeyUsage::KeyCertSign,
            6 => KeyUsage::CRLSign,
            7 => KeyUsage::EncipherOnly,
            _ => KeyUsage::DecipherOnly,
        }
    }

    pub fn to_byte(&self) -> u8 {
        match self {
            KeyUsage::DigitalSignature => 0,
            KeyUsage::NonRepudiation => 1,
            KeyUsage::KeyEncipherment => 2,
            KeyUsage::DataEncipherment => 3,
            KeyUsage::KeyAgreement => 4,
            KeyUsage::KeyCertSign => 5,
            KeyUsage::CRLSign => 6,
            KeyUsage::EncipherOnly => 7,
            KeyUsage::DecipherOnly => 8,
        }
    }
}

impl FromStr for KeyUsage {
    type Err = X509Error;

    fn from_str(s: &str) -> Result<KeyUsage, X509Error> {
        match s {
            "digitalSignature" => Ok(KeyUsage::DigitalSignature),
            "nonRepudiation" => Ok(KeyUsage::NonRepudiation),
            "keyEncipherment" => Ok(KeyUsage::KeyEncipherment),
            "dataEncipherment" => Ok(KeyUsage::DataEncipherment),
            "keyAgreement" => Ok(KeyUsage::KeyAgreement),
            "keyCertSign" => Ok(KeyUsage::KeyCertSign),
            "cRLSign" => Ok(KeyUsage::CRLSign),
            "encipherOnly" => Ok(KeyUsage::EncipherOnly),
            "decipherOnly" => Ok(KeyUsage::DecipherOnly),
            _ => Err(X509ErrorKind::Decode.into()),
        }
    }
}

impl EncodeTo for KeyUsageExtension {
    fn encode(&self) -> Box<[u8]> {
        let mut result:u16 = 0;
        for key in self.key_usage.iter() {
            result |= 0x8000 >> key.to_byte();
        }
        let result:[u8;2] = unsafe {mem::transmute(result)};
        let mut result = &result as &[u8];
        while result.len() > 1 && result[0] == 0 {
            result = &result[1..];
        }
        encode::encode_bit_string(result)
    }
}

impl DecodeFrom for KeyUsageExtension {
    fn decode(bytes: &[u8]) -> Result<(KeyUsageExtension, usize), X509Error> {

        let (bytes, total_encoded_size, _) = decode::decode_bit_string(bytes)?;
        let mut usages:Vec<KeyUsage> = Vec::with_capacity(9);
        let mut mask:u8 = 0x80;
        for i in 0usize ..= 7 {
            if bytes[0] & mask != 0 {
                usages.push(KeyUsage::from_byte(i as u8));
            }
            mask >>= 1;
        }
        if bytes.len() > 1 && bytes[1] & 0x80 != 0 {
            usages.push(KeyUsage::DecipherOnly);
        }
        Ok((KeyUsageExtension { key_usage: usages.into_boxed_slice()}, total_encoded_size))
    }
}

impl fmt::Display for KeyUsageExtension {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Usage: {:?}", self.key_usage)
    }
}

impl TryFrom<JSON> for KeyUsageExtension{
    type Error = JSONError;

    fn try_from(value: JSON) -> Result<Self, Self::Error> {
        let key_usage = value.resolve_into_iter()?
            .map(|item |item.resolve_into_string())
            .collect::<Result<Vec<_>, _>>()?
            .into_iter()
            .map(|item|KeyUsage::from_str(&item).map_err(|_|JSONErrorKind::Resolve))
            .collect::<Result<Box<[_]>, _>>()?;

        Ok(KeyUsageExtension{
            key_usage
        })
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_encode(){
        let extension = KeyUsageExtension {
            key_usage: Box::from([KeyUsage::DigitalSignature, KeyUsage::KeyCertSign, KeyUsage::CRLSign])
        };

        let encoded:&[u8] = &extension.encode();
        assert_eq!(encoded[3], 0x86);
    }


}