use crate::asn1::encode::EncodeTo;
use crate::asn1::encode;
use crate::asn1::decode::DecodeFrom;
use crate::x509_error::X509Error;
use crate::asn1::decode;
use crate::universal_type::UniversalType;
use crate::extensions::authority_key_id::AuthorityKeyIdentifierExtension;
use std::fmt;
use crate::extensions::subject_key_id::SubjectKeyIdentifierExtension;
use crate::extensions::key_usage::KeyUsageExtension;
use crate::extensions::basic_constraints::BasicConstraintsExtension;
use json::JSON;
use json::error::JSONError;
use crate::extensions::certificate_policies::CertificatePoliciesExtension;
use crate::oid::ObjectIdentifier;
use crate::oid;
use crate::display;
use crate::extensions::extended_key_usage::ExtendedKeyUsageExtension;
use crate::extensions::subject_alternative_names::SubjectAlternativeNamesExtension;
use crate::extensions::issuer_alternative_name::IssuerAlternativeNamesExtension;


#[derive(Debug, Clone)]
pub struct Extension {
    pub id: ObjectIdentifier,
    pub critical: bool,
    pub value: StandardExtension,
}

impl EncodeTo for Extension {
    fn encode(&self) -> Box<[u8]> {
        let mut result = encode::encode_object_identifier(&self.id).into_vec();
        result.extend_from_slice(&encode::encode_bool(self.critical));
        let standard_extension:Box<[u8]> = match self.value {
            StandardExtension::AuthorityKeyIdentifier(ref authority_key_identifier) => authority_key_identifier.encode(),
            StandardExtension::SubjectKeyIdentifier(ref subject_key_identifier) => subject_key_identifier.encode(),
            StandardExtension::KeyUsage(ref key_usage) => key_usage.encode(),
            StandardExtension::Basic(ref constraints) => constraints.encode(),
            StandardExtension::CertificatePolicies(ref policies) => policies.encode(),
            StandardExtension::ExtendedKeyUsage(ref extended_key_usage) => extended_key_usage.encode(),
            StandardExtension::SubjectAlternativeName(ref alternative_names) => alternative_names.encode(),
            StandardExtension::IssuerAlternativeName(ref alternative_names) => alternative_names.encode(),
            StandardExtension::Unknown{ref bytes} =>  bytes.clone(),
        };
        result.extend_from_slice(&encode::encode_octet_string(&standard_extension));
        encode::encode_sequence(&result)
    }
}

impl DecodeFrom for Extension {
    fn decode(bytes: &[u8]) -> Result<(Extension, usize), X509Error> {
        let (bytes, total_encoded_size) = decode::uncover_sequence(bytes)?;
        let (id, _, bytes) = decode::decode_object_identifier(bytes)?;
        let (_, _, tag_number, _) = decode::decode_tag_identifier(bytes)?;
        let (critical, _, bytes) = if UniversalType::from_u64(tag_number)? == UniversalType::Boolean {
            decode::decode_bool(bytes)?
        }
        else {
            (false, 0, bytes)
        };
        let (bytes, _, _) = decode::decode_octet_string(bytes)?;
        let standard_extension = match &id as &[u64] {
            oid::AUTHORITY_KEY_IDENTIFIER   => {
                let (extension, _) = AuthorityKeyIdentifierExtension::decode(&bytes)?;
                StandardExtension::AuthorityKeyIdentifier(extension)
            },
            oid::SUBJECT_KEY_IDENTIFIER => {
                let (extension, _) = SubjectKeyIdentifierExtension::decode(&bytes)?;
                StandardExtension::SubjectKeyIdentifier(extension)
            },
            oid::KEY_USAGE => {
                let (extension, _) = KeyUsageExtension::decode(&bytes)?;
                StandardExtension::KeyUsage(extension)
            },
            oid::BASIC_CONSTRAINTS => {
                let (extension, _) = BasicConstraintsExtension::decode(&bytes)?;
                StandardExtension::Basic(extension)
            },
            oid::CERTIFICATE_POLICIES => {
                let (extension, _) = CertificatePoliciesExtension::decode(&bytes)?;
                StandardExtension::CertificatePolicies(extension)
            },
            oid::EXTENDED_KEY_USAGE => {
                let (extension, _) = ExtendedKeyUsageExtension::decode(&bytes)?;
                StandardExtension::ExtendedKeyUsage(extension)
            },
            oid::SUBJECT_ALTERNATIVE_NAME => {
                let (extension, _) = SubjectAlternativeNamesExtension::decode(&bytes)?;
                StandardExtension::SubjectAlternativeName(extension)
            },
            oid::ISSUER_ALTERNATIVE_NAME => {
                let (extension, _) = IssuerAlternativeNamesExtension::decode(&bytes)?;
                StandardExtension::IssuerAlternativeName(extension)
            },
            _ => StandardExtension::Unknown{ bytes: Box::from(bytes)}
        };
        Ok((Extension{
            id,
            critical,
            value: standard_extension,
        }, total_encoded_size))
    }
}

impl fmt::Display for Extension {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {

        write!(f, "\nExtension: {} \nCritical: {}\n", oid::oid_to_human_readable(&self.id),
               display::boolean_yes_no(self.critical))?;

        match self.value {
            StandardExtension::AuthorityKeyIdentifier(ref authority_key_identifier) => {
                write!(f, "{}", authority_key_identifier)
            },
            StandardExtension::SubjectKeyIdentifier(ref subject_key_identifier) => {
                write!(f, "{}", subject_key_identifier)
            },
            StandardExtension::KeyUsage(ref key_usage) => {
                write!(f, "{}", key_usage)
            },
            StandardExtension::Basic(ref constraints) => {
                write!(f, "{}", constraints)
            },
            StandardExtension::CertificatePolicies(ref policies) => {
                write!(f, "{}", policies)
            },
            StandardExtension::ExtendedKeyUsage(ref extended_key_usage) => {
                write!(f, "{}", extended_key_usage)
            },
            StandardExtension::SubjectAlternativeName(ref alternative_name) => {
                write!(f, "{}", alternative_name)
            },
            StandardExtension::IssuerAlternativeName(ref alternative_name) => {
                write!(f, "{}", alternative_name)
            },
            _ => Ok(())
        }
    }
}

impl TryFrom<JSON> for Extension {
    type Error = JSONError;

    fn try_from(value: JSON) -> Result<Self, Self::Error> {
        let mut props = value.resolve_into_object()?;
        let id = oid::from_json(JSON::remove_property(&mut props, "extnID")?)?;
        let critical = JSON::remove_property(&mut props, "critical")?.resolve_into_boolean()?;
        let value = JSON::remove_property(&mut props, "extnValue")?;
        let value = match &id as &[u64] {
            oid::KEY_USAGE => StandardExtension::KeyUsage(value.try_into()?),
            oid::BASIC_CONSTRAINTS => StandardExtension::Basic(value.try_into()?),
            oid::CERTIFICATE_POLICIES => StandardExtension::CertificatePolicies(value.try_into()?),
            oid::EXTENDED_KEY_USAGE => StandardExtension::ExtendedKeyUsage(value.try_into()?),
            oid::SUBJECT_ALTERNATIVE_NAME => StandardExtension::SubjectAlternativeName(value.try_into()?),
            _ => StandardExtension::Unknown { bytes: Box::from([]) }
        };

        Ok(Extension {
            id,
            critical,
            value,
        })
    }
}

#[derive(Debug, Clone)]
pub enum StandardExtension {
    Unknown{bytes:Box<[u8]>},
    AuthorityKeyIdentifier(AuthorityKeyIdentifierExtension),
    SubjectKeyIdentifier(SubjectKeyIdentifierExtension),
    KeyUsage(KeyUsageExtension),
    Basic(BasicConstraintsExtension),
    CertificatePolicies(CertificatePoliciesExtension),
    ExtendedKeyUsage(ExtendedKeyUsageExtension),
    SubjectAlternativeName(SubjectAlternativeNamesExtension),
    IssuerAlternativeName(IssuerAlternativeNamesExtension),
}
