use crate::asn1::decode::DecodeFrom;
use crate::x509_error::X509Error;
use crate::asn1::decode;
use crate::asn1::encode::EncodeTo;
use crate::asn1::encode;
use std::fmt;
use crate::display;

#[derive(Debug, Clone)]
pub struct SubjectKeyIdentifierExtension {
    pub key_identifier: Box<[u8]>,
}

impl EncodeTo for SubjectKeyIdentifierExtension {
    fn encode(&self) -> Box<[u8]> {
        encode::encode_octet_string(&self.key_identifier)
    }
}

impl DecodeFrom for SubjectKeyIdentifierExtension {
    fn decode(bytes: &[u8]) -> Result<(SubjectKeyIdentifierExtension, usize), X509Error> {
        let (key_identifier, total_encoded_size, _)= decode::decode_octet_string(bytes)?;
        Ok((SubjectKeyIdentifierExtension { key_identifier }, total_encoded_size))
    }
}

impl fmt::Display for SubjectKeyIdentifierExtension {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Key: {}", display::display_octets(&self.key_identifier, 20))
    }
}