use crate::asn1::encode::EncodeTo;
use crate::asn1::encode;
use crate::asn1::decode;
use crate::x509_error::X509Error;
use crate::asn1::decode::DecodeFrom;
use std::str;
use json::error::JSONErrorKind;
use json::JSON;
use json::error::JSONError;
use crate::asn1::tag_complexity::TagComplexity;
use std::fmt;
use crate::oid::ObjectIdentifier;
use crate::oid;

#[derive(Debug, Clone)]
pub struct CertificatePoliciesExtension {
    policies: Box<[PolicyInformation]>,
}

impl EncodeTo for CertificatePoliciesExtension {
    fn encode(&self) -> Box<[u8]> {
        let mut result = Vec::new();
        for policy_qualifier in self.policies.iter() {
            policy_qualifier.encode_to(&mut result);
        }
        encode::encode_sequence(&result)
    }
}

impl DecodeFrom for CertificatePoliciesExtension {
    fn decode(bytes: &[u8]) -> Result<(CertificatePoliciesExtension, usize), X509Error> {
        let (mut bytes, total_encoded_size) = decode::uncover_sequence(bytes)?;
        let mut policies = Vec::new();
        while bytes.len() > 0 {
            let (policy, size) = PolicyInformation::decode(bytes)?;
            policies.push(policy);
            bytes = &bytes[size..];
        }
        Ok((CertificatePoliciesExtension {
            policies:policies.into_boxed_slice(),
        },
            total_encoded_size,
        ))
    }
}

impl fmt::Display for CertificatePoliciesExtension {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for i in 0usize .. self.policies.len() {
            writeln!(f, "Policy # {} : {}", i+1,
                oid::oid_to_human_readable(&self.policies[i].policy_identifier))?;
            writeln!(f, "{}", &self.policies[i])?;
        }
        Ok(())
    }
}

impl TryFrom<JSON> for CertificatePoliciesExtension {
    type Error = JSONError;

    fn try_from(value: JSON) -> Result<Self, Self::Error> {
        let policies = value.resolve_into_iter()?
            .map(|info| info.try_into())
            .collect::<Result<Box<[_]>, _>>()?;

        Ok(CertificatePoliciesExtension {
            policies
        })
    }
}

#[derive(Debug, Clone)]
pub struct PolicyInformation {
    policy_identifier: ObjectIdentifier,
    policy_qualifiers: Box<[PolicyQualifierInfo]>,
}

impl EncodeTo for PolicyInformation {
    fn encode(&self) -> Box<[u8]> {
        let mut result = encode::encode_object_identifier(&self.policy_identifier).to_vec();
        if self.policy_qualifiers.len() > 0 {
            let mut policy_qualifiers = Vec::new();
            for policy_qualifier in self.policy_qualifiers.iter() {
                policy_qualifier.encode_to(&mut policy_qualifiers);
            }
            result.extend_from_slice(&encode::encode_sequence(&policy_qualifiers));
        }

        encode::encode_sequence(&result)
    }
}

impl DecodeFrom for PolicyInformation {
    fn decode(bytes: &[u8]) -> Result<(PolicyInformation, usize), X509Error> {
        let (bytes, total_encoded_size) = decode::uncover_sequence(bytes)?;
        let (policy_identifier, _, bytes) = decode::decode_object_identifier(bytes)?;
        let policy_qualifiers = if bytes.len() > 0 {
            let (mut bytes, _) = decode::uncover_sequence(bytes)?;
            let mut policy_qualifiers = Vec::new();
            while bytes.len() > 0 {
                let (policy_qualifier, size) = PolicyQualifierInfo::decode(bytes)?;
                policy_qualifiers.push(policy_qualifier);
                bytes = &bytes[size ..];
            }
            policy_qualifiers.into_boxed_slice()
        }
        else {
            Box::new([])
        };

        Ok((PolicyInformation {
            policy_identifier,
            policy_qualifiers
        },
            total_encoded_size,
        ))
    }
}

impl fmt::Display for PolicyInformation {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for i in 0usize .. self.policy_qualifiers.len() {
            writeln!(f, "Qualifier # {} : {}", i+1, oid::oid_to_human_readable(&self.policy_qualifiers[i].policy_qualifier_id))?;
            write!(f, "{}",  &self.policy_qualifiers[i])?;
        }
        Ok(())
    }
}

impl TryFrom<JSON> for PolicyInformation {
    type Error = JSONError;

    fn try_from(value: JSON) -> Result<Self, Self::Error> {
        let mut props = value.resolve_into_object()?;
        let policy_identifier = props.remove("policyIdentifier").ok_or(JSONErrorKind::Resolve)?;
        let policy_identifier = oid::from_json(policy_identifier)?;
        let policy_qualifiers = match props.remove("policyQualifiers") {
            Some(json) => json.resolve_into_iter()?
                .map(|json|json.try_into())
                .collect::<Result<Box<[_]>, JSONError>>()?,
            None => Box::new([]),
        };

        Ok(PolicyInformation {
            policy_identifier,
            policy_qualifiers,
        })
    }
}

#[derive(Debug, Clone)]
pub struct PolicyQualifierInfo {
    policy_qualifier_id: ObjectIdentifier,
    qualifier: Qualifier,
}

#[derive(Debug, Clone)]
pub enum Qualifier {
    CPSuri(String),
    UserNotice(UserNotice),
}

impl EncodeTo for PolicyQualifierInfo {
    fn encode(&self) -> Box<[u8]> {
        let mut result = encode::encode_object_identifier(&self.policy_qualifier_id).to_vec();
        let qualifier = match self.qualifier {
            Qualifier::CPSuri(ref uri) => encode::encode_ia5_string(uri.as_bytes()),
            Qualifier::UserNotice(ref notice) => notice.encode(),
        };
        result.extend_from_slice(&qualifier);
        encode::encode_sequence(&result)
    }
}

impl DecodeFrom for PolicyQualifierInfo {
    fn decode(bytes: &[u8]) -> Result<(PolicyQualifierInfo, usize), X509Error> {
        let (bytes, total_encoded_size) = decode::uncover_sequence(bytes)?;
        let (policy_qualifier_id, _, bytes) = decode::decode_object_identifier(bytes)?;
        let qualifier = match decode::decode_tag_identifier(bytes)? {
            (_, ref tag_complexity, _, _) if tag_complexity == &TagComplexity::Constructed => {
                let (notice, _) = UserNotice::decode(bytes)?;
                Qualifier::UserNotice(notice)
            },
            _ => {
                let (uri, _, _) = decode::decode_ia5_string(bytes)?;
                Qualifier::CPSuri(str::from_utf8(&uri)?.to_string())
            },
        };
        Ok((PolicyQualifierInfo {
            policy_qualifier_id,
            qualifier
        },
            total_encoded_size,
        ))
    }
}

impl fmt::Display for PolicyQualifierInfo {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self.qualifier {
            Qualifier::CPSuri(ref uri) => write!(f, "CPS Uri: {}", uri),
            Qualifier::UserNotice(ref notice) => write!(f, "{}", notice),
        }
    }
}

impl TryFrom<JSON> for PolicyQualifierInfo {
    type Error = JSONError;

    fn try_from(value: JSON) -> Result<Self, Self::Error> {
        let mut props = value.resolve_into_object()?;
        let policy_qualifier_id = props.remove("policyQualifierId").ok_or(JSONErrorKind::Resolve)?;
        let policy_qualifier_id = oid::from_json(policy_qualifier_id)?;
        let qualifier = match props.remove("cPSuri") {
            Some(json) => Qualifier::CPSuri(json.resolve_into_string()?),
            None => Qualifier::UserNotice(props.remove( "userNotice").ok_or(JSONErrorKind::Resolve)?.try_into()?),
        };
        Ok(PolicyQualifierInfo {
            policy_qualifier_id,
            qualifier,
        })
    }
}

#[derive(Debug, Clone)]
pub struct UserNotice {
    notice_ref: Option<NoticeReference>,
    explicit_text: Option<String>,
}

impl EncodeTo for UserNotice {
    fn encode(&self) -> Box<[u8]> {

        let mut result = Vec::new();
        if let Some(ref notice_ref) = self.notice_ref {
            notice_ref.encode_to(&mut result);
        }
        if let Some(ref explicit_text) = self.explicit_text {
            result.extend_from_slice(&encode::encode_utf8_string(explicit_text.as_bytes()));
        }

        encode::encode_sequence(&result)
    }
}

impl DecodeFrom for UserNotice {
    fn decode(bytes: &[u8]) -> Result<(UserNotice, usize), X509Error> {
        let (mut bytes, total_encoded_size) = decode::uncover_sequence(bytes)?;
        let notice_ref = match decode::decode_tag_identifier(bytes)? {
            (_, ref tag_complexity, _, _) if tag_complexity == &TagComplexity::Constructed => {
                let (notice_ref, size) = NoticeReference::decode(bytes)?;
                bytes = &bytes[size..];
                Some(notice_ref)
            },
            _ => None,
        };
        let explicit_text = if bytes.len() > 0 {
            let (text, _, _) = decode::decode_string(bytes)?;
            let text = str::from_utf8(&text)?;
            Some(text.to_string())
        }
        else {
            None
        };
        Ok((UserNotice {
            notice_ref,
            explicit_text
        },
            total_encoded_size,
        ))
    }
}

impl fmt::Display for UserNotice {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if let Some(ref notice_ref) = self.notice_ref {
            writeln!(f, "User Notice: {}, ", notice_ref)?;
        }
        if let Some(ref explicit_text) = self.explicit_text {
            writeln!(f, "User Notice: {}", explicit_text)?;
        }

        Ok(())
    }
}

impl TryFrom<JSON> for UserNotice {
    type Error = JSONError;

    fn try_from(value: JSON) -> Result<Self, Self::Error> {
        let mut props = value.resolve_into_object()?;
        let notice_ref = match props.remove( "noticeRef"){
            Some(json) => Some(json.try_into()?),
            None => None
        };
        let explicit_text = match props.remove( "explicitText"){
            Some(json) => Some(json.resolve_into_string()?),
            None => None,
        };

        Ok(UserNotice {
            notice_ref,
            explicit_text,
        })
    }
}

#[derive(Debug, Clone)]
pub struct NoticeReference {
    organization: String,
    notice_numbers: Box<[u64]>,
}

impl EncodeTo for NoticeReference {
    fn encode(&self) -> Box<[u8]> {
        let mut result = encode::encode_utf8_string(self.organization.as_bytes()).to_vec();
        let mut numbers: Vec<u8> = Vec::new();
        for number in  self.notice_numbers.iter() {
            numbers.extend_from_slice(&encode::encode_integer(*number as i64));
        }
        result.extend_from_slice(&encode::encode_sequence(&numbers));
        encode::encode_sequence(&result)
    }
}

impl DecodeFrom for NoticeReference {
    fn decode(bytes: &[u8]) -> Result<(NoticeReference, usize), X509Error> {
        let (bytes, total_encoded_size) = decode::uncover_sequence(bytes)?;
        let (organization, _, mut bytes) = decode::decode_string(bytes)?;
        let mut numbers:Vec<u64> = Vec::new();
        while bytes.len() > 0 {
            let (number, size, _ ) = decode::decode_integer(bytes)?;
            numbers.push(number as u64);
            bytes = &bytes[size ..];
        }

        Ok((NoticeReference {
            organization: str::from_utf8(&organization)?.to_string(),
            notice_numbers: numbers.into_boxed_slice(),
        },
            total_encoded_size,
        ))
    }
}

impl fmt::Display for NoticeReference {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Organization: {}, Notice numbers: {:?}", self.organization, self.notice_numbers)
    }
}

impl TryFrom<JSON> for NoticeReference{
    type Error = JSONError;

    fn try_from(value: JSON) -> Result<Self, Self::Error> {
        let mut props = value.resolve_into_object()?;
        let organization = JSON::remove_property(&mut props, "organization")?.resolve_into_string()?;
        let notice_numbers = props.remove( "noticeNumbers")
            .ok_or(JSONErrorKind::Resolve)?
            .resolve_into_iter()?
            .map(|item|item.resolve_into_number())
            .map(|item|item.map(|item|item as u64))
            .collect::<Result<Box<[u64]>, JSONError>>()?;

        Ok(NoticeReference {
            organization,
            notice_numbers,
        })
    }
}