use crate::asn1::decode;
use crate::x509_error::X509Error;
use crate::asn1::encode;
use crate::asn1::encode::EncodeTo;
use crate::asn1::decode::DecodeFrom;
use std::fmt;
use crate::display;
use json::JSON;
use json::error::JSONError;

#[derive(Debug, Clone)]
pub struct BasicConstraintsExtension {
    certificate_authority: bool,
    path_len_constraint: Option<usize>,
}

impl EncodeTo for BasicConstraintsExtension {
    fn encode(&self) -> Box<[u8]> {
        let mut result = encode::encode_bool(self.certificate_authority).to_vec();
        if let Some(value) = self.path_len_constraint {
            result.extend_from_slice(&encode::encode_integer(value as i64));
        }
        encode::encode_sequence(&result)
    }
}

impl DecodeFrom for BasicConstraintsExtension {
    fn decode(bytes: &[u8]) -> Result<(BasicConstraintsExtension, usize), X509Error> {
        let (bytes, total_encoded_size) = decode::uncover_sequence(bytes)?;
        let (certificate_authority, _, bytes) = if bytes.len() > 0 {
            decode::decode_bool(bytes)?
        }
        else {
            (false, 0, bytes)
        };
        let path_len_constraint = if bytes.len() > 0 {
            let (result, _, _) = decode::decode_integer(bytes)?;
            Some(result as usize)
        }
        else {
            None
        };

        Ok((BasicConstraintsExtension {
                certificate_authority,
                path_len_constraint,
            },
            total_encoded_size,
        ))
    }
}

impl fmt::Display for BasicConstraintsExtension {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "CA: {}", display::boolean_yes_no(self.certificate_authority))?;
        if let Some(value) = self.path_len_constraint {
            write!(f, "Max path length: {}", value)?;
        }

        Ok(())
    }
}

impl TryFrom<JSON> for BasicConstraintsExtension {
    type Error = JSONError;

    fn try_from(value: JSON) -> Result<Self, Self::Error> {
        let mut props = value.resolve_into_object()?;
        let certificate_authority = JSON::remove_property(&mut props, "cA")?.resolve_boolean()?;
        let path_len_constraint = match props.remove( "pathLenConstraint") {
            Some(JSON::JSONNumber(value)) => Some(value as usize),
            _ => None,
        };
        Ok(BasicConstraintsExtension{
            certificate_authority,
            path_len_constraint,
        })
    }
}