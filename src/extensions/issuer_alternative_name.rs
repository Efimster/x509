use crate::asn1::{encode, decode};
use crate::asn1::encode::EncodeTo;
use crate::general_name::GeneralName;
use crate::x509_error::X509Error;
use crate::asn1::decode::DecodeFrom;
use core::fmt;

#[derive(Debug, Clone)]
pub struct IssuerAlternativeNamesExtension {
    names: Box<[GeneralName]>,
}

impl EncodeTo for IssuerAlternativeNamesExtension {
    fn encode(&self) -> Box<[u8]> {
        let mut result = Vec::new();
        for i in 0..self.names.len() {
            self.names[i].encode_to(&mut result);
        }
        encode::encode_sequence(&result)
    }
}

impl DecodeFrom for IssuerAlternativeNamesExtension {
    fn decode(bytes: &[u8]) -> Result<(IssuerAlternativeNamesExtension, usize), X509Error> {
        let (mut bytes, total_encoded_size) = decode::uncover_sequence(bytes)?;
        let mut names = Vec::new();
        while bytes.len() > 0 {
            let (name, size) = GeneralName::decode(bytes)?;
            names.push(name);
            bytes = &bytes[size..];
        }
        Ok((IssuerAlternativeNamesExtension { names: names.into_boxed_slice() }, total_encoded_size))
    }
}

impl fmt::Display for IssuerAlternativeNamesExtension {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let tab = "\t".repeat(f.width().unwrap_or_default());
        for name in self.names.iter() {
            f.write_str(&format!("{}{}, ", tab, name))?;
        }
        Ok(())
    }
}