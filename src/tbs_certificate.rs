use crate::algorithm::AlgorithmIdentifier;
use crate::attribute::Name;
use crate::asn1::encode::EncodeTo;
use crate::asn1::encode;
use crate::extensions::extension::Extension;
use crate::asn1::tag_class::TagClass;
use crate::asn1::tag_complexity::TagComplexity;
use crate::validity::Validity;
use crate::asn1::decode::DecodeFrom;
use crate::x509_error::{X509Error, X509ErrorKind};
use crate::asn1::decode;
use std::fmt;
use crate::display;
use crate::public_key_info::SubjectPublicKeyInfo;
use json::JSON;
use json::error::JSONError;
use crate::version::Version;
use crate::utils::transmute;

#[derive(Debug)]
pub struct TbsCertificate {
    pub version: Version,
    pub serial_number: Box<[u8]>,
    pub signature: AlgorithmIdentifier,
    pub issuer: Name,
    pub validity: Validity,
    pub subject: Name,
    pub subject_public_key_info: SubjectPublicKeyInfo,
    pub issuer_unique_id: Box<[u8]>,
    pub subject_unique_id: Box<[u8]>,
    pub extensions: Box<[Extension]>,
    pub der_encoded: Box<[u8]>,
}

impl EncodeTo for TbsCertificate {
    fn encode(&self) -> Box<[u8]> {
        let mut result: Vec<u8> = Vec::new();
        result.extend_from_slice(&encode::encode_explicit(0, &self.version.encode()));
        result.extend_from_slice(&encode::encode_big_unsigned_integer(&self.serial_number));
        self.signature.encode_to(&mut result);
        self.issuer.encode_to(&mut result);
        self.validity.encode_to(&mut result);
        self.subject.encode_to(&mut result);
        self.subject_public_key_info.encode_to(&mut result);

        if self.issuer_unique_id.len() > 0 {
            result.extend_from_slice(&encode::encode_bytes(TagClass::ContextSpecific,
TagComplexity::Primitive, 1u64, &self.issuer_unique_id));
        }

        if self.subject_unique_id.len() > 0 {
            result.extend_from_slice(&encode::encode_bytes(TagClass::ContextSpecific,
TagComplexity::Primitive, 2u64, &self.subject_unique_id));
        }

        let mut extensions: Vec<u8> = Vec::new();
        for extension in self.extensions.iter() {
            extension.encode_to(&mut extensions);
        }

        if extensions.len() > 0 {
            let extensions = encode::encode_sequence(&extensions);
            let extensions = encode::encode_bytes(TagClass::ContextSpecific, TagComplexity::Constructed,
        3u64, &extensions);
            result.extend_from_slice(&extensions);
        }

        encode::encode_sequence(&result)
    }
}

impl DecodeFrom for TbsCertificate {
    fn decode(bytes: &[u8]) -> Result<(TbsCertificate, usize), X509Error> {
        let data = bytes;

        let (bytes, total_encoded_size) = decode::uncover_sequence(bytes)?;
        let der_encoded:Box<[u8]> = Box::from(&data[..total_encoded_size]);
        let (tag_class, _, tag_number, _) = decode::decode_tag_identifier(bytes)?;
        let (version, bytes) = if tag_class == TagClass::ContextSpecific && tag_number == 0 {
            let (bytes, _) = decode::uncover_explicit(bytes, 0)?;
            Version::decode_cut(bytes)?
        }
        else {
            (Version::V1, bytes)
        };

        let (serial_number, _, bytes) = decode::decode_big_integer(bytes)?;
        let (signature, bytes) = AlgorithmIdentifier::decode_cut(bytes)?;
        let (issuer, bytes) = Name::decode_cut(bytes)?;
        let (validity, bytes) = Validity::decode_cut(bytes)?;
        let (subject, bytes) = Name::decode_cut(bytes)?;
        let (subject_public_key_info, mut bytes) = SubjectPublicKeyInfo::decode_cut(bytes)?;
        let (issuer_unique_id, encoded_size) = TbsCertificate::decode_issuer_unique_id(bytes)?;
        bytes = &bytes[encoded_size..];
        let (subject_unique_id, encoded_size) = Self::decode_subject_unique_id(bytes)?;
        bytes = &bytes[encoded_size..];

        let (extensions, _) = Self::decode_extensions(bytes)?;

        Ok((TbsCertificate {
            version,
            serial_number,
            signature,
            issuer,
            validity,
            subject,
            subject_public_key_info,
            issuer_unique_id,
            subject_unique_id,
            extensions,
            der_encoded,
        }, total_encoded_size))
    }
}

impl TbsCertificate {
    fn decode_issuer_unique_id(bytes:&[u8]) -> Result<(Box<[u8]>, usize), X509Error>{
        if bytes.len() == 0 {
            return Ok((Box::new([]), 0));
        }

        let (tag_class, _, tag_number, length, encoded_size) = decode::decode_tag_and_length(bytes)?;
        if tag_class == TagClass::ContextSpecific  && tag_number == 1u64 {
            let total_encoded_size = encoded_size + length;
            Ok(((&bytes[encoded_size..total_encoded_size]).to_vec().into_boxed_slice(), total_encoded_size))
        }
        else {
            Ok((Box::new([]), 0))
        }
    }

    fn decode_subject_unique_id(bytes: &[u8]) -> Result<(Box<[u8]>, usize), X509Error> {
        if bytes.len() == 0 {
            return Ok((Box::new([]), 0));
        }

        let (tag_class, _, tag_number, length, encoded_size) = decode::decode_tag_and_length(bytes)?;
        if tag_class == TagClass::ContextSpecific && tag_number == 2u64 {
            let total_encoded_size = encoded_size + length;
            Ok(((&bytes[encoded_size..total_encoded_size]).to_vec().into_boxed_slice(), total_encoded_size))
        } else {
            Ok((Box::new([]), 0))
        }
    }

    fn decode_extensions(bytes: &[u8]) -> Result<(Box<[Extension]>, usize), X509Error> {
        if bytes.len() == 0 {
            return Ok((Box::new([]), 0));
        }

        let (tag_class, tag_complexity, tag_number, length, encoded_size) = decode::decode_tag_and_length(bytes)?;
        if tag_class != TagClass::ContextSpecific || tag_number != 3 || tag_complexity != TagComplexity::Constructed {
            return Err(X509ErrorKind::Decode.into())
        }

        let total_encoded_size = encoded_size + length;
        let (mut bytes, _) = decode::uncover_sequence(&bytes[encoded_size..total_encoded_size])?;
        let mut result:Vec<Extension> = Vec::new();
        while bytes.len() > 0 {
            let (extension, encoded_size) = Extension::decode(bytes)?;
            result.push(extension);
            bytes = &bytes[encoded_size .. ];
        }

        Ok((result.into_boxed_slice(), total_encoded_size))
    }
}

impl fmt::Display for TbsCertificate {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Version: {:?}
Serial number: {}
Issuer:\n {:<1}
Subject:\n {:<1}
Validity:\n {:<1}
Signature:\n {:<1}
Public key data:\n{:<1}
Issuer unique Id: {}
Subject unique Id: {}",

            self.version,
            display::display_octets(&self.serial_number, 20),
            self.issuer,
            self.subject,
            self.validity,
            self.signature,
            self.subject_public_key_info,
            display::display_octets(&self.issuer_unique_id, 20),
            display::display_octets(&self.subject_unique_id, 20),
        )?;

        for extension in self.extensions.iter() {
            f.write_str(&format!("\n{}", extension))?;
        }

        Ok(())
    }
}

impl TryFrom<JSON> for TbsCertificate{
    type Error = JSONError;

    fn try_from(value: JSON) -> Result<Self, Self::Error> {
        let mut props = value.resolve_into_object()?;
        let version = JSON::remove_property(&mut props, "version")?.try_into()?;
        let serial_number = JSON::remove_property(&mut props, "serialNumber")?.resolve_into_number()? as u64;
        let signature = JSON::remove_property(&mut props, "signature")?.try_into()?;
        let issuer = JSON::remove_property(&mut props, "issuer")?.try_into()?;
        let validity = JSON::remove_property(&mut props, "validity")?.try_into()?;
        let subject = JSON::remove_property(&mut props, "subject")?.try_into()?;
        let subject_public_key_info = JSON::remove_property(&mut props, "subjectPublicKeyInfo")?.try_into()?;
        let issuer_unique_id:Box<[u8]> = match props.remove( "issuerUniqueID"){
            None => Box::new([]),
            Some(json) => transmute::u64_to_bytes_be(json.resolve_into_number()? as u64),
        };
        let subject_unique_id: Box<[u8]> = match props.remove( "subjectUniqueID"){
            None => Box::new([]),
            Some(json) => transmute::u64_to_bytes_be(json.resolve_into_number()? as u64),
        };
        let extensions = match props.remove( "extensions"){
            None => Box::new([]),
            Some(json) => json.resolve_into_iter()?
                .map(|json|json.try_into())
                .collect::<Result<Box<[Extension]>, JSONError>>()?
        };
        Ok(TbsCertificate{
            version,
            serial_number: transmute::u64_to_bytes_be(serial_number),
            signature,
            issuer,
            validity,
            subject,
            subject_public_key_info,
            issuer_unique_id,
            subject_unique_id,
            extensions,
            der_encoded: Box::new([])
        })
    }
}






