use crate::asn1::encode::EncodeTo;
use crate::asn1::encode;
use crate::asn1::decode::DecodeFrom;
use crate::asn1::decode;
use crate::x509_error::X509Error;
use std::fmt;
use crate::display;
use std::str;
use json::error::{JSONError, JSONErrorKind};
use json::JSON;
use crate::oid::ObjectIdentifier;
use crate::oid;

//const OID_RSA_ENCRYPTION:[u64; 7] = [1, 2, 840, 113549, 1, 1, 1];
//const OID_SHA256WITH_RSA_ENCRYPTION: [u64; 7] = [1, 2, 840, 113549, 1, 1, 11];
//const OID_COMMON_NAME: [u64; 4] = [2, 5, 4, 3];
//const OID_SERIAL_NUMBER: [u64; 4] = [2, 5, 4, 5];
//const OID_COUNTRY_OR_REGION: [u64; 4] = [2, 5, 4, 6];
//const OID_ORGANIZATION: [u64; 4] = [2, 5, 4, 10];
//const OID_ORGANIZATION_UNIT: [u64; 4] = [2, 5, 4, 11];

#[derive(Debug, PartialEq, Clone)]
pub struct Attribute {
    pub attribute_type: ObjectIdentifier,
    pub value:Box<[u8]>
}

impl EncodeTo for Attribute {
    fn encode(&self) -> Box<[u8]>{
        let mut result = encode::encode_object_identifier(&self.attribute_type).into_vec();
        result.extend_from_slice(&encode::encode_utf8_string(&self.value));
        encode::encode_sequence(&result)
    }
}

impl  DecodeFrom for Attribute {
    fn decode(bytes: &[u8]) -> Result<(Self, usize), X509Error> where Self: std::marker::Sized {
        let (bytes, total_encoded_size) = decode::uncover_sequence(bytes)?;
        let (attribute_type, _, bytes) = decode::decode_object_identifier(bytes)?;

        let (value, _, _) = decode::decode_string(bytes)?;
        Ok((Attribute{
            attribute_type,
            value,
        }, total_encoded_size))
    }
}

impl fmt::Display for Attribute {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let width = f.width().unwrap_or_default();
        let id = oid::oid_to_human_readable(&self.attribute_type);
        let value = match str::from_utf8(&self.value) {
            Ok(value) => value.to_string(),
            Err(_) => display::display_octets(&self.value, 15),
        };
        f.write_str(&format!("{}{}: {}, ", "\t".repeat(width), id, value))
    }
}

impl TryFrom<JSON> for Attribute {
    type Error = JSONError;

    fn try_from(value: JSON) -> Result<Self, Self::Error> {
        let mut props = value.resolve_into_object()?;
        let attribute_type = props.remove("type").ok_or(JSONErrorKind::Resolve)?;
        let attribute_type = oid::from_json(attribute_type)?;
        let value = props.remove("value").ok_or(JSONErrorKind::Resolve)?.resolve_into_string()?;
        Ok(Attribute {
            attribute_type,
            value: Box::from(value.as_bytes()),
        })
    }
}

pub type Name = RDNSequence;

#[derive(Debug, PartialEq, Clone)]
pub struct RDNSequence {
    pub rdn_sequence: Box<[RelativeDistinguishedName]>
}

impl EncodeTo for RDNSequence {
    fn encode(&self) -> Box<[u8]>{
        let mut result: Vec<u8> = Vec::new();
        for rdn in self.rdn_sequence.iter() {
            rdn.encode_to(&mut result);
        }
        encode::encode_sequence(&result)
    }
}

impl DecodeFrom for RDNSequence {
    fn decode(bytes: &[u8]) -> Result<(Self, usize), X509Error> where Self: std::marker::Sized {
        let (mut bytes, total_encoded_size) = decode::uncover_sequence(bytes)?;
        let mut rdn_sequence: Vec<RelativeDistinguishedName> = Vec::new();
        while bytes.len() > 0 {
            let (rdn, encoded_size) = RelativeDistinguishedName::decode(bytes)?;
            rdn_sequence.push(rdn);
            bytes = &bytes[encoded_size..];
        }
        Ok((RDNSequence {
            rdn_sequence: rdn_sequence.into_boxed_slice(),
        }, total_encoded_size))
    }
}

impl fmt::Display for RDNSequence {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let width = f.width().unwrap_or_default();
        for rdn in self.rdn_sequence.iter() {
            f.write_str(&format!("{}{}", "\t".repeat(width), rdn))?;
        }
        Ok(())
    }
}

impl TryFrom<JSON> for RDNSequence {
    type Error = JSONError;

    fn try_from(value: JSON) -> Result<Self, Self::Error> {
        let rdn_sequence = value.resolve_into_iter()?
            .map(|item| item.try_into()).collect::<Result<Box<_>, _>>()?;

        Ok(RDNSequence { rdn_sequence })
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct RelativeDistinguishedName {
    pub attributes:Box<[Attribute]>,
}

impl EncodeTo for RelativeDistinguishedName {
    fn encode(&self) -> Box<[u8]> {
        let mut result: Vec<u8> = Vec::new();
        for attr in self.attributes.iter() {
            attr.encode_to(&mut result);
        }
        encode::encode_set(&result)
    }
}

impl DecodeFrom for RelativeDistinguishedName {
    fn decode(bytes: &[u8]) -> Result<(Self, usize), X509Error> where Self: std::marker::Sized {
        let (mut bytes, total_encoded_size) = decode::uncover_set(bytes)?;
        let mut attributes:Vec<Attribute> = Vec::new();
        while bytes.len() > 0 {
            let (attribute, encoded_size) = Attribute::decode(bytes)?;
            attributes.push(attribute);
            bytes = &bytes[encoded_size..];
        }
        Ok((RelativeDistinguishedName{
            attributes: attributes.into_boxed_slice(),
        }, total_encoded_size))
    }
}

impl fmt::Display for RelativeDistinguishedName {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let width = f.width().unwrap_or_default();
        for attribute in self.attributes.iter() {
            f.write_str(&format!("{}{}\n", "\t".repeat(width), attribute))?;
        }
        Ok(())
    }
}

impl TryFrom<JSON> for RelativeDistinguishedName {
    type Error = JSONError;

    fn try_from(value: JSON) -> Result<Self, Self::Error> {
        let attributes = value.resolve_into_iter()?
            .map(|item|item.try_into()).collect::<Result<Box<_>, _>>()?;
        Ok(RelativeDistinguishedName{
            attributes
        })
    }
}




