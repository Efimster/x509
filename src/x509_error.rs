use core::fmt;
use std::error::Error;
use std::str::Utf8Error;
use std::num::ParseIntError;
use std::io;

use json::error::JSONError;

#[derive(Clone, Debug)]
pub enum X509ErrorKind {
    Encode,
    Decode,
    Verification,
    Sign,
    EllipticCurveMismatch,
    UnsupportedPublicKeyAlgorithm,
    UnsupportedSignatureAlgorithm,
    UnsupportedHashAlgorithm,
    EcdsaSignatureExpected,
    EcdsaPublicKeyExpected,
    EcdsaUnsupportedCurve,
    RsaSignatureExpected,
    RsaPublicKeyExpected,
    Inner
}

#[derive(Debug)]
pub struct X509Error {
    pub source:Option<Box<dyn Error>>,
    pub kind:X509ErrorKind,
}

impl X509Error {
    pub fn new_inner(source:Box<dyn Error>)->Self{
        X509Error {
            source: Some(source),
            kind: X509ErrorKind::Inner
        }
    }

    pub fn new_kind(kind:X509ErrorKind)->Self{
        X509Error {
            source: None,
            kind
        }
    }
}

impl Error for X509Error {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        self.source.as_deref()
    }
}

impl fmt::Display for X509ErrorKind {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &self {
            X509ErrorKind::Encode => write!(f, "x509 encode error"),
            X509ErrorKind::Decode => write!(f, "x509 decode error"), 
            X509ErrorKind::Verification => write!(f, "x509 verification error"), 
            X509ErrorKind::Sign => write!(f, "x509 sign error"), 
            X509ErrorKind::EllipticCurveMismatch => write!(f, "x509 elliptic curves don't match"), 
            X509ErrorKind::UnsupportedPublicKeyAlgorithm => write!(f, "x509 unsupported public key algorithm"), 
            X509ErrorKind::UnsupportedSignatureAlgorithm => write!(f, "x509 unsupported signature algorithm"), 
            X509ErrorKind::UnsupportedHashAlgorithm => write!(f, "x509 unsupported hash algorithm"), 
            X509ErrorKind::EcdsaSignatureExpected => write!(f, "x509 ecdsa signature is not found"),
            X509ErrorKind::EcdsaPublicKeyExpected => write!(f, "x509 ecdsa public key not found"),
            X509ErrorKind::EcdsaUnsupportedCurve => write!(f, "x509 unsupported ecdsa curve"),
            X509ErrorKind::RsaSignatureExpected => write!(f, "x509 rsa signature is not found"),
            X509ErrorKind::RsaPublicKeyExpected => write!(f, "x509 rsa public key is not found"),
            X509ErrorKind::Inner =>  write!(f, "error in X509Error::source"),
        } 
    }
}

impl fmt::Display for X509Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.kind)
    }
}

impl X509ErrorKind {
    pub fn into_boxed_error(self) -> Box<dyn Error> {
        let error:X509Error = self.into();
        error.into()
    }
}

impl From<X509ErrorKind> for X509Error {
    fn from(kind: X509ErrorKind) -> Self {
        Self::new_kind(kind)
    }
}

impl From<Box<dyn Error>> for X509Error {
    fn from(err: Box<dyn Error>) -> Self {
        Self::new_inner(err)
    }
}

impl From<Utf8Error> for X509Error {
    fn from(error: Utf8Error) -> Self {
        Self::new_inner(error.into())
    }
}

impl From<ParseIntError> for X509Error {
    fn from(error: ParseIntError) -> Self {
        Self::new_inner(error.into())
    }
}

impl From<JSONError> for X509Error {
    fn from(error: JSONError) -> Self {
        Self::new_inner(error.into())
    }
}

impl From<io::Error> for X509Error {
    fn from(error: io::Error) -> Self {
        Self::new_inner(error.into())
    }
}