use crate::asn1::encode::EncodeTo;
use crate::algorithm::AlgorithmIdentifier;
use crate::asn1::encode;
use crate::asn1::decode;
use crate::x509_error::X509Error;
use crate::asn1::decode::DecodeFrom;
use crate::x509_error::X509ErrorKind;
use std::fmt;
use crate::rsa::rsa_public_key::RSAPublicKey;
use json::JSON;
use json::error::JSONError;
use crate::public_key::PublicKey;
use ecc::point::EccPoint;

#[derive(Debug)]
pub struct SubjectPublicKeyInfo {
    pub algorithm: AlgorithmIdentifier,
    pub public_key: PublicKey,
}

impl EncodeTo for SubjectPublicKeyInfo {
    fn encode(&self) -> Box<[u8]> {
        let public_key = match &self.public_key {
            PublicKey::RSA(key) => key.encode(),
            PublicKey::ECDSA(point) => point.to_octet_string(false),
            PublicKey::None => Box::new([]),
        };
        let mut result = self.algorithm.encode().into_vec();

        result.extend_from_slice(&encode::encode_bit_string(&public_key));

        encode::encode_sequence(&result)
    }
}

impl DecodeFrom for SubjectPublicKeyInfo {
    fn decode(bytes: &[u8]) -> Result<(SubjectPublicKeyInfo, usize), X509Error> {
        let (bytes, total_encoded_size) = decode::uncover_sequence(bytes)?;
        let (algorithm, bytes) = AlgorithmIdentifier::decode_cut(bytes)?;
        let (subject_public_key, _, _) = decode::decode_bit_string(bytes)?;
        let public_key = match &algorithm.algorithm_id as &[u64] {
            [1, 2, 840, 113549, 1, 1, 1] => { PublicKey::RSA(RSAPublicKey::decode(&subject_public_key)?.0)},
            [1, 2, 840, 10045, 2, 1] => {
                let point = EccPoint::from_octet_string(&subject_public_key).ok_or(X509ErrorKind::Decode)?;
                PublicKey::ECDSA(point)
            },
            _ => PublicKey::None,
        };

        Ok((SubjectPublicKeyInfo {
            algorithm,
            public_key,
        }, total_encoded_size))
    }
}

impl fmt::Display for SubjectPublicKeyInfo {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
//        let tab = "\t".repeat(f.width().unwrap_or_default());
        f.write_str(&format!("{:<1}", self.algorithm))?;
        f.write_str(&format!("{:<1}", self.public_key))
    }
}

impl TryFrom<JSON> for SubjectPublicKeyInfo {
    type Error = JSONError;

    fn try_from(value: JSON) -> Result<Self, Self::Error> {
        let mut props = value.resolve_into_object()?;
        let algorithm = JSON::remove_property(&mut props, "algorithm")?.try_into()?;
        Ok(SubjectPublicKeyInfo {
            algorithm,
            public_key: PublicKey::None,
        })
    }
}