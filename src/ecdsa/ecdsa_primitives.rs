use ecc::domain_parameters::EccDomainParameters;
use ecc::point::EccPoint;
use bigint::BigInt;


pub fn ecdsa_generate_keys(domain_parameters: &EccDomainParameters, private_key_len_bits:usize, user_input: Option<&[u8]>) -> (BigInt, EccPoint){
    let private_key = &ecc::pseudorandom_number_generator::generate(
    &domain_parameters.point_prime_order, 1, private_key_len_bits, user_input)[0];

    let public_key = domain_parameters.base_point.scalar_multiplication_x9_602(
    private_key, &domain_parameters.prime_moduli, &domain_parameters.a, &domain_parameters.point_prime_order);

    (private_key.clone(), public_key)
}

pub fn ecdsa_public_key_validation(public_key: &EccPoint, domain_parameters: &EccDomainParameters) -> bool {
    match public_key {
        EccPoint::Infinity => false,
        EccPoint::XY {x, y} => {
            if x >= &domain_parameters.prime_moduli || y >= &domain_parameters.prime_moduli {
                return false;
            }
            let moduli = &domain_parameters.prime_moduli;
            let mu = &BigInt::mu_for_barret_reduction(moduli);
            let y_sqr_expected = x.modular_sqr_with_barret_reduction(moduli, mu).modular_multiplication_with_barret_reduction(&x, moduli, mu)
            +
            domain_parameters.a.modular_multiplication_with_barret_reduction(&x, moduli, mu)
            +
            &domain_parameters.b;
            if y.modular_sqr_with_barret_reduction(moduli, mu) != y_sqr_expected.barrett_reduction(moduli, mu) {
                return false;
            }
            public_key.scalar_multiplication_window(&domain_parameters.point_prime_order, moduli,
                &domain_parameters.a, &domain_parameters.point_prime_order) == EccPoint::Infinity
        }
    }
}

pub fn ecdsa_generate_secret_number_and_inverse(base_point_order:&BigInt, secret_key_len_bits:usize, user_input: Option<&[u8]>) -> (BigInt, BigInt) {
    loop {
        let k = &ecc::pseudorandom_number_generator::generate(base_point_order,
          1, secret_key_len_bits, user_input)[0];

        let k_inverse: BigInt = match k.modular_multiplicative_inverse(base_point_order) {
            Some(value) => value,
            None => continue,
        };

        return (k.clone(), k_inverse)
    }
}

pub fn ecdsa_sign(private_key:&BigInt, domain_parameters:&EccDomainParameters, message_digest_as_integer:&BigInt,
    secret_number:&BigInt, secret_number_inverse:&BigInt) -> (BigInt, BigInt){

    let prime_n = &domain_parameters.point_prime_order;
    let mu_n = &BigInt::mu_for_barret_reduction(prime_n);

    let k_point = domain_parameters.base_point.scalar_multiplication_x9_602(secret_number,
        &domain_parameters.prime_moduli, &domain_parameters.a, prime_n);

    let signature_r_part = match k_point {
        EccPoint::Infinity => BigInt::zero(),
        EccPoint::XY { x, y:_ } => x.barrett_reduction(prime_n, mu_n),
    };

    let mut signature_s_part = private_key.modular_multiplication_with_barret_reduction(&signature_r_part, prime_n, mu_n);
    signature_s_part = (message_digest_as_integer + &signature_s_part).barrett_reduction(prime_n, mu_n);
    signature_s_part = secret_number_inverse.modular_multiplication_with_barret_reduction(&signature_s_part, prime_n, mu_n);

    return (signature_r_part, signature_s_part);
}

pub fn ecdsa_sign_p(private_key: &BigInt, domain_parameters: &EccDomainParameters, message_digest_as_integer: &BigInt,
    secret_number: &BigInt, secret_number_inverse: &BigInt) -> (BigInt, BigInt)
{
    let prime_n = &domain_parameters.point_prime_order;
    let mu_n = &BigInt::mu_for_barret_reduction(prime_n);

    let k_point = domain_parameters.base_point.p_scalar_multiplication_x9_602(secret_number,
            &domain_parameters.prime_moduli, &domain_parameters.a, prime_n, &domain_parameters.reduction);

    let signature_r_part = match k_point {
        EccPoint::Infinity => BigInt::zero(),
        EccPoint::XY { x, y: _ } => x.barrett_reduction(prime_n, mu_n),
    };

    let mut signature_s_part = private_key.modular_multiplication_with_barret_reduction(&signature_r_part, prime_n, mu_n);
    signature_s_part = (message_digest_as_integer + &signature_s_part).barrett_reduction(prime_n, mu_n);
    signature_s_part = secret_number_inverse.modular_multiplication_with_barret_reduction(&signature_s_part, prime_n, mu_n);

    return (signature_r_part, signature_s_part);
}

pub fn ecdsa_verify(signature_r_part:&BigInt, signature_s_part:&BigInt, public_key: &EccPoint,
    domain_parameters: &EccDomainParameters, message_digest_as_integer: &BigInt) -> bool
{

    let prime_n = &domain_parameters.point_prime_order;
    let mu_n = &BigInt::mu_for_barret_reduction(prime_n);

    {
        let n_1 = prime_n - &BigInt::one();
        let zero = BigInt::zero();
        if signature_r_part == &zero || signature_r_part > &n_1 || signature_s_part == &zero || signature_s_part > &n_1 {
            return false;
        }
    }

    let c = match signature_s_part.modular_multiplicative_inverse(&domain_parameters.point_prime_order) {
        Some(value) => value,
        None => return false,
    };

    let u1 = message_digest_as_integer.modular_multiplication_with_barret_reduction(&c, prime_n, mu_n);
    let u2 = signature_r_part.modular_multiplication_with_barret_reduction(&c, prime_n, mu_n);
    let point_u1 = domain_parameters.base_point.scalar_multiplication_x9_602(&u1, &domain_parameters.prime_moduli,
    &domain_parameters.a, &domain_parameters.point_prime_order);
    let point_u2 = public_key.scalar_multiplication_x9_602(&u2, &domain_parameters.prime_moduli,
    &domain_parameters.a, &domain_parameters.point_prime_order);
    let v = match point_u1.add(&point_u2, &domain_parameters.prime_moduli,
    &BigInt::mu_for_barret_reduction(&domain_parameters.prime_moduli), &domain_parameters.a){
        EccPoint::Infinity => return false,
        EccPoint::XY {x, y:_} => x.barrett_reduction(prime_n, mu_n),
    };

    signature_r_part == &v
}

pub fn ecdsa_verify_p(signature_r_part: &BigInt, signature_s_part: &BigInt, public_key: &EccPoint,
    domain_parameters: &EccDomainParameters, message_digest_as_integer: &BigInt) -> bool
{
    let prime_n = &domain_parameters.point_prime_order;
    let mu_n = &BigInt::mu_for_barret_reduction(prime_n);

    {
        let n_1 = prime_n - &BigInt::one();
        let zero = BigInt::zero();
        if signature_r_part == &zero || signature_r_part > &n_1 || signature_s_part == &zero || signature_s_part > &n_1 {
            return false;
        }
    }

    let c = match signature_s_part.modular_multiplicative_inverse(&domain_parameters.point_prime_order) {
        Some(value) => value,
        None => return false,
    };
    let u1 = message_digest_as_integer.modular_multiplication_with_barret_reduction(&c, prime_n, mu_n);
    let u2 = signature_r_part.modular_multiplication_with_barret_reduction(&c, prime_n, mu_n);
    let point_u1 = domain_parameters.base_point.p_scalar_multiplication_x9_602(&u1, &domain_parameters.prime_moduli,
     &domain_parameters.a, &domain_parameters.point_prime_order, &domain_parameters.reduction);
    let point_u2 = public_key.p_scalar_multiplication_x9_602(&u2, &domain_parameters.prime_moduli,
    &domain_parameters.a, &domain_parameters.point_prime_order, &domain_parameters.reduction);
    let v = match point_u1.p_add(&point_u2, &domain_parameters.prime_moduli, &domain_parameters.a, &domain_parameters.reduction) {
        EccPoint::Infinity => return false,
        EccPoint::XY { x, y: _ } => x.barrett_reduction(prime_n, mu_n),
    };

    signature_r_part == &v
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::str::FromStr;

    #[test]
    fn test_sign_small() {
        let domain_parameters = EccDomainParameters {
            prime_moduli: BigInt::from(23u64),
            point_prime_order: BigInt::from(7u64),
            a: BigInt::one(),
            b: BigInt::one(),
            base_point: EccPoint::XY {x:BigInt::from(13u64), y:BigInt::from(7u64)},
            reduction: Box::new(BigInt::classic_reduction),
        };

        let private_key = BigInt::from(3u64);
        let public_key = domain_parameters.base_point.scalar_multiplication_double_and_add_decreasing(
            &private_key, &domain_parameters.prime_moduli, &domain_parameters.a, &domain_parameters.point_prime_order);

        assert_eq!(public_key, EccPoint::XY {
            x: BigInt::from(17u64),
            y: BigInt::from(3u64),
        });
        let secret_number = BigInt::from(4u64);
        let secret_number_inverse = secret_number.modular_multiplicative_inverse(&domain_parameters.point_prime_order).unwrap();

        let (r, s) = ecdsa_sign(&private_key, &domain_parameters,
        &BigInt::from(6u64), &secret_number, &secret_number_inverse);
        assert_eq!(r, BigInt::from(3u64));
        assert_eq!(s, BigInt::from(2u64));
    }

    #[test]
    fn test_ecdsa_sign(){
        let domain_parameters = EccDomainParameters::new_p_192();

        let private_key = BigInt::from_str("1a8d598fc15bf0fd89030b5cb1111aeb92ae8baf5ea475fb").unwrap();
        let public_key = domain_parameters.base_point.scalar_multiplication_double_and_add_decreasing(
        &private_key, &domain_parameters.prime_moduli, &domain_parameters.a, &domain_parameters.point_prime_order);

        assert_eq!(public_key, EccPoint::XY {
            x:BigInt::from_str("62b12d60690cdcf330babab6e69763b471f994dd702d16a5").unwrap(),
            y:BigInt::from_str("63bf5ec08069705ffff65e5ca5c0d69716dfcb3474373902").unwrap(),
        });
        let message_digest = ecc::data_conversion::octets_to_integer(&sha::sha1::encode("abc".as_bytes()) as &[u8]);
        let secret_number = BigInt::from_str("fa6de29746bbeb7f8bb1e761f85f7dfb2983169d82fa2f4e").unwrap();
        let secret_number_inverse = secret_number.modular_multiplicative_inverse(&domain_parameters.point_prime_order).unwrap();

        let (r, s) = ecdsa_sign_p(&private_key, &domain_parameters,
        &message_digest, &secret_number, &secret_number_inverse);
        assert_eq!(r, BigInt::from_str("885052380ff147b734c330c43d39b2c4a89f29b0f749fead").unwrap());
        assert_eq!(s, BigInt::from_str("e9ecc78106def82bf1070cf1d4d804c3cb390046951df686").unwrap());
    }

    #[test]
    fn test_ecdsa_verify() {
        let domain_parameters = EccDomainParameters::new_p_192();

        let public_key = EccPoint::XY {
            x: BigInt::from_str("62b12d60690cdcf330babab6e69763b471f994dd702d16a5").unwrap(),
            y: BigInt::from_str("63bf5ec08069705ffff65e5ca5c0d69716dfcb3474373902").unwrap(),
        };

        let message_digest = ecc::data_conversion::octets_to_integer(&sha::sha1::encode("abc".as_bytes()) as &[u8]);
        let r = BigInt::from_str("885052380ff147b734c330c43d39b2c4a89f29b0f749fead").unwrap();
        let s = BigInt::from_str("e9ecc78106def82bf1070cf1d4d804c3cb390046951df686").unwrap();

        assert!(ecdsa_verify_p(&r, &s, &public_key, &domain_parameters,
        &message_digest));
    }
}