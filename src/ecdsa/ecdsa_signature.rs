use crate::asn1::encode::EncodeTo;
use crate::asn1::decode::DecodeFrom;
use crate::asn1::encode;
use crate::x509_error::X509Error;
use crate::asn1::decode;

#[derive(Debug)]
pub struct EcdsaSignature {
    pub r: Box<[u8]>,
    pub s: Box<[u8]>,
}

impl EncodeTo for EcdsaSignature {
    fn encode(&self) -> Box<[u8]> {
        let mut result = Vec::new();
        result.extend_from_slice(&encode::encode_big_unsigned_integer(&self.r));
        result.extend_from_slice(&encode::encode_big_unsigned_integer(&self.s));
        encode::encode_sequence(&result)
    }
}

impl DecodeFrom for EcdsaSignature {
    fn decode(bytes: &[u8]) -> Result<(EcdsaSignature, usize), X509Error> {
        let (bytes, total_encoded_size) = decode::uncover_sequence(bytes)?;
        let (r, _, bytes) = decode::decode_big_unsigned_integer(bytes)?;
        let (s, _, _) = decode::decode_big_unsigned_integer(bytes)?;

        Ok((EcdsaSignature {
            r,
            s,
        }, total_encoded_size))
    }
}