use crate::asn1::encode::EncodeTo;
use crate::asn1::decode::DecodeFrom;
use crate::x509_error::{X509Error, X509ErrorKind};
use crate::asn1::encode;
use crate::asn1::decode;
use crate::oid::ObjectIdentifier;
use crate::oid;

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum NamedCurve {
    Secp192r1,
    Secp256r1,
    // Secp384r1,
    Secp521r1,
}

impl NamedCurve {
    pub fn to_oid(&self) -> ObjectIdentifier{
        let result:&[u64] = match self {
            NamedCurve::Secp192r1 => oid::ELLIPTIC_CURVE_DOMAIN_SECP192R1,
            NamedCurve::Secp256r1 => oid::ELLIPTIC_CURVE_DOMAIN_SECP256R1,
            // NamedCurve::Secp384r1 => oid::ELLIPTIC_CURVE_DOMAIN_SECP384R1,
            NamedCurve::Secp521r1 => oid::ELLIPTIC_CURVE_DOMAIN_SECP521R1,
        };
        Box::from(result)
    }

    pub fn from_oid(oid:&[u64]) -> Result<NamedCurve, X509Error> {
        let result = match oid {
            oid::ELLIPTIC_CURVE_DOMAIN_SECP192R1 => NamedCurve::Secp192r1,
            oid::ELLIPTIC_CURVE_DOMAIN_SECP256R1 => NamedCurve::Secp256r1,
            // oid::ELLIPTIC_CURVE_DOMAIN_SECP384R1 => NamedCurve::Secp384r1,
            oid::ELLIPTIC_CURVE_DOMAIN_SECP521R1 => NamedCurve::Secp521r1,
            _ => return Err(X509ErrorKind::Decode.into()),
        };
        Ok(result)
    }
}

impl EncodeTo for NamedCurve {
    fn encode(&self) -> Box<[u8]> {
        encode::encode_object_identifier(&self.to_oid())
    }
}

impl DecodeFrom for NamedCurve {
    fn decode(bytes: &[u8]) -> Result<(NamedCurve, usize), X509Error> {
        let (oid, total_encoded_size, _) = decode::decode_object_identifier(bytes)?;
        Ok((NamedCurve::from_oid(&oid)?, total_encoded_size))
    }
}