use crate::oid::ObjectIdentifier;


pub fn display_object_identifier(oid:&ObjectIdentifier) -> String {
    let result = oid.iter().map(|sub_identifier| sub_identifier.to_string()).collect::<Vec<String>>().join(".");
    format!("{}", result)
}

pub fn display_octets (bytes:&[u8], len:usize) -> String {
    let (len, crumbs) = if bytes.len() <= len {
        (bytes.len(), "")
    }
    else {
        (len, "...")
    };
    format!("{} byte(s): {:02x?} {}", bytes.len(), &bytes[ .. len], crumbs)
}

pub fn boolean_yes_no(value: bool) -> String {
    match value {
        true => "YES".to_string(),
        false => "NO".to_string(),
    }
}