use crate::asn1::encode::EncodeTo;
use crate::asn1::decode::DecodeFrom;
use crate::asn1::encode;
use crate::x509_error::{X509Error, X509ErrorKind};
use crate::asn1::decode;

const UNSPECIFIED:u8 = 0;
const KEY_COMPROMISED:u8 = 1;
const CA_COMPROMISED:u8 = 2;
const AFFILIATION_CHANGED:u8 = 3;
const SUPERSEDED:u8 = 4;
const CESSATION_OF_OPERATION:u8 = 5;
const CERTIFICATE_HOLD:u8 = 6;
const REMOVE_FROM_CRL:u8 = 8;
const PRIVILEGE_WITHDRAWN:u8 = 9;
const A_A_COMPROMISED:u8 = 10;


#[derive(Debug)]
pub enum CRLReason {
    Unspecified,
    KeyCompromise,
    CACompromise,
    AffiliationChanged,
    Superseded,
    CessationOfOperation,
    CertificateHold,
    RemoveFromCRL,
    PrivilegeWithdrawn,
    AACompromise,
}

impl CRLReason {
    pub fn to_u8(&self) -> u8 {
        match self {
            CRLReason::Unspecified => UNSPECIFIED,
            CRLReason::KeyCompromise => KEY_COMPROMISED,
            CRLReason::CACompromise => CA_COMPROMISED,
            CRLReason::AffiliationChanged => AFFILIATION_CHANGED,
            CRLReason::Superseded => SUPERSEDED,
            CRLReason::CessationOfOperation => CESSATION_OF_OPERATION,
            CRLReason::CertificateHold => CERTIFICATE_HOLD,
            CRLReason::RemoveFromCRL => REMOVE_FROM_CRL,
            CRLReason::PrivilegeWithdrawn => PRIVILEGE_WITHDRAWN,
            CRLReason::AACompromise => A_A_COMPROMISED,

        }
    }

    pub fn from_i64(value: i64) -> Result<CRLReason, X509Error> {
        let value  = match value as u8 {
            UNSPECIFIED => CRLReason::Unspecified,
            KEY_COMPROMISED => CRLReason::KeyCompromise,
            CA_COMPROMISED => CRLReason::CACompromise,
            AFFILIATION_CHANGED => CRLReason::AffiliationChanged,
            SUPERSEDED => CRLReason::Superseded,
            CESSATION_OF_OPERATION => CRLReason::CessationOfOperation,
            CERTIFICATE_HOLD => CRLReason::CertificateHold,
            REMOVE_FROM_CRL => CRLReason::RemoveFromCRL,
            PRIVILEGE_WITHDRAWN => CRLReason::PrivilegeWithdrawn,
            A_A_COMPROMISED => CRLReason::AACompromise,
            _ => return Err(X509ErrorKind::Decode.into()),
        };
        Ok(value)
    }
}

impl EncodeTo for CRLReason {
    fn encode(&self) -> Box<[u8]> {
        encode::encode_integer(self.to_u8() as i64)
    }
}

impl DecodeFrom for CRLReason {
    fn decode(bytes: &[u8]) -> Result<(CRLReason, usize), X509Error> {
        let (value, size, _) = decode::decode_integer(bytes)?;
        Ok((CRLReason::from_i64(value)?, size))
    }
}