use crate::version::Version;
use crate::ocsp::responder_id::ResponderID;
use crate::date_time::DateTime;
use crate::ocsp::single_response::SingleResponse;
use crate::extensions::extension::Extension;
use crate::asn1::decode::DecodeFrom;
use crate::asn1::decode;
use crate::asn1::encode::EncodeTo;
use crate::asn1::encode;
use crate::x509_error::X509Error;
use crate::asn1::tag_class::TagClass;

#[derive(Debug)]
pub struct ResponseData {
    pub version: Version,
    pub responder_id: ResponderID,
    pub produced_at: DateTime,
    pub responses: Box<[SingleResponse]>,
    pub response_extensions: Box<[Extension]>,
}

impl EncodeTo for ResponseData {
    fn encode(&self) -> Box<[u8]> {
        let mut result = Vec::new();
        result.extend_from_slice(&encode::encode_explicit(0, &self.version.encode()));
        self.responder_id.encode_to(&mut result);
        result.extend_from_slice(&encode::encode_generalized_time(self.produced_at));

        let mut responses: Vec<u8> = Vec::new();
        for response in self.responses.iter() {
            response.encode_to(&mut responses);
        }
        result.extend_from_slice(&encode::encode_sequence(&responses));

        let mut extensions: Vec<u8> = Vec::new();
        for extension in self.response_extensions.iter() {
            extension.encode_to(&mut extensions);
        }

        if extensions.len() > 0 {
            let extensions = encode::encode_sequence(&extensions);
            result.extend_from_slice(&encode::encode_explicit(1, &extensions));
        }

        encode::encode_sequence(&result)
    }
}

impl DecodeFrom for ResponseData {
    fn decode(bytes: &[u8]) -> Result<(ResponseData, usize), X509Error> {
        let (bytes, total_size) = decode::uncover_sequence(bytes)?;
        let (tag_class, _, tag_number, _) = decode::decode_tag_identifier(bytes)?;
        let (version, bytes) = if tag_class == TagClass::ContextSpecific && tag_number == 0 {
            let (bytes, _) = decode::uncover_explicit(bytes, 0)?;
            Version::decode_cut(bytes)?
        } else {
            (Version::V1, bytes)
        };

        let (responder_id, bytes) = ResponderID::decode_cut(bytes)?;
        let (produced_at, _, bytes) = decode::decode_time(bytes)?;
        let (mut responses_bytes, responses_size) = decode::uncover_sequence(bytes)?;
        let mut responses: Vec<SingleResponse> = Vec::new();
        while responses_bytes.len() > 0 {
            let (single_response, encoded_size) = SingleResponse::decode(bytes)?;
            responses.push(single_response);
            responses_bytes = &responses_bytes[encoded_size..];
        }
        let bytes = &bytes[responses_size ..];

        let response_extensions = if bytes.len() > 0 {
            let (_, _, tag_number, _) = decode::decode_tag_identifier(bytes)?;
            if tag_number == 1 {
                let (bytes, _) = decode::uncover_explicit(bytes, 1)?;
                let (mut bytes, _) = decode::uncover_sequence(bytes)?;
                let mut response_extensions: Vec<Extension> = Vec::new();
                while bytes.len() > 0 {
                    let (extension, encoded_size) = Extension::decode(bytes)?;
                    response_extensions.push(extension);
                    bytes = &bytes[encoded_size..];
                }
                response_extensions.into_boxed_slice()
            } else {
                Box::new([]) as Box<[Extension]>
            }
        } else {
            Box::new([]) as Box<[Extension]>
        };

        Ok((ResponseData {
            version,
            responder_id,
            produced_at,
            responses: responses.into_boxed_slice(),
            response_extensions,
        }, total_size))
    }
}