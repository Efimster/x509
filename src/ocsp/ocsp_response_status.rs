use crate::x509_error::{X509Error, X509ErrorKind};
use crate::asn1::encode::EncodeTo;
use crate::asn1::decode::DecodeFrom;
use crate::asn1::encode;
use crate::asn1::decode;

const SUCCESSFUL: u8 = 0;
const MALFORMED_REQUEST: u8 = 1;
const INTERNAL_ERROR: u8 = 2;
const TRY_LATER: u8 = 3;
const SIG_REQUIRED: u8 = 5;
const UNAUTHORIZED: u8 = 6;

#[derive(Debug)]
pub enum OCSPResponseStatus {
    Successful,
    MalformedRequest,
    InternalError,
    TryLater,
    SigRequired,
    Unauthorized,
}

impl OCSPResponseStatus {
    pub fn to_u8(&self) -> u8 {
        match self {
            OCSPResponseStatus::Successful => SUCCESSFUL,
            OCSPResponseStatus::MalformedRequest => MALFORMED_REQUEST,
            OCSPResponseStatus::InternalError => INTERNAL_ERROR,
            OCSPResponseStatus::TryLater => TRY_LATER,
            OCSPResponseStatus::SigRequired => SIG_REQUIRED,
            OCSPResponseStatus::Unauthorized => UNAUTHORIZED,
        }
    }

    pub fn from_i64(value: i64) -> Result<OCSPResponseStatus, X509Error> {
        let value = match value as u8 {
            SUCCESSFUL => OCSPResponseStatus::Successful,
            MALFORMED_REQUEST => OCSPResponseStatus::MalformedRequest,
            INTERNAL_ERROR => OCSPResponseStatus::InternalError,
            TRY_LATER => OCSPResponseStatus::TryLater,
            SIG_REQUIRED => OCSPResponseStatus::SigRequired,
            UNAUTHORIZED => OCSPResponseStatus::Unauthorized,
            _ => return Err(X509ErrorKind::Decode.into()),
        };
        Ok(value)
    }
}

impl EncodeTo for OCSPResponseStatus {
    fn encode(&self) -> Box<[u8]> {
        encode::encode_integer(self.to_u8() as i64)
    }
}

impl DecodeFrom for OCSPResponseStatus {
    fn decode(bytes: &[u8]) -> Result<(OCSPResponseStatus, usize), X509Error> {
        let (value, size, _) = decode::decode_integer(bytes)?;
        Ok((OCSPResponseStatus::from_i64(value)?, size))
    }
}