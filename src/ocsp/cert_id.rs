use crate::algorithm::AlgorithmIdentifier;
use crate::asn1::decode::DecodeFrom;
use crate::asn1::encode::EncodeTo;
use crate::x509_error::X509Error;
use crate::asn1::encode;
use crate::asn1::decode;

#[derive(Debug)]
pub struct CertID {
    pub hash_algorithm: AlgorithmIdentifier,
    pub issuer_name_hash: Box<[u8]>,
    pub issuer_key_hash: Box<[u8]>,
    pub serial_number: Box<[u8]>,
}

impl EncodeTo for CertID {
    fn encode(&self) -> Box<[u8]> {
        let mut result = self.hash_algorithm.encode().into_vec();
        result.extend_from_slice(&encode::encode_octet_string(&self.issuer_name_hash));
        result.extend_from_slice(&encode::encode_octet_string(&self.issuer_key_hash));
        result.extend_from_slice(&encode::encode_big_unsigned_integer(&self.serial_number));
        encode::encode_sequence(&result)
    }
}

impl DecodeFrom for CertID {
    fn decode(bytes: &[u8]) -> Result<(CertID, usize), X509Error> {
        let (bytes, total_size) = decode::uncover_sequence(bytes)?;
        let (hash_algorithm, bytes) = AlgorithmIdentifier::decode_cut(bytes)?;
        let (issuer_name_hash, _, _) = decode::decode_octet_string(bytes)?;
        let (issuer_key_hash, _, _) = decode::decode_octet_string(bytes)?;
        let (serial_number, _, _) = decode::decode_big_integer(bytes)?;

        Ok((CertID {
            hash_algorithm,
            issuer_name_hash,
            issuer_key_hash,
            serial_number,
        }, total_size))
    }
}