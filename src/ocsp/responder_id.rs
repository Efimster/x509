use crate::attribute::Name;
use crate::asn1::encode::EncodeTo;
use crate::x509_error::{X509Error, X509ErrorKind};
use crate::asn1::decode;
use crate::asn1::encode;
use crate::asn1::decode::DecodeFrom;
use crate::asn1::tag_complexity::TagComplexity;
use crate::universal_type::UniversalType;

#[derive(Debug)]
pub enum ResponderID {
    ByName(Name),
    ByKey(Box<[u8]>),
}

impl EncodeTo for ResponderID {
    fn encode(&self) -> Box<[u8]> {
        let (mut result, tag_number) = match self {
            ResponderID::ByName(name) => (name.encode(), 1),
            ResponderID::ByKey(key) => (encode::encode_octet_string(&key), 2),
        };
        encode::encode_implicit(tag_number, &mut result);
        result
    }
}

impl DecodeFrom for ResponderID {
    fn decode(bytes: &[u8]) -> Result<(ResponderID, usize), X509Error> {
        let (_, tag_complexity, tag_number, length, encoded_size) = decode::decode_tag_and_length(bytes)?;
        let total_size = length + encoded_size;
        let mut data = (&bytes[..total_size]).to_vec();

        let result = match tag_number {
            1 => {
                data[0] = encode::encode_tag_universal_identifier(
                    TagComplexity::Constructed, UniversalType::Sequence);
                let (name, _) = Name::decode(&data)?;
                ResponderID::ByName(name)
            },
            2 => {
                data[0] = encode::encode_tag_universal_identifier(
                    tag_complexity, UniversalType::OctetString);
                let (key, _, _) = decode::decode_octet_string(&data)?;
                ResponderID::ByKey(key)
            },
            _ => return Err(X509ErrorKind::Decode.into()),
        };

        Ok((result, total_size))
    }
}