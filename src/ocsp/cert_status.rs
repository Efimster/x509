use crate::date_time::DateTime;
use crate::crl::crl_reason::CRLReason;
use crate::asn1::encode::EncodeTo;
use crate::asn1::encode;
use crate::asn1::decode::DecodeFrom;
use crate::x509_error::X509Error;
use crate::asn1::decode;
use crate::asn1::tag_complexity::TagComplexity;
use crate::universal_type::UniversalType;

#[derive(Debug)]
pub enum CertStatus {
    Good,
    Revoked(RevokedInfo),
    Unknown,
}

impl EncodeTo for CertStatus {
    fn encode(&self) -> Box<[u8]> {
        let (mut result, tag_number) = match self {
            CertStatus::Good => (encode::encode_null(), 0),
            CertStatus::Revoked(info) => (info.encode(), 1),
            CertStatus::Unknown => (encode::encode_null(), 2),
        };
        encode::encode_implicit(tag_number, &mut result);
        result
    }
}

impl DecodeFrom for CertStatus {
    fn decode(bytes: &[u8]) -> Result<(CertStatus, usize), X509Error> {
        let (_, _, tag_number, length, encoded_size) = decode::decode_tag_and_length(bytes)?;
        let total_size = length + encoded_size;

        let result = match tag_number {
            0 => CertStatus::Good,
            1 => {
                let mut data = (&bytes[..total_size]).to_vec();
                data[0] = encode::encode_tag_universal_identifier(
                TagComplexity::Constructed, UniversalType::Sequence);
                let (revoked_info, _) = RevokedInfo::decode(&data)?;
                CertStatus::Revoked(revoked_info)
            },
            _ => CertStatus::Unknown,
        };

        Ok((result, total_size))
    }
}

#[derive(Debug)]
pub struct RevokedInfo {
    pub revocation_time:DateTime,
    pub revocation_reason: Option<CRLReason>,
}

impl EncodeTo for RevokedInfo {
    fn encode(&self) -> Box<[u8]> {
        let mut result = Vec::new();
        result.extend_from_slice(&encode::encode_generalized_time(self.revocation_time));
        if let Some(ref revocation_reason) = self.revocation_reason {
            result.extend_from_slice(&encode::encode_explicit(0, &revocation_reason.encode()));
        }

        encode::encode_sequence(&result)
    }
}

impl DecodeFrom for RevokedInfo {
    fn decode(bytes: &[u8]) -> Result<(RevokedInfo, usize), X509Error> {
       let (bytes, total_size) = decode::uncover_sequence(bytes)?;
        let (revocation_time, _, bytes) = decode::decode_time(bytes)?;
        let revocation_reason = if bytes.len() > 0 {
            let (bytes, _) = decode::uncover_explicit(bytes, 0)?;
            let (revocation_reason, _) = CRLReason::decode(bytes)?;
            Some(revocation_reason)
        }
        else {
            None
        };

        Ok((RevokedInfo{
            revocation_time,
            revocation_reason,
        }, total_size))
    }
}