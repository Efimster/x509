use crate::ocsp::response_data::ResponseData;
use crate::algorithm::AlgorithmIdentifier;
use crate::certificate::Certificate;
use crate::asn1::encode::EncodeTo;
use crate::asn1::encode;
use crate::asn1::decode::DecodeFrom;
use crate::x509_error::X509Error;
use crate::asn1::decode;

#[derive(Debug)]
pub struct BasicOCSPResponse {
    tbs_response_data: ResponseData,
    signature_algorithm: AlgorithmIdentifier,
    signature: Box<[u8]>,
    certs: Box<[Certificate]>,
}

impl EncodeTo for BasicOCSPResponse {
    fn encode(&self) -> Box<[u8]> {
        let mut result = Vec::new();
        self.tbs_response_data.encode_to(&mut result);
        self.signature_algorithm.encode_to(&mut result);
        result.extend_from_slice(&encode::encode_bit_string(&self.signature));

        let mut certificates: Vec<u8> = Vec::new();
        for certificate in self.certs.iter() {
            certificate.encode_to(&mut certificates);
        }

        if certificates.len() > 0 {
            let certificates = encode::encode_sequence(&certificates);
            result.extend_from_slice(&encode::encode_explicit(0, &certificates));
        }

        encode::encode_sequence(&result)
    }
}

impl DecodeFrom for BasicOCSPResponse {
    fn decode(bytes: &[u8]) -> Result<(BasicOCSPResponse, usize), X509Error> {
        let (bytes, total_size) = decode::uncover_sequence(bytes)?;
        let (responder_data, bytes) = ResponseData::decode_cut(bytes)?;
        let (signature_algorithm, bytes) = AlgorithmIdentifier::decode_cut(bytes)?;
        let (signature, _, bytes) = decode::decode_bit_string(bytes)?;

        let certificates = if bytes.len() > 0 {
            let (_, _, tag_number, _) = decode::decode_tag_identifier(bytes)?;
            if tag_number == 0 {
                let (bytes, _) = decode::uncover_explicit(bytes, 0)?;
                let (mut bytes, _) = decode::uncover_sequence(bytes)?;
                let mut certificates: Vec<Certificate> = Vec::new();
                while bytes.len() > 0 {
                    let (certificate, encoded_size) = Certificate::decode(bytes)?;
                    certificates.push(certificate);
                    bytes = &bytes[encoded_size..];
                }
                certificates.into_boxed_slice()
            } else {
                Box::new([]) as Box<[Certificate]>
            }
        } else {
            Box::new([]) as Box<[Certificate]>
        };

        Ok((BasicOCSPResponse {
            tbs_response_data: responder_data,
            signature_algorithm,
            signature,
            certs: certificates,
        }, total_size))
    }
}