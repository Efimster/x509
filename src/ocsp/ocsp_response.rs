use crate::ocsp::ocsp_response_status::OCSPResponseStatus;
use crate::ocsp::basic_ocsp_response::BasicOCSPResponse;
use crate::asn1::encode::EncodeTo;
use crate::asn1::decode::DecodeFrom;
use crate::asn1::encode;
use crate::asn1::decode;
use crate::x509_error::{X509Error, X509ErrorKind};

#[derive(Debug)]
pub struct OCSPResponse {
    pub response_status: OCSPResponseStatus,
    pub response: Option<StandardOCSPResponse>,
}

#[derive(Debug)]
pub enum StandardOCSPResponse {
    Basic(BasicOCSPResponse),
}

impl EncodeTo for OCSPResponse {
    fn encode(&self) -> Box<[u8]> {
        let mut result = Vec::new();
        self.response_status.encode_to(&mut result);
        if let Some(StandardOCSPResponse::Basic(ref response)) = self.response {
            let mut response_bytes = encode::encode_object_identifier( &[1, 3, 6, 1, 5, 5, 7, 48, 1, 1]).to_vec();
            response_bytes.extend_from_slice(&encode::encode_octet_string(&response.encode()));
            let response_bytes = encode::encode_sequence(&response_bytes);
            result.extend_from_slice(&encode::encode_explicit(0, &response_bytes));
        }
        encode::encode_sequence(&result)
    }
}

impl DecodeFrom for OCSPResponse {
    fn decode(bytes: &[u8]) -> Result<(OCSPResponse, usize), X509Error> {
        let (bytes, total_size) = decode::uncover_sequence(bytes)?;
        let (response_status, bytes) = OCSPResponseStatus::decode_cut(bytes)?;

        let response = if bytes.len() > 0 {
            let (_, _, tag_number, _) = decode::decode_tag_identifier(bytes)?;
            if tag_number == 0 {
                let (bytes, _) = decode::uncover_explicit(bytes, 0)?;
                let (bytes, _) = decode::uncover_sequence(bytes)?;
                let (oid, _, bytes) = decode::decode_object_identifier(bytes)?;
                match &oid as &[u64] {
                    &[1, 3, 6, 1, 5, 5, 7, 48, 1, 1] => {
                        let (basic_response, _) = BasicOCSPResponse::decode(bytes)?;
                        Some(StandardOCSPResponse::Basic(basic_response))
                    },
                    _ => return Err(X509ErrorKind::Decode.into())
                }

            } else {
                None
            }
        } else {
            None
        };

        Ok((OCSPResponse {
            response_status,
            response,
        }, total_size))
    }
}