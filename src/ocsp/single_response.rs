use crate::ocsp::cert_id::CertID;
use crate::ocsp::cert_status::CertStatus;
use crate::date_time::DateTime;
use crate::extensions::extension::Extension;
use crate::asn1::decode::DecodeFrom;
use crate::asn1::encode::EncodeTo;
use crate::x509_error::X509Error;
use crate::asn1::decode;
use crate::asn1::encode;

#[derive(Debug)]
pub struct SingleResponse {
    pub cert_id: CertID,
    pub cert_status: CertStatus,
    pub this_update: DateTime,
    pub next_update: Option<DateTime>,
    pub single_extensions: Box <[Extension]>,
}

impl EncodeTo for SingleResponse {
    fn encode(&self) -> Box<[u8]> {
        let mut result = Vec::new();
        self.cert_id.encode_to(&mut result);
        self.cert_status.encode_to(&mut result);
        result.extend_from_slice(&encode::encode_generalized_time(self.this_update));
        if let Some(next_update) = self.next_update {
            result.extend_from_slice(&encode::encode_explicit(0,
                &encode::encode_generalized_time(next_update)));
        }

        let mut extensions: Vec<u8> = Vec::new();
        for extension in self.single_extensions.iter() {
            extension.encode_to(&mut extensions);
        }

        if extensions.len() > 0 {
            let extensions = encode::encode_sequence(&extensions);
            result.extend_from_slice(&encode::encode_explicit(1, &extensions));
        }

        encode::encode_sequence(&result)
    }
}

impl DecodeFrom for SingleResponse {
    fn decode(bytes: &[u8]) -> Result<(SingleResponse, usize), X509Error> {
        let (bytes, total_size) = decode::uncover_sequence(bytes)?;
        let (cert_id, bytes) = CertID::decode_cut(bytes)?;
        let (cert_status, bytes) = CertStatus::decode_cut(bytes)?;
        let (this_update, _, bytes) = decode::decode_time(bytes)?;

        let (next_update, bytes) = if bytes.len() > 0 {
            let (_, _, tag_number, _) = decode::decode_tag_identifier(bytes)?;
            if tag_number == 0 {
                let (bytes, _) = decode::uncover_explicit(bytes, 0)?;
                let (next_update, _, bytes) = decode::decode_time(bytes)?;
                (Some(next_update), bytes)
            }
            else {
                (None, bytes)
            }
        }
        else {
            (None, bytes)
        };

        let single_extensions = if bytes.len() > 0 {
            let (_, _, tag_number, _) = decode::decode_tag_identifier(bytes)?;
            if tag_number == 1 {
                let (bytes, _) = decode::uncover_explicit(bytes, 1)?;
                let (mut bytes, _) = decode::uncover_sequence(bytes)?;
                let mut single_extensions: Vec<Extension> = Vec::new();
                while bytes.len() > 0 {
                    let (extension, encoded_size) = Extension::decode(bytes)?;
                    single_extensions.push(extension);
                    bytes = &bytes[encoded_size..];
                }
                single_extensions.into_boxed_slice()
            } else {
                Box::new([]) as Box<[Extension]>
            }
        }
        else {
            Box::new([]) as Box<[Extension]>
        };

        Ok((SingleResponse {
            cert_id,
            cert_status,
            this_update,
            next_update,
            single_extensions,
        }, total_size))
    }
}