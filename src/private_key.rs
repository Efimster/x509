use crate::rsa::rsa_private_key::RSAPrivateKey;
use bigint::BigInt;
use crate::asn1::encode::EncodeTo;
use crate::asn1::encode;
use crate::asn1::decode::DecodeFrom;
use crate::x509_error::X509Error;
use crate::asn1::decode;
use core::fmt;

#[derive(Debug, Clone)]
pub enum PrivateKey {
    None,
    RSA(RSAPrivateKey),
    ECDSA(BigInt),
}

impl EncodeTo for PrivateKey {
    fn encode(&self) -> Box<[u8]> {
        match self {
            PrivateKey::RSA(key) => key.encode(),
            PrivateKey::ECDSA(key) => {
                let octets = ecc::data_conversion::integer_to_octets(key);
                encode::encode_octet_string(&octets)
            },
            PrivateKey::None => Box::new([])
        }
    }
}

impl PrivateKey {
    pub fn decode_ecdsa(bytes: &[u8]) -> Result<(PrivateKey, usize), X509Error> {
        let (octets, total_encoded_size, _) = decode::decode_octet_string(bytes)?;
        Ok((PrivateKey::ECDSA(ecc::data_conversion::octets_to_integer(&octets)), total_encoded_size))
    }

    pub fn decode_rsa(bytes: &[u8]) -> Result<(PrivateKey, usize), X509Error> {
        let (key, size)= RSAPrivateKey::decode(bytes)?;
        Ok((PrivateKey::RSA(key), size))
    }
}

impl fmt::Display for PrivateKey {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            PrivateKey::RSA(key) => write!(f, "{}", key),
            PrivateKey::ECDSA(key) => write!(f, "{:?}", key),
            PrivateKey::None => write!(f, "NONE"),
        }
    }
}