use std::mem;
use bigint::BigInt;

pub fn u64_from_bytes_be(bytes: &[u8]) -> u64 {
    let mut result: [u8; 8] = [0; 8];

    if bytes.len() < 8 {
        let start = 8 - bytes.len();
        result[start..].copy_from_slice(bytes);
    } else {
        result.copy_from_slice(bytes);
    }

    let result: u64 = unsafe { mem::transmute(result) };
    result.to_be()
}

pub fn create_buffer(length: usize) -> Box<[u8]> {
    let mut buff: Vec<u8> = Vec::with_capacity(length);
    unsafe { buff.set_len(length); }
    let result: Box<[u8]> = buff.into_boxed_slice();
    result
}

#[inline]
pub fn u64_to_bytes_be(value: u64) -> Box<[u8]> {
    let mut result = unsafe { &mem::transmute::<u64, [u8; 8]>(value.to_be()) as &[u8]};

    while result.len() > 1 && result[0] == 0 {
        result = &result[1..];
    }
    Box::from(result)
}

pub fn concat_u8_slices(lhs: &[u8], rhs: &[u8]) -> Box<[u8]> {
    let mut result: Vec<u8> = Vec::with_capacity(lhs.len() + rhs.len());
    result.extend_from_slice(lhs);
    result.extend_from_slice(rhs);
    result.into_boxed_slice()
}

pub fn bytes_be_to_bigint(bytes:&[u8]) -> BigInt {
    let mut value:Box<[u8]> = Box::from(bytes);
    value.reverse();
    BigInt::from(&value as &[u8])
}