use crate::private_key::PrivateKey;
use std::fs;
use std::path::Path;
use std::ffi::OsStr;
use crate::x509_error::X509Error;

pub fn read_certificate_file(filepath: &str) -> Result<Box<[u8]>, X509Error> {
    let file_extension = Path::new(filepath)
        .extension()
        .and_then(OsStr::to_str);
    match file_extension {
        Some(extension) if extension == "pem" => read_pem_file(filepath, "CERTIFICATE"),
        _ => Ok(fs::read(filepath)?.into_boxed_slice())
    }
}

pub fn write_certificate_file(filepath: &str, bytes: &[u8]) -> Result<(), X509Error> {
    let file_extension = Path::new(filepath)
        .extension()
        .and_then(OsStr::to_str);
    match file_extension {
        Some(extension) if extension == "pem" => write_pem_file(filepath, "CERTIFICATE", bytes),
        _ => Ok(fs::write(filepath, bytes)?)
    }
}

pub fn read_private_key(filepath: &str, signature_schema: &str) -> PrivateKey {
    match signature_schema {
        "rsa" => {
            let content = read_pem_file(filepath, "RSA PRIVATE KEY")
                .expect("< could not read rsa private key");
            PrivateKey::decode_rsa(&content).expect("< could not read rsa private key").0
        }
        "ecdsa" => {
            let content = read_pem_file(filepath, "EC PRIVATE KEY")
                .expect("< could not read ecdsa private key");
            PrivateKey::decode_ecdsa(&content).expect("< could not read ecdsa private key").0
        }
        _ => {
            panic!("< Fail to read private key. {} signature schema is unknown", signature_schema);
        }
    }
}

pub fn read_pem_file(filepath: &str, marker: &str) -> Result<Box<[u8]>, X509Error> {
    let base64_text = fs::read_to_string(filepath)?
        .lines()
        .skip_while(|line| !line.contains(&format!("BEGIN {}", marker)))
        .skip(1)
        .take_while(|line| !line.contains(&format!("END {}", marker))).collect::<String>();
    Ok(base64::decode_bytes(base64_text.as_bytes()))
}

pub fn write_pem_file(filepath: &str, marker: &str, bytes: &[u8]) -> Result<(), X509Error> {
    let base64_text = base64::encode(bytes);
    let header = format!("-----BEGIN {}-----", marker);
    let footer = format!("-----END {}-----", marker);
    let base64_text = base64_text.chars().enumerate()
        .flat_map(|(i, c)| {
            if i != 0 && i % 65 == 0 {
                Some('\n')
            } else {
                None
            }.into_iter().chain(std::iter::once(c))
        })
        .collect::<String>();
    let base64_text = format!("{}\n{}\n{}", header, base64_text, footer);
    Ok(fs::write(filepath, base64_text.as_bytes())?)
}