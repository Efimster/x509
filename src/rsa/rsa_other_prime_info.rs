use crate::asn1::encode::EncodeTo;
use crate::asn1::encode;
use crate::asn1::decode::DecodeFrom;
use crate::x509_error::X509Error;
use crate::asn1::decode;
use crate::display;
use std::fmt;

#[derive(Debug, Clone)]
pub struct OtherPrimeInfo {
    pub prime: Box<[u8]>,
    pub exponent: Box<[u8]>,
    pub coefficient: Box<[u8]>,
}

impl EncodeTo for OtherPrimeInfo {
    fn encode(&self) -> Box<[u8]> {
        let mut result:Vec<u8> = Vec::with_capacity(self.prime.len() + self.exponent.len() + self.coefficient.len());
        result.extend_from_slice(&encode::encode_big_unsigned_integer(&self.prime));
        result.extend_from_slice(&encode::encode_big_unsigned_integer(&self.exponent));
        result.extend_from_slice(&encode::encode_big_unsigned_integer(&self.coefficient));
        encode::encode_sequence(&result)
    }
}

impl DecodeFrom for OtherPrimeInfo {
    fn decode(bytes: &[u8]) -> Result<(OtherPrimeInfo, usize), X509Error> {
        let (bytes, total_encoded_size) = decode::uncover_sequence(bytes)?;
        let (prime, _, bytes) = decode::decode_big_unsigned_integer(bytes)?;
        let (exponent, _, bytes) = decode::decode_big_unsigned_integer(bytes)?;
        let (coefficient, _, _) = decode::decode_big_unsigned_integer(bytes)?;

        Ok((OtherPrimeInfo {
            prime,
            exponent,
            coefficient,
        }, total_encoded_size))
    }
}

impl fmt::Display for OtherPrimeInfo {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let tab = "\t".repeat(f.width().unwrap_or_default());
        f.write_str(&format!("\n{}Prime: {}", tab, display::display_octets(&self.prime, 20)))?;
        f.write_str(&format!("\n{}Exponent: {}", tab, display::display_octets(&self.exponent, 20)))?;
        f.write_str(&format!("\n{}Coefficient: {}", tab, display::display_octets(&self.coefficient, 20)))
    }
}