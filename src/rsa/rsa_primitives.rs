use crate::digest_info::DigestInfo;
use crate::algorithm::AlgorithmIdentifier;
use crate::algorithm::AlgorithmParameters;
use crate::asn1::encode::EncodeTo;
use crate::x509_error::{X509Error, X509ErrorKind};
use sha::{sha256, sha384, sha512, sha224};
use bigint::BigInt;
use crate::rsa::rsa_private_key::RSAPrivateKey;
use crate::rsa::rsa_public_key::RSAPublicKey;
use crate::oid;
use crate::utils::transmute;


pub fn rsassa_pkcs_verify(public_key:&RSAPublicKey, message: &[u8], signature: &[u8], sha_algorithm:&[u64]) -> Result<bool, X509Error> {
    let modulus = transmute::bytes_be_to_bigint(&public_key.modulus);
    let exponent= transmute::bytes_be_to_bigint(&public_key.exponent);
    let signature = transmute::bytes_be_to_bigint(signature);

    let encoded_message_1 = rsavp1(&modulus, &exponent, &signature)?;
    let encoded_message_1 = encoded_message_1.as_reverse_bytes(false);
    let encoded_message_2 = emsa_pkcs_encode(message, public_key.modulus.len(), sha_algorithm)?;
    Ok(encoded_message_1 == encoded_message_2)
}

pub fn rsavp1(modulus: &BigInt, exponent: &BigInt, signature: &BigInt) -> Result<BigInt, X509Error> {
    //TODO: modular exponentiation for fixed exponent algorithm could be used for faster results (usually exponent is small number)
    if signature > modulus {
        //it could be reduced first and used with other modular exponentiation algorithms
        Ok(signature.left_to_right_modular_exponentiation(exponent, modulus))
    }
    else {
        Ok(signature.montgomery_exponentiation(exponent, modulus))
    }
}

pub fn emsa_pkcs_encode(message: &[u8], em_len: usize, sha_algorithm: &[u64]) -> Result<Box<[u8]>, X509Error> {

    let digest:Box<[u8]> = match sha_algorithm {
        oid::SHA256 => Box::from(sha256::encode(message)),
        oid::SHA384 => Box::from(sha384::encode(message)),
        oid::SHA512 => Box::from(sha512::encode(message)),
        oid::SHA224 => Box::from(sha224::encode(message)),
        oid::SHA1 => Box::from(sha::sha1::encode(message)),
        _ => return Err(X509ErrorKind::Encode.into()),
    };

    let digest_algorithm = AlgorithmIdentifier {
        algorithm_id: Box::from(sha_algorithm),
        parameters: AlgorithmParameters::None,
    };

    let digest_info = DigestInfo {
        digest_algorithm,
        digest,
    }.encode();

    if em_len < digest_info.len() + 11 {
        return Err(X509ErrorKind::Encode.into());
    }

    let mut result: Vec<u8> = Vec::with_capacity(em_len);
    result.push(0);
    result.push(1);
    for _ in 0..em_len - digest_info.len() - 3 {
        result.push(0xff);
    }
    result.push(0);
    result.extend_from_slice(&digest_info);

    Ok(result.into_boxed_slice())
}

pub fn rsassa_pkcs_sign(private_key: &RSAPrivateKey, message: &[u8], sha_algorithm: &[u64]) -> Result<Box<[u8]>, X509Error>{
    let message = transmute::bytes_be_to_bigint(&emsa_pkcs_encode(message, private_key.modulus.len(), sha_algorithm)?);
    let signature = rsasp1(private_key, &message)?;
    Ok(signature.as_reverse_bytes(false))
}

pub fn rsasp1(private_key:&RSAPrivateKey, message: &BigInt) -> Result<BigInt, X509Error> {
    let modulus = transmute::bytes_be_to_bigint(&private_key.modulus);

    if private_key.version == 0 {
        let private_exponent = transmute::bytes_be_to_bigint( &private_key.private_exponent);
        return Ok(message.montgomery_exponentiation(&private_exponent, &modulus));
    }

    let p = transmute::bytes_be_to_bigint(&private_key.prime1);
    let q = transmute::bytes_be_to_bigint(&private_key.prime2);
    let dp = transmute::bytes_be_to_bigint(&private_key.exponent1);
    let dq = transmute::bytes_be_to_bigint(&private_key.exponent2);
    let q_inv = transmute::bytes_be_to_bigint(&private_key.coefficient);

    let other_prime_infos: Box<[(BigInt, BigInt, BigInt)]> = match private_key.other_prime_infos {
        Some(ref other_prime_infos) => {
            other_prime_infos.iter().map(|prime_info| {
                let prime = transmute::bytes_be_to_bigint(&prime_info.prime);
                let exponent = transmute::bytes_be_to_bigint(&prime_info.exponent);
                let coefficient = transmute::bytes_be_to_bigint(&prime_info.coefficient);
                (prime, exponent, coefficient)
            }).collect::<Vec<_>>().into_boxed_slice()
        },
        None => Box::new([])
    };

    let mut prime_infos:Vec<(BigInt, BigInt, BigInt)> = Vec::with_capacity(other_prime_infos.len() + 2);
    prime_infos.push((p.clone(), dp.clone(), q_inv.clone()));
    prime_infos.push((q.clone(), dq.clone(), BigInt::one()));
    prime_infos.extend_from_slice( &other_prime_infos);

    let mut s_arr:Vec<BigInt> = Vec::with_capacity(other_prime_infos.len() + 2);
    s_arr.push(message.left_to_right_modular_exponentiation(&dp, &p));
    s_arr.push(message.left_to_right_modular_exponentiation(&dq, &q));
    for prime_info in other_prime_infos.iter() {
        s_arr.push(message.left_to_right_modular_exponentiation(&prime_info.1, &prime_info.0));
    }

    let h = if &s_arr[0] < &s_arr[1] {
        &p - &(&s_arr[1] - &s_arr[0]).modular_multiplication(&q_inv, &p)
    } else {
        (&s_arr[0] - &s_arr[1]).modular_multiplication(&q_inv, &p)
    };
    let mut s:BigInt = &s_arr[1] + &(q * h);

    if other_prime_infos.len() == 0 {
        return Ok(s);
    }

    let mut r = prime_infos[0].0.clone();
    for i in 2 .. prime_infos.len() {
        r = &r * &prime_infos[i - 1].0;
        let h = if s_arr[i] < s {
            &prime_infos[i].0 - &(&s - &s_arr[i]).modular_multiplication(&prime_infos[i].2, &prime_infos[i].0)
        }
        else {
            (&s_arr[i] - &s).modular_multiplication(&prime_infos[i].2, &prime_infos[i].0)
        };
        s = &s + &(&r * h);
    }

    Ok(s)
}
