use crate::asn1::encode::EncodeTo;
use crate::asn1::encode;
use crate::asn1::decode::DecodeFrom;
use crate::asn1::decode;
use crate::x509_error::X509Error;
use crate::rsa::rsa_other_prime_info::OtherPrimeInfo;
use bigint::BigInt;
use std::fmt;
use crate::display;

#[derive(Debug, Clone)]
pub struct RSAPrivateKey {
    pub version: i64,
    pub modulus: Box<[u8]>,
    pub public_exponent: Box<[u8]>,
    pub private_exponent: Box<[u8]>,
    pub prime1: Box<[u8]>,
    pub prime2: Box<[u8]>,
    pub exponent1: Box<[u8]>,
    pub exponent2: Box<[u8]>,
    pub coefficient: Box<[u8]>,
    pub other_prime_infos: Option<Box<[OtherPrimeInfo]>>
}

impl RSAPrivateKey {
    pub fn from_primes(primes:&[BigInt]) -> RSAPrivateKey {
        debug_assert!(primes.len() > 1);
        let one = BigInt::one();
        let phi = primes.iter().map(|prime| prime - &one).collect::<Vec<_>>();
        let lambda = BigInt::lcm_n(phi.as_slice());
        let e = BigInt::from(65537u64);
        let d = e.modular_multiplicative_inverse(&lambda).unwrap();
        let dp = d.classic_reduction(&phi[0]);
        let dq = d.classic_reduction(&phi[1]);
        let q_inv = primes[1].modular_multiplicative_inverse(&primes[0]).unwrap();
        let version = if primes.len() < 3 {0i64} else {1};
        let other_prime_infos: Option<Box<[OtherPrimeInfo]>> = match version {
            0 => None,
            _ => {
                let mut infos:Vec<OtherPrimeInfo> = Vec::with_capacity(primes.len() - 2);
                let mut t = primes[0].clone();
                for i in 2 .. primes.len() {
                    t = t * &primes[i - 1];
                    let exponent = d.classic_reduction(&phi[i]);
                    let coefficient = t.modular_multiplicative_inverse(&primes[i]).unwrap();
                    infos.push(OtherPrimeInfo{
                        prime: primes[i].as_reverse_bytes(true),
                        exponent: exponent.as_reverse_bytes(true),
                        coefficient: coefficient.as_reverse_bytes(true),
                    })
                }
                Some(infos.into_boxed_slice())
            }
        };
        let modulus = primes.iter().fold(BigInt::one(), |result, prime| result * prime);

        RSAPrivateKey {
            version,
            modulus: modulus.as_reverse_bytes(true),
            public_exponent: e.as_reverse_bytes(true),
            private_exponent: d.as_reverse_bytes(true),
            prime1: primes[0].as_reverse_bytes(true),
            prime2: primes[1].as_reverse_bytes(true),
            exponent1: dp.as_reverse_bytes(true),
            exponent2: dq.as_reverse_bytes(true),
            coefficient: q_inv.as_reverse_bytes(true),
            other_prime_infos
        }
    }
}

impl EncodeTo for RSAPrivateKey {
    fn encode(&self) -> Box<[u8]> {
        let mut result = Vec::new();
        result.extend_from_slice(&encode::encode_integer(self.version));
        result.extend_from_slice(&encode::encode_big_unsigned_integer(&self.modulus));
        result.extend_from_slice(&encode::encode_big_unsigned_integer(&self.public_exponent));
        result.extend_from_slice(&encode::encode_big_unsigned_integer(&self.private_exponent));
        result.extend_from_slice(&encode::encode_big_unsigned_integer(&self.prime1));
        result.extend_from_slice(&encode::encode_big_unsigned_integer(&self.prime2));
        result.extend_from_slice(&encode::encode_big_unsigned_integer(&self.exponent1));
        result.extend_from_slice(&encode::encode_big_unsigned_integer(&self.exponent2));
        result.extend_from_slice(&encode::encode_big_unsigned_integer(&self.coefficient));
        match self.other_prime_infos {
            Some(ref other_infos) => {
                let mut bytes = Vec::new();
                for info in other_infos.iter() {
                    info.encode_to(&mut bytes);
                }
                result.extend_from_slice(&encode::encode_sequence(&bytes));
            },
            None => (),
        };

        encode::encode_sequence(&result)
    }
}

impl DecodeFrom for RSAPrivateKey {
    fn decode(bytes: &[u8]) -> Result<(RSAPrivateKey, usize), X509Error> {
        let (bytes, total_encoded_size) = decode::uncover_sequence(bytes)?;
        let (version, _, bytes) = decode::decode_integer(bytes)?;
        let (modulus, _, bytes) = decode::decode_big_unsigned_integer(bytes)?;
        let (public_exponent, _, bytes) = decode::decode_big_unsigned_integer(bytes)?;
        let (private_exponent, _, bytes) = decode::decode_big_unsigned_integer(bytes)?;
        let (prime1, _, bytes) = decode::decode_big_unsigned_integer(bytes)?;
        let (prime2, _, bytes) = decode::decode_big_unsigned_integer(bytes)?;
        let (exponent1, _, bytes) = decode::decode_big_unsigned_integer(bytes)?;
        let (exponent2, _, bytes) = decode::decode_big_unsigned_integer(bytes)?;
        let (coefficient, _, bytes) = decode::decode_big_unsigned_integer(bytes)?;

        let other_prime_infos: Option<Box<[OtherPrimeInfo]>> = if version == 0 || bytes.len() == 0 {
            None
        }
        else {
            let (mut bytes, _) = decode::uncover_sequence(bytes)?;
            let mut result:Vec<OtherPrimeInfo> = Vec::new();
            while bytes.len() > 0 {
                let (info, size) = OtherPrimeInfo::decode(bytes)?;
                result.push(info);
                bytes = &bytes[size ..];
            }
            Some(result.into_boxed_slice())
        };

        Ok((RSAPrivateKey {
            version,
            modulus,
            public_exponent,
            private_exponent,
            prime1,
            prime2,
            exponent1,
            exponent2,
            coefficient,
            other_prime_infos,
        }, total_encoded_size))
    }
}

impl fmt::Display for RSAPrivateKey {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let tab = "\t".repeat(f.width().unwrap_or_default());
        f.write_str(&format!("\n{}Version: {}", tab, self.version))?;
        f.write_str(&format!("\n{}Modulus: {}", tab, display::display_octets(&self.modulus, 20)))?;
        f.write_str(&format!("\n{}Public Exponent: {}", tab, display::display_octets(&self.public_exponent, 20)))?;
        f.write_str(&format!("\n{}Private Exponent: {}", tab, display::display_octets(&self.private_exponent, 20)))?;
        f.write_str(&format!("\n{}Prime 1: {}", tab, display::display_octets(&self.prime1, 20)))?;
        f.write_str(&format!("\n{}Prime 2: {}", tab, display::display_octets(&self.prime2, 20)))?;
        f.write_str(&format!("\n{}Exponent 1: {}", tab, display::display_octets(&self.exponent1, 20)))?;
        f.write_str(&format!("\n{}Exponent 2: {}", tab, display::display_octets(&self.exponent2, 20)))?;
        f.write_str(&format!("\n{}Coefficient: {}", tab, display::display_octets(&self.coefficient, 20)))?;
        if let Some(infos) = &self.other_prime_infos {
            f.write_str("\nOther primes:")?;
            for i in 0usize .. infos.len() {
                f.write_str(&format!("\n{:<1}", infos[i]))?;
            }
        }
        Ok(())
    }
}

