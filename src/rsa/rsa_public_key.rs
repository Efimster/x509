use crate::asn1::encode::EncodeTo;
use crate::asn1::encode;
use crate::asn1::decode::DecodeFrom;
use crate::asn1::decode;
use crate::x509_error::X509Error;
use std::fmt;
use crate::display;
use crate::utils::transmute;

#[derive(Debug, Clone)]
pub struct RSAPublicKey {
    pub modulus: Box<[u8]>,
    pub exponent: Box<[u8]>,
}

impl EncodeTo for RSAPublicKey {
    fn encode(&self) -> Box<[u8]> {
        let mut result = encode::encode_big_unsigned_integer(&self.modulus).into_vec();
        result.extend_from_slice(&encode::encode_big_unsigned_integer(&self.exponent));
        encode::encode_sequence(&result)
    }
}

impl DecodeFrom for RSAPublicKey {
    fn decode(bytes: &[u8]) -> Result<(RSAPublicKey, usize), X509Error> {
        let (bytes, total_encoded_size) = decode::uncover_sequence(bytes)?;
        let (modulus, _, bytes) = decode::decode_big_unsigned_integer(bytes)?;
        let (exponent, _, _) = decode::decode_big_unsigned_integer(bytes)?;

        Ok((RSAPublicKey {
            modulus,
            exponent,
        }, total_encoded_size))
    }
}

impl fmt::Display for RSAPublicKey {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let tab = "\t".repeat(f.width().unwrap_or_default());
        f.write_str(&format!("\n{}Modulus: {}", tab, display::display_octets(&self.modulus, 20)))?;
        if self.exponent.len() < 8 {
            let exponent = transmute::u64_from_bytes_be(&self.exponent);
            f.write_str(&format!("\n{}Exponent: {}", tab, exponent))?;
        } else {
            f.write_str(&format!("\n{}Exponent: {}", tab, display::display_octets(&self.exponent, 20)))?;
        }

        f.write_str(&format!("\n{}Key size: {} bits", tab, self.modulus.len() * 8))
    }
}