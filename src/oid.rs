use crate::x509_error::X509Error;
use std::str::FromStr;
use json::JSON;
use json::error::{JSONError, JSONErrorKind};

pub const X_500_ATTRIBUTES: &[u64] = &[2, 5, 4];
pub const COMMON_NAME: &[u64] = &[2, 5, 4, 3];
pub const COMMON_NAME_HUMAN: &str = "Common name";
pub const SERIAL_NUMBER: &[u64] = &[2, 5, 4, 5];
pub const COUNTRY_OR_REGION:&[u64] = &[2, 5, 4, 6];
pub const COUNTRY_OR_REGION_HUMAN: &str = "Country or region";
pub const LOCALITY_NAME: &[u64] = &[2, 5, 4, 7];
pub const STATE_OR_PROVINCE: &[u64] = &[2, 5, 4, 8];
pub const STREET_ADDRESS: &[u64] = &[2, 5, 4, 9];
pub const ORGANIZATION: &[u64] = &[2, 5, 4, 10];
pub const ORGANIZATION_HUMAN: &str = "Organization";
pub const ORGANIZATION_UNIT: &[u64] = &[2, 5, 4, 11];
pub const ORGANIZATION_UNIT_HUMAN: &str = "Organization unit";
pub const OTHER_NAME: &[u64] = &[2, 5, 4, 97];

pub const CERTIFICATE_EXTENSION: &[u64] = &[2, 5, 29];
pub const SUBJECT_KEY_IDENTIFIER: &[u64] = &[2, 5, 29, 14];
pub const KEY_USAGE: &[u64] = &[2, 5, 29, 15];
pub const KEY_USAGE_HUMAN: &str = "Key usage";
pub const SUBJECT_ALTERNATIVE_NAME: &[u64] = &[2, 5, 29, 17];
pub const SUBJECT_ALTERNATIVE_NAME_HUMAN: &str = "Subject alternative name";
pub const ISSUER_ALTERNATIVE_NAME: &[u64] = &[2, 5, 29, 18];
pub const BASIC_CONSTRAINTS: &[u64] = &[2, 5, 29, 19];
pub const BASIC_CONSTRAINTS_HUMAN: &str = "Basic constraints";
pub const CRL_DISTRIBUTION_POINT: &[u64] = &[2, 5, 29, 31];
pub const CERTIFICATE_POLICIES: &[u64] = &[2, 5, 29, 32];
pub const CERTIFICATE_POLICIES_HUMAN: &str = "Certificate policies";
pub const AUTHORITY_KEY_IDENTIFIER: &[u64] = &[2, 5, 29, 35];
pub const EXTENDED_KEY_USAGE: &[u64] = &[2, 5, 29, 37];
pub const EXTENDED_KEY_USAGE_HUMAN: &str = "Extended key usage";
pub const ANY_EXTENDED_KEY_USAGE: &[u64] = &[2, 5, 29, 37, 0];
pub const ANY_POLICY_IDENTIFIER: &[u64] = &[2, 5, 29, 32, 0];
pub const ANY_POLICY_IDENTIFIER_HUMANS: &str = "Any policy identifier";

pub const ECDSA_SIGNATURE_WITH_SHA_1: &[u64] = &[1, 2, 840, 10045, 4, 1];
pub const ECDSA_SIGNATURE_WITH_SHA_1_HUMAN: &str = "ECDSA Signature with SHA-1";
pub const ECDSA_SIGNATURE_WITH_SHA_224: &[u64] = &[1, 2, 840, 10045, 4, 3, 1];
pub const ECDSA_SIGNATURE_WITH_SHA_224_HUMAN: &str = "ECDSA Signature with SHA-224";
pub const ECDSA_SIGNATURE_WITH_SHA_256: &[u64] = &[1, 2, 840, 10045, 4, 3, 2];
pub const ECDSA_SIGNATURE_WITH_SHA_256_HUMAN: &str = "ECDSA Signature with SHA-256";
pub const ECDSA_SIGNATURE_WITH_SHA_384: &[u64] = &[1, 2, 840, 10045, 4, 3, 3];
pub const ECDSA_SIGNATURE_WITH_SHA_384_HUMAN: &str = "ECDSA Signature with SHA-384";
pub const ECDSA_SIGNATURE_WITH_SHA_512: &[u64] = &[1, 2, 840, 10045, 4, 3, 4];
pub const ECDSA_SIGNATURE_WITH_SHA_512_HUMAN: &str = "ECDSA Signature with SHA-512";
pub const ELLIPTIC_CURVE_PUBLIC_KEY: &[u64] = &[1, 2, 840, 10045, 2, 1];
pub const ELLIPTIC_CURVE_PUBLIC_KEY_HUMAN: &str = "Elliptic Curve Public Key";
pub const ELLIPTIC_CURVE_DOMAIN_SECP192R1: &[u64] = &[1, 2, 840, 10045, 3, 1, 1];
pub const ELLIPTIC_CURVE_DOMAIN_SECP192R1_HUMAN: &str = "Elliptic Curve secp192r1";
pub const ELLIPTIC_CURVE_DOMAIN_SECP256R1: &[u64] = &[1, 2, 840, 10045, 3, 1, 7];
pub const ELLIPTIC_CURVE_DOMAIN_SECP256R1_HUMAN: &str = "Elliptic Curve secp256r1";
pub const ELLIPTIC_CURVE_DOMAIN_SECP384R1: &[u64] = &[1, 3, 132, 0, 34];
pub const ELLIPTIC_CURVE_DOMAIN_SECP384R1_HUMAN: &str = "Elliptic Curve secp384r1";
pub const ELLIPTIC_CURVE_DOMAIN_SECP521R1: &[u64] = &[1, 3, 132, 0, 35];
pub const ELLIPTIC_CURVE_DOMAIN_SECP521R1_HUMAN: &str = "Elliptic Curve secp521r1";

pub const SHA1_WITH_RSA_ENCRYPTION: &[u64] = &[1, 2, 840, 113549, 1, 1, 5];
pub const SHA1_WITH_RSA_ENCRYPTION_HUMAN: &str = "SHA-1 with RSA Encryption";
pub const SHA256_WITH_RSA_ENCRYPTION: &[u64] = &[1, 2, 840, 113549, 1, 1, 11];
pub const SHA256_WITH_RSA_ENCRYPTION_HUMAN: &str = "SHA-256 with RSA Encryption";
pub const SHA384_WITH_RSA_ENCRYPTION: &[u64] = &[1, 2, 840, 113549, 1, 1, 12];
pub const SHA384_WITH_RSA_ENCRYPTION_HUMAN: &str = "SHA-384 with RSA Encryption";
pub const SHA512_WITH_RSA_ENCRYPTION: &[u64] = &[1, 2, 840, 113549, 1, 1, 13];
pub const SHA512_WITH_RSA_ENCRYPTION_HUMAN: &str = "SHA-512 with RSA Encryption";
pub const SHA224_WITH_RSA_ENCRYPTION: &[u64] = &[1, 2, 840, 113549, 1, 1, 14];
pub const SHA224_WITH_RSA_ENCRYPTION_HUMAN: &str = "SHA-224 with RSA Encryption";
pub const RSA_ENCRYPTION: &[u64] = &[1, 2, 840, 113549, 1, 1, 1];
pub const RSA_ENCRYPTION_HUMAN: &str = "RSA Encryption";
pub const EMAIL_ADDRESS: &[u64] = &[1, 2, 840, 113549, 1, 9, 1];

pub const PKIX: &[u64] = &[1, 3, 6, 1, 5, 5, 7];
pub const AUTHORITY_INFORMATION_ACCESS: &[u64] = &[1, 3, 6, 1, 5, 5, 7, 1, 1];
pub const CPS_URI: &[u64] = &[1, 3, 6, 1, 5, 5, 7, 2, 1];
pub const CPS_URI_HUMAN: &str = "CPS URI";
pub const USER_NOTICE: &[u64] = &[1, 3, 6, 1, 5, 5, 7, 2, 2];
pub const OCSP_BASIC_RESPONSE: &[u64] = &[1, 3, 6, 1, 5, 5, 7, 48, 1, 1];
pub const KEY_PURPOSE_SERVER_AUTH: &[u64] = &[1, 3, 6, 1, 5, 5, 7, 3, 1];
pub const KEY_PURPOSE_CLIENT_AUTH: &[u64] = &[1, 3, 6, 1, 5, 5, 7, 3, 2];
pub const KEY_PURPOSE_CODE_SIGNING: &[u64] = &[1, 3, 6, 1, 5, 5, 7, 3, 3];
pub const KEY_PURPOSE_EMAIL_PROTECTION: &[u64] = &[1, 3, 6, 1, 5, 5, 7, 3, 4];
pub const KEY_PURPOSE_TIME_STAMPING: &[u64] = &[1, 3, 6, 1, 5, 5, 7, 3, 8];
pub const KEY_PURPOSE_OCSP_SIGNIN: &[u64] = &[1, 3, 6, 1, 5, 5, 7, 3, 9];

pub const SHA1: &[u64] = &[1, 3, 14, 3, 2, 26];

pub const SHA256: &[u64] = &[2, 16, 840, 1, 101, 3, 4, 2, 1];
pub const SHA384: &[u64] = &[2, 16, 840, 1, 101, 3, 4, 2, 2];
pub const SHA512: &[u64] = &[2, 16, 840, 1, 101, 3, 4, 2, 3];
pub const SHA224: &[u64] = &[2, 16, 840, 1, 101, 3, 4, 2, 4];
pub const OV_SSL_CERTIFICATE: &[u64] = &[2, 16, 840, 1, 114412, 1, 1];
pub const ORGANIZATION_VALIDATED: &[u64] = &[2, 23, 140, 1, 2, 2];

pub type ObjectIdentifier = Box<[u64]>;

pub fn oid_to_str(oid:&[u64]) -> String {
    let mut result = Vec::with_capacity(oid.len() << 1);
    for i in 0 .. oid.len() {
        result.push(oid[i].to_string());
    }
    result.join(".")
}

pub fn oid_to_human_readable(oid: &[u64]) -> String {
    if &oid[..X_500_ATTRIBUTES.len()] == X_500_ATTRIBUTES {
        return oid_x500_to_human_readable(oid);
    }

    if &oid[..CERTIFICATE_EXTENSION.len()] == CERTIFICATE_EXTENSION {
        return oid_certificate_extension_to_human_readable(oid);
    }

    if oid.len() >= PKIX.len() && &oid[..PKIX.len()] == PKIX {
        return oid_pkix_to_human_readable(oid);
    }

    match &oid as &[u64] {
        SHA256_WITH_RSA_ENCRYPTION => SHA256_WITH_RSA_ENCRYPTION_HUMAN.to_string(),
        SHA384_WITH_RSA_ENCRYPTION => SHA384_WITH_RSA_ENCRYPTION_HUMAN.to_string(),
        SHA512_WITH_RSA_ENCRYPTION => SHA512_WITH_RSA_ENCRYPTION_HUMAN.to_string(),
        SHA224_WITH_RSA_ENCRYPTION => SHA224_WITH_RSA_ENCRYPTION_HUMAN.to_string(),
        SHA1_WITH_RSA_ENCRYPTION => SHA1_WITH_RSA_ENCRYPTION_HUMAN.to_string(),
        RSA_ENCRYPTION => RSA_ENCRYPTION_HUMAN.to_string(),
        EMAIL_ADDRESS => "Email Address".to_string(),
        ECDSA_SIGNATURE_WITH_SHA_1 => ECDSA_SIGNATURE_WITH_SHA_1_HUMAN.to_string(),
        ECDSA_SIGNATURE_WITH_SHA_256 => ECDSA_SIGNATURE_WITH_SHA_256_HUMAN.to_string(),
        ECDSA_SIGNATURE_WITH_SHA_224 => ECDSA_SIGNATURE_WITH_SHA_224_HUMAN.to_string(),
        ECDSA_SIGNATURE_WITH_SHA_384 => ECDSA_SIGNATURE_WITH_SHA_384_HUMAN.to_string(),
        ECDSA_SIGNATURE_WITH_SHA_512 => ECDSA_SIGNATURE_WITH_SHA_512_HUMAN.to_string(),
        ELLIPTIC_CURVE_PUBLIC_KEY => ELLIPTIC_CURVE_PUBLIC_KEY_HUMAN.to_string(),
        ELLIPTIC_CURVE_DOMAIN_SECP192R1 => ELLIPTIC_CURVE_DOMAIN_SECP192R1_HUMAN.to_string(),
        ELLIPTIC_CURVE_DOMAIN_SECP256R1 => ELLIPTIC_CURVE_DOMAIN_SECP256R1_HUMAN.to_string(),
        ELLIPTIC_CURVE_DOMAIN_SECP384R1 => ELLIPTIC_CURVE_DOMAIN_SECP384R1_HUMAN.to_string(),
        ELLIPTIC_CURVE_DOMAIN_SECP521R1 => ELLIPTIC_CURVE_DOMAIN_SECP521R1_HUMAN.to_string(),

        OV_SSL_CERTIFICATE => "Organizationally‐Validated (OV) Secure Sockets Layer (SSL) Certificate".to_string(),
        ORGANIZATION_VALIDATED => "Organization identity asserted".to_string(),
        SHA256 => "SHA256".to_string(),
        SHA384 => "SHA384".to_string(),
        SHA512 => "SHA512".to_string(),
        SHA224 => "SHA224".to_string(),
        SHA1 => "SHA1".to_string(),

        _ => oid_to_str(oid),
    }
}

fn oid_x500_to_human_readable(oid: &[u64]) -> String {
    match &oid as &[u64] {
        COUNTRY_OR_REGION => COUNTRY_OR_REGION_HUMAN.to_string(),
        ORGANIZATION => ORGANIZATION_HUMAN.to_string(),
        ORGANIZATION_UNIT => ORGANIZATION_UNIT_HUMAN.to_string(),
        COMMON_NAME => COMMON_NAME_HUMAN.to_string(),
        OTHER_NAME => "Other name".to_string(),
        SERIAL_NUMBER => "Serial number".to_string(),
        STATE_OR_PROVINCE => "State or province".to_string(),
        STREET_ADDRESS => "Street".to_string(),
        LOCALITY_NAME => "Locality".to_string(),
        _ => oid_to_str(oid),
    }
}

fn oid_certificate_extension_to_human_readable(oid: &[u64]) -> String {
    match &oid as &[u64] {
        SUBJECT_KEY_IDENTIFIER => "Subject Key Identifier".to_string(),
        KEY_USAGE => KEY_USAGE_HUMAN.to_string(),
        EXTENDED_KEY_USAGE => EXTENDED_KEY_USAGE_HUMAN.to_string(),
        BASIC_CONSTRAINTS => BASIC_CONSTRAINTS_HUMAN.to_string(),
        CERTIFICATE_POLICIES => CERTIFICATE_POLICIES_HUMAN.to_string(),
        AUTHORITY_KEY_IDENTIFIER => "Authority Key Identifier".to_string(),
        ANY_POLICY_IDENTIFIER => ANY_POLICY_IDENTIFIER_HUMANS.to_string(),
        ANY_EXTENDED_KEY_USAGE => "Any extended key usage".to_string(),
        SUBJECT_ALTERNATIVE_NAME => SUBJECT_ALTERNATIVE_NAME_HUMAN.to_string(),
        ISSUER_ALTERNATIVE_NAME => "Issuer alternative name".to_string(),
        CRL_DISTRIBUTION_POINT => "CRL distribution point".to_string(),
        _ => oid_to_str(oid),
    }
}

fn oid_pkix_to_human_readable(oid: &[u64]) -> String {
    match &oid as &[u64] {
        CPS_URI => CPS_URI_HUMAN.to_string(),
        AUTHORITY_INFORMATION_ACCESS => "Authority information access".to_string(),
        USER_NOTICE => "User notice".to_string(),
        OCSP_BASIC_RESPONSE => "OCSP basic response".to_string(),

        KEY_PURPOSE_SERVER_AUTH => "Server authentication".to_string(),
        KEY_PURPOSE_CLIENT_AUTH => "Client authentication".to_string(),
        KEY_PURPOSE_CODE_SIGNING => "Code signin".to_string(),
        KEY_PURPOSE_EMAIL_PROTECTION => "Email protection".to_string(),
        KEY_PURPOSE_TIME_STAMPING => "Time stamping".to_string(),
        KEY_PURPOSE_OCSP_SIGNIN => "OCSP signin".to_string(),
        _ => oid_to_str(oid),
    }
}

pub fn from_str(string:&str) -> Result<ObjectIdentifier, X509Error>{
    let result = match string {
        SHA256_WITH_RSA_ENCRYPTION_HUMAN => Box::from(SHA256_WITH_RSA_ENCRYPTION),
        SHA384_WITH_RSA_ENCRYPTION_HUMAN => Box::from(SHA384_WITH_RSA_ENCRYPTION),
        SHA512_WITH_RSA_ENCRYPTION_HUMAN => Box::from(SHA512_WITH_RSA_ENCRYPTION),
        SHA224_WITH_RSA_ENCRYPTION_HUMAN => Box::from(SHA224_WITH_RSA_ENCRYPTION),
        SHA1_WITH_RSA_ENCRYPTION_HUMAN => Box::from(SHA1_WITH_RSA_ENCRYPTION),
        ECDSA_SIGNATURE_WITH_SHA_256_HUMAN => Box::from(ECDSA_SIGNATURE_WITH_SHA_256),
        ECDSA_SIGNATURE_WITH_SHA_224_HUMAN => Box::from(ECDSA_SIGNATURE_WITH_SHA_224),
        ECDSA_SIGNATURE_WITH_SHA_384_HUMAN => Box::from(ECDSA_SIGNATURE_WITH_SHA_384),
        ECDSA_SIGNATURE_WITH_SHA_512_HUMAN => Box::from(ECDSA_SIGNATURE_WITH_SHA_512),
        ECDSA_SIGNATURE_WITH_SHA_1_HUMAN => Box::from(ECDSA_SIGNATURE_WITH_SHA_1),
        ELLIPTIC_CURVE_PUBLIC_KEY_HUMAN => Box::from(ELLIPTIC_CURVE_PUBLIC_KEY),
        ELLIPTIC_CURVE_DOMAIN_SECP192R1_HUMAN => Box::from(ELLIPTIC_CURVE_DOMAIN_SECP192R1),
        ELLIPTIC_CURVE_DOMAIN_SECP256R1_HUMAN => Box::from(ELLIPTIC_CURVE_DOMAIN_SECP256R1),
        ELLIPTIC_CURVE_DOMAIN_SECP384R1_HUMAN => Box::from(ELLIPTIC_CURVE_DOMAIN_SECP384R1),
        ELLIPTIC_CURVE_DOMAIN_SECP521R1_HUMAN => Box::from(ELLIPTIC_CURVE_DOMAIN_SECP521R1),
        RSA_ENCRYPTION_HUMAN => Box::from(RSA_ENCRYPTION),
        KEY_USAGE_HUMAN => Box::from(KEY_USAGE),
        EXTENDED_KEY_USAGE_HUMAN => Box::from(EXTENDED_KEY_USAGE),
        BASIC_CONSTRAINTS_HUMAN => Box::from(BASIC_CONSTRAINTS),
        CERTIFICATE_POLICIES_HUMAN => Box::from(CERTIFICATE_POLICIES),
        SUBJECT_ALTERNATIVE_NAME_HUMAN => Box::from(SUBJECT_ALTERNATIVE_NAME),
        COUNTRY_OR_REGION_HUMAN => Box::from(COUNTRY_OR_REGION),
        COMMON_NAME_HUMAN => Box::from(COMMON_NAME),
        ORGANIZATION_HUMAN => Box::from(ORGANIZATION),
        ORGANIZATION_UNIT_HUMAN => Box::from(ORGANIZATION_UNIT),
        ANY_POLICY_IDENTIFIER_HUMANS => Box::from(ANY_POLICY_IDENTIFIER),
        CPS_URI_HUMAN => Box::from(CPS_URI),
        _ => {
            string.split(".").map(|part| u64::from_str(part))
                .collect::<Result<Vec<_>, _>>()?.into_boxed_slice()
        }
    };

    Ok(result)
}

pub fn from_json(json: JSON) -> Result<Box<[u64]>, JSONError> {
    match json {
        JSON::JSONArray(array) => array.into_iter()
            .map(|item|item.resolve_into_number())
            .map(|item|item.map(|item|item as u64))
            .collect::<Result<Box<[u64]>, _>>(),
        JSON::JSONString(string) => from_str(&string).map_err(|_|JSONErrorKind::Resolve.into()),
        _ => Err(JSONErrorKind::Resolve.into()),
    }
}



