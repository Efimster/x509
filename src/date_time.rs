use std::fmt;
use json::{error::{JSONError, JSONErrorKind}, JSON};

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct DateTime {
    pub year: i64,
    pub month: u8,
    pub day: u8,
    pub hours: u8,
    pub minutes: u8,
    pub seconds: u8,
}


impl DateTime {
    pub fn new(year:i64, month:u8, day:u8, hours: u8, minutes:u8, seconds:u8) -> DateTime {
        debug_assert!(month <= 12);
        debug_assert!(day <= 31);
        debug_assert!(hours < 24);
        debug_assert!(minutes < 60);
        debug_assert!(seconds < 60);

        DateTime {
            year,
            month,
            day,
            hours,
            minutes,
            seconds,
        }
    }
}

impl DateTime {
    pub fn to_der_string(&self) -> String{
        if self.year < 2050 {
            format!("{:02}{:02}{:02}{:02}{:02}{:02}Z", self.year % 100, self.month, self.day, self.hours, self.minutes, self.seconds)
        }
        else {
            format!("{:04}{:02}{:02}{:02}{:02}{:02}Z", self.year, self.month, self.day, self.hours, self.minutes, self.seconds)
        }
    }
}

impl fmt::Display for DateTime {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:02}.{:02}.{:04} {:02}:{:02}:{:02} GMT", self.day, self.month, self.year, self.hours, self.minutes, self.seconds)
    }
}

impl TryFrom<JSON> for DateTime{
    type Error = JSONError;

    fn try_from(value: JSON) -> Result<Self, Self::Error> {
        let string = value.resolve_into_string()?;
        let (year, string) = if string.len() > 13 {
            (i64::from_str_radix(&string[..4], 10).map_err(|_|JSONErrorKind::Resolve)?, &string[4..])
        } else {
            let mut year = i64::from_str_radix(&string[..2], 10).map_err(|_|JSONErrorKind::Resolve)?;
            year = if year < 50 { 2000 + year} else { 1900 + year};
            (year, &string[2..])
        };

        let month = u8::from_str_radix(&string[..2], 10).map_err(|_|JSONErrorKind::Resolve)?;
        let day = u8::from_str_radix(&string[2..4], 10).map_err(|_|JSONErrorKind::Resolve)?;
        let hour = u8::from_str_radix(&string[4..6], 10).map_err(|_|JSONErrorKind::Resolve)?;
        let minutes = u8::from_str_radix(&string[6..8], 10).map_err(|_|JSONErrorKind::Resolve)?;
        let seconds = u8::from_str_radix(&string[8..10], 10).map_err(|_|JSONErrorKind::Resolve)?;
        Ok(DateTime::new(year, month, day, hour, minutes, seconds))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_display() {
        assert_eq!(format!("{}", DateTime::new(1981, 1, 10, 0, 1, 12)), "10.01.1981 00:01:12 GMT");
        assert_eq!(format!("{}", DateTime::new(2050, 12, 31, 2, 41, 7)), "31.12.2050 02:41:07 GMT");
    }
}
