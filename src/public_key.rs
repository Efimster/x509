use crate::rsa::rsa_public_key::RSAPublicKey;
use std::fmt;
use ecc::point::EccPoint;
use crate::display;
use crate::oid::ObjectIdentifier;

#[derive(Debug, Clone)]
pub enum PublicKey {
    None,
    RSA(RSAPublicKey),
    ECDSA(EccPoint),
}

impl fmt::Display for PublicKey {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            PublicKey::RSA(key) => f.write_str(&format!("{:<1}", key))?,
            PublicKey::ECDSA(point) => {
                let tab = "\t".repeat(f.width().unwrap_or_default());
                write!(f, "{}{:<1}", tab, display::display_octets(&point.to_octet_string(false), 20))?
            },
            _ => (),
        }
        Ok(())
    }
}

impl PublicKey {
    pub fn to_oid(&self) -> ObjectIdentifier {
        match self {
            PublicKey::RSA(_) => Box::new([1, 2, 840, 113549, 1, 1, 1]),
            PublicKey::ECDSA(_) => Box::new([1, 2, 840, 10045, 2, 1]),
            PublicKey::None => Box::new([]),
        }
    }
}