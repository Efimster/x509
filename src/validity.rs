use crate::asn1::encode::EncodeTo;
use crate::asn1::encode;
use crate::date_time::DateTime;
use crate::asn1::decode::DecodeFrom;
use crate::x509_error::X509Error;
use crate::asn1::decode;
use std::fmt;
use json::JSON;
use json::error::JSONError;

#[derive(Debug)]
pub struct Validity {
    pub not_before:DateTime,
    pub not_after:DateTime,
}

impl EncodeTo for Validity {
    fn encode(&self) -> Box<[u8]> {
        let mut result = encode::encode_time(self.not_before).into_vec();
        result.extend_from_slice(&encode::encode_time(self.not_after));
        encode::encode_sequence(&result)
    }
}

impl DecodeFrom for Validity {
    fn decode(bytes: &[u8]) -> Result<(Validity, usize), X509Error> {
        let ( bytes, total_encoded_size) = decode::uncover_sequence(bytes)?;
        let (not_before, _, bytes) = decode::decode_time(bytes)?;
        let (not_after, _, _) = decode::decode_time(bytes)?;
        Ok((Validity {
            not_before,
            not_after,
        }, total_encoded_size))
    }
}

impl fmt::Display for Validity {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let width = f.width().unwrap_or_default();
        f.write_str(&format!("{}Not valid before: {}\n", "\t".repeat(width), self.not_before))?;
        f.write_str(&format!("{}Not valid after: {}", "\t".repeat(width), self.not_after))
    }
}

impl TryFrom<JSON> for Validity {
    type Error = JSONError;

    fn try_from(value: JSON) -> Result<Self, Self::Error> {
        let mut props = value.resolve_into_object()?;
        let not_before = JSON::remove_property(&mut props,"notBefore")?.try_into()?;
        let not_after = JSON::remove_property(&mut props, "notAfter")?.try_into()?;
        Ok(Validity {
            not_before,
            not_after,
        })
    }
}