use crate::ecdsa::ecdsa_signature::EcdsaSignature;
use core::fmt;
use crate::display;

#[derive(Debug)]
pub enum Signature {
    None,
    RSA(Box<[u8]>),
    ECDSA(EcdsaSignature),
}

impl fmt::Display for Signature {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {

        match self {
            Signature::RSA(bytes) => write!(f, "Signature value: {}", display::display_octets(&bytes, 10)),
            Signature::ECDSA(ecdsa_signature) => write!(f, "Signature r: {} s: {}",
                display::display_octets(&ecdsa_signature.r, 10), display::display_octets(&ecdsa_signature.s, 10)),
            Signature::None => write!(f, "NO Signature"),
        }
    }
}