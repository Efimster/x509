use crate::x509_error::{X509Error, X509ErrorKind};

const UNIVERSAL:u8 = 0;
const APPLICATION: u8 = 1;
const CONTEXT_SPECIFIC: u8 = 2;
const PRIVATE: u8 = 3;

#[derive(PartialEq, Debug)]
pub enum TagClass {
    Universal,
    Application,
    ContextSpecific,
    Private
}

impl TagClass {
    pub fn to_u8(&self) -> u8 {
        match self {
            TagClass::Universal => UNIVERSAL,
            TagClass::Application => APPLICATION,
            TagClass::ContextSpecific => CONTEXT_SPECIFIC,
            TagClass::Private => PRIVATE,
        }
    }

    pub fn from_u8(byte:u8) -> Result<TagClass, X509Error> {
        match byte {
            UNIVERSAL => Ok(TagClass::Universal),
            APPLICATION => Ok(TagClass::Application),
            CONTEXT_SPECIFIC => Ok(TagClass::ContextSpecific),
            PRIVATE => Ok(TagClass::Private),
            _ => Err(X509ErrorKind::Decode.into())
        }
    }
}


