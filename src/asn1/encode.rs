use super::tag_class::TagClass;
use super::tag_complexity::TagComplexity;
use crate::universal_type::UniversalType;
use crate::date_time::DateTime;

const U64_7BIT_VECTOR_CAPACITY:usize = 64/7 + 1;

pub trait EncodeTo {
    fn encode_to(&self, to: &mut Vec<u8>) {
        to.extend_from_slice(&Self::encode(self));
    }
    fn encode(&self) -> Box<[u8]>;
}

pub fn encode_u64_7_bit_be(mut value: u64) -> Box<[u8]> {
    let mut result:Vec<u8> = Vec::new();
    let mut byte = (value as u8) & 0x7f;
    result.push(byte);
    value >>= 7;

    while value > 0 {
        byte = (value as u8) & 0x7f | 0x80;
        result.push(byte);
        value >>= 7;
    }

    result.reverse();
    result.into_boxed_slice()
}

pub fn encode_tag_identifier(tag_class:TagClass, tag_complexity:TagComplexity, tag_number:u64) -> Box<[u8]> {

    if tag_number <= 31 {
        Box::new ([(tag_class.to_u8() << 6) | (tag_complexity.to_u8() << 5) | (tag_number as u8)])
    }
    else {
        let mut first_byte: u8 = (tag_class.to_u8() << 6) | (tag_complexity.to_u8() << 5);
        first_byte |= 0b11111;
        let tag_number = encode_u64_7_bit_be(tag_number);
        let mut result: Vec<u8> = Vec::with_capacity(U64_7BIT_VECTOR_CAPACITY + 1);
        result.push(first_byte);
        result.extend_from_slice(&tag_number);
        result.into_boxed_slice()
    }
}

pub fn encode_tag_universal_identifier(tag_complexity: TagComplexity, universal_type: UniversalType) -> u8 {
    (TagClass::Universal.to_u8() << 6) | (tag_complexity.to_u8() << 5) | (universal_type.to_u8())
}

pub fn encode_length(length: usize) -> Box<[u8]> {
    if length < 128 {
        Box::new([encode_length_short_form(length)])
    }
    else {
        encode_length_long_form(length)
    }
}

#[inline]
pub fn encode_length_short_form(length:usize) -> u8 {
    length as u8
}

pub fn encode_length_long_form(length: usize) -> Box<[u8]> {
    let bytes: [u8; 8] = unsafe { std::mem::transmute(length.to_be()) };
    let mut index = 0;
    while index < bytes.len() - 1 && bytes[index] == 0 {
        index += 1;
    }
    let bytes = &bytes[index..];
    let mut result: Vec<u8> = Vec::with_capacity(10);
    result.push((bytes.len() as u8) | 0x80);
    result.extend_from_slice(bytes);
    result.into_boxed_slice()
}

pub fn encode_integer(value: i64) -> Box<[u8]> {
    let mut result:Vec<u8> = Vec::with_capacity(10);
    result.push(encode_tag_universal_identifier(TagComplexity::Primitive,UniversalType::Integer));
    let bytes:[u8; 8] = unsafe { std::mem::transmute(value.to_be())};
    let skip = 0u8.wrapping_sub((bytes[0] & 0x80) >> 7);
    let mut index = 0;
    while index < bytes.len() - 1 && bytes[index] == skip {
        index += 1;
    }

    if 0u8.wrapping_sub((bytes[index] & 0x80) >> 7) != skip {
        index -= 1;
    }

    let bytes = &bytes[index ..];

    result.extend_from_slice( &encode_length(bytes.len()));
    result.extend_from_slice(bytes);
    result.into_boxed_slice()
}

pub fn encode_universal_bytes(tag_complexity:TagComplexity, universal_type:UniversalType, inner_bytes:&[u8]) -> Box<[u8]>{
    let mut result:Vec<u8> = Vec::with_capacity(3 + (inner_bytes.len() >> 8) + inner_bytes.len());
    result.push(encode_tag_universal_identifier(tag_complexity, universal_type));
    result.extend_from_slice(&encode_length(inner_bytes.len()));
    result.extend_from_slice(inner_bytes);
    result.into_boxed_slice()
}

pub fn encode_bytes(tag_class:TagClass, tag_complexity: TagComplexity, tag_num: u64, inner_bytes: &[u8]) -> Box<[u8]> {
    let mut result: Vec<u8> = Vec::with_capacity(3 + (inner_bytes.len() >> 8) + inner_bytes.len());
    result.extend_from_slice(&encode_tag_identifier(tag_class, tag_complexity, tag_num));
    result.extend_from_slice(&encode_length(inner_bytes.len()));
    result.extend_from_slice(inner_bytes);
    result.into_boxed_slice()
}

pub fn encode_sequence(inner_bytes: &[u8]) -> Box<[u8]>{
    encode_universal_bytes(TagComplexity::Constructed, UniversalType::Sequence, inner_bytes)
}

pub fn encode_object_identifier(oid:&[u64]) -> Box<[u8]> {
    debug_assert!(oid.len() > 1);
    let first_sub_identifier = oid[0] * 40 + oid[1];
    let mut result: Vec<u8> = Vec::with_capacity((U64_7BIT_VECTOR_CAPACITY + 1) * oid.len());
    result.extend_from_slice(&encode_u64_7_bit_be(first_sub_identifier));

    if oid.len() > 2 {
        let oid = &oid[2..];
        for sub_identifier in oid.iter() {
            result.extend_from_slice(&encode_u64_7_bit_be(*sub_identifier));
        }
    }

    encode_universal_bytes(TagComplexity::Primitive, UniversalType::ObjectIdentifier, &result)
}

#[inline]
pub fn encode_octet_string(octets:&[u8]) -> Box<[u8]>{
    encode_universal_bytes(TagComplexity::Primitive, UniversalType::OctetString, octets)
}

#[inline]
pub fn encode_set(inner_bytes: &[u8]) -> Box<[u8]> {
    encode_universal_bytes(TagComplexity::Constructed, UniversalType::Set, inner_bytes)
}

pub fn encode_time(time:DateTime) -> Box<[u8]> {
    if time.year < 2050 {
        encode_utc_time(time)
    }
    else {
        encode_generalized_time(time)
    }
}

pub fn encode_utc_time(time: DateTime) -> Box<[u8]> {
    let time = format!("{:02}{:02}{:02}{:02}{:02}{:02}Z", time.year % 100, time.month, time.day, time.hours, time.minutes, time.seconds);
    encode_universal_bytes(TagComplexity::Primitive, UniversalType::UtcTime, time.as_bytes())
}

pub fn encode_generalized_time(time: DateTime) -> Box<[u8]> {
    let time = format!("{:04}{:02}{:02}{:02}{:02}{:02}Z", time.year, time.month, time.day, time.hours, time.minutes, time.seconds);
    encode_universal_bytes(TagComplexity::Primitive, UniversalType::GeneralizedTime, time.as_bytes())
}



pub fn encode_null() -> Box<[u8]> {
    let identifier = encode_tag_universal_identifier(TagComplexity::Primitive, UniversalType::Null);
    Box::new([identifier, 0])
}

#[inline]
pub fn encode_bit_string(octets: &[u8]) -> Box<[u8]> {
    let mut result: Vec<u8> = Vec::with_capacity(4 + (octets.len() >> 8) + octets.len());
    result.push(encode_tag_universal_identifier(TagComplexity::Primitive, UniversalType::BitString));
    result.extend_from_slice(&encode_length(octets.len() + 1));
    result.push(0);
    result.extend_from_slice(octets);
    result.into_boxed_slice()
}

pub fn encode_bool(value:bool) -> Box<[u8]> {

    let value = match value {
        true => 0xffu8,
        false => 0x00u8
    };

    Box::new([
        encode_tag_universal_identifier(TagComplexity::Primitive, UniversalType::Boolean),
        encode_length_short_form(1),
        value
    ])
}

pub fn encode_printable_string(octets: &[u8]) -> Box<[u8]> {
    encode_universal_bytes(TagComplexity::Primitive, UniversalType::PrintableString, octets)
}

pub fn encode_utf8_string(octets: &[u8]) -> Box<[u8]> {
    encode_universal_bytes(TagComplexity::Primitive, UniversalType::Utf8String, octets)
}

pub fn encode_ia5_string(octets: &[u8]) -> Box<[u8]> {
    encode_universal_bytes(TagComplexity::Primitive, UniversalType::IA5String, octets)
}

pub fn encode_big_integer(octets: &[u8]) -> Box<[u8]> {
    encode_universal_bytes(TagComplexity::Primitive, UniversalType::Integer, octets)
}

pub fn encode_big_unsigned_integer(octets: &[u8]) -> Box<[u8]> {

    if octets[0] & 0x80 != 0 {
        let mut result:Vec<u8> = Vec::with_capacity(octets.len() + 1);
        result.push(0);
        result.extend_from_slice(&octets);
        encode_universal_bytes(TagComplexity::Primitive, UniversalType::Integer, &result)
    }
    else {
        encode_universal_bytes(TagComplexity::Primitive, UniversalType::Integer, octets)
    }
}

pub fn encode_explicit(tag_num:u64, bytes:&[u8]) -> Box<[u8]> {
    encode_bytes(TagClass::ContextSpecific, TagComplexity::Constructed,
        tag_num, bytes)
}

pub fn encode_implicit(tag_num: u64, bytes: &mut [u8]){
    bytes[0] = (bytes[0] & 0b00_1_00000) | (TagClass::ContextSpecific.to_u8() << 6) | tag_num as u8;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_encode_u64_7_bit(){
        assert_eq!(&encode_u64_7_bit_be(180) as &[u8], &[0x81, 0x34]);
        assert_eq!(&encode_u64_7_bit_be(0) as &[u8], &[0x0]);
        assert_eq!(&encode_u64_7_bit_be(0x71) as &[u8], &[0x71]);
        assert_eq!(&encode_u64_7_bit_be(0x8080) as &[u8], &[0x82, 0x81, 0x0]);
    }

    #[test]
    fn test_encode_integer(){
        assert_eq!(&encode_integer(0) as &[u8], [2, 1, 0]);
        assert_eq!(&encode_integer(127) as &[u8], [2, 1, 0x7f]);
        assert_eq!(&encode_integer(128) as &[u8], [2, 2, 0, 0x80]);
        assert_eq!(&encode_integer(256) as &[u8], [2, 2, 1, 0]);
        assert_eq!(&encode_integer(-128) as &[u8], [2, 1, 0x80]);
        assert_eq!(&encode_integer(-129) as &[u8], [2, 2, 0xff, 0x7f]);
        assert_eq!(&encode_integer(i64::min_value()) as &[u8], [2, 8, 0x80, 0, 0, 0, 0, 0, 0, 0]);
    }

    #[test]
    fn test_encode_length_long_form(){
        assert_eq!(&encode_length_long_form(1) as &[u8], [0x81, 0x1]);
        assert_eq!(&encode_length_long_form(256) as &[u8], [0x82, 0x1, 0x0]);
        assert_eq!(&encode_length_long_form(201) as &[u8], [0x81, 0xc9]);
    }

    #[test]
    fn test_encode_time() {
        let date_time = DateTime {
            year: 2000,
            month: 1,
            day: 2,
            hours: 3,
            minutes: 4,
            seconds: 5
        };

        let mut result:Vec<u8> = Vec::with_capacity(20);
        result.push(23);
        result.push(13);
        result.extend_from_slice("000102030405Z".as_bytes());
        assert_eq!(&encode_time(date_time)[..], &result as &[u8]);

        let date_time = DateTime {
            year: 3000,
            month: 10,
            day: 2,
            hours: 23,
            minutes: 34,
            seconds: 45
        };
        let mut result: Vec<u8> = Vec::with_capacity(20);
        result.push(24);
        result.push(15);
        result.extend_from_slice("30001002233445Z".as_bytes());
        assert_eq!(&encode_time(date_time) as &[u8], &result as &[u8]);
    }
}