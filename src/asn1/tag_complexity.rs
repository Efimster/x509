use crate::x509_error::{X509Error, X509ErrorKind};

const PRIMITIVE:u8 = 0;
const CONSTRUCTED:u8 = 1;

#[derive(PartialEq, Debug)]
pub enum TagComplexity {
    Primitive,
    Constructed,
}

impl TagComplexity {
    pub fn to_u8(&self) -> u8 {
        match self {
            TagComplexity::Primitive => PRIMITIVE,
            TagComplexity::Constructed => CONSTRUCTED,
        }
    }

    pub fn from_u8(byte: u8) -> Result<TagComplexity, X509Error> {
        match byte {
            PRIMITIVE => Ok(TagComplexity::Primitive),
            CONSTRUCTED => Ok(TagComplexity::Constructed),
            _ => Err(X509ErrorKind::Decode.into()),
        }
    }
}