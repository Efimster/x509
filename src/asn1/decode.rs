use crate::x509_error::{X509Error, X509ErrorKind};
use crate::asn1::tag_class::TagClass;
use crate::asn1::tag_complexity::TagComplexity;
use crate::universal_type::UniversalType;
use crate::date_time::DateTime;
use std::str;
use crate::oid::ObjectIdentifier;

pub trait DecodeFrom {
    fn decode(bytes: &[u8]) -> Result<(Self, usize), X509Error> where Self: std::marker::Sized;
    fn decode_cut(bytes: &[u8]) -> Result<(Self, &[u8]), X509Error> where Self: std::marker::Sized{
        let (result, encoded_size) = Self::decode(bytes)?;
        Ok((result, &bytes[encoded_size..]))
    }
}

pub fn decode_7_bit_be_to_u64(bytes:&[u8]) -> (u64, usize) {
    let mut result:u64 = 0;
    let mut index = 0usize;
    loop {
        result <<= 7;
        result |= (bytes[index] & 0x7f) as u64;
        if bytes[index] & 0x80 == 0 {
            break;
        }
        index += 1;
    }

    (result, index + 1)
}

pub fn decode_tag_identifier(bytes:&[u8]) -> Result<(TagClass, TagComplexity, u64, usize), X509Error>{
    let byte = bytes[0];
    let tag_number:u64 = (byte & 0b11111) as u64;
    let tag_class = TagClass::from_u8(byte >> 6)?;
    let tag_complexity = TagComplexity::from_u8((byte >> 5) & 0x01)?;

    if tag_number != 0b11111 {
        Ok((tag_class, tag_complexity, tag_number, 1))
    }
    else {
        let (tag_number, encoded_size) = decode_7_bit_be_to_u64(&bytes[1..]);
        Ok((tag_class, tag_complexity, tag_number, encoded_size + 1))
    }
}

pub fn decode_length(bytes:&[u8]) -> Result<(usize, usize), X509Error> {
    if bytes[0] & 0x80 == 0 {
        Ok((bytes[0] as usize, 1))
    }
    else {
        decode_length_long_form(bytes)
    }
}

pub fn decode_length_long_form(bytes: &[u8]) -> Result<(usize, usize), X509Error> {
    let len = (bytes[0] & 0x7f) as usize;
    let bytes = &bytes[1..];
    let mut result: [u8; 8] = [0; 8];
    for i in 0..len {
        result[len - i - 1] = bytes[i];
    }
    Ok((usize::from_le_bytes(result), len + 1))
}

pub fn decode_tag_and_length(bytes:&[u8]) -> Result<(TagClass, TagComplexity, u64, usize, usize), X509Error> {
    let (tag_class, tag_complexity, tag_number, encoded_size) = decode_tag_identifier(bytes)?;
    let mut total_encoded_size = encoded_size;
    let bytes = &bytes[encoded_size..];
    let (length, encoded_size) = decode_length(bytes)?;
    total_encoded_size += encoded_size;
    Ok((tag_class, tag_complexity, tag_number, length, total_encoded_size))
}

pub fn decode_integer(bytes:&[u8]) -> Result<(i64, usize, &[u8]), X509Error> {
    let (_, tag_complexity, tag_number, length, encoded_size) = decode_tag_and_length(bytes)?;
    if UniversalType::from_u64(tag_number)? != UniversalType::Integer || tag_complexity != TagComplexity::Primitive{
        return Err(X509ErrorKind::Decode.into())
    }

    let total_encoded_size = encoded_size + length;
    let value_bytes = &bytes[encoded_size..total_encoded_size];

    let skip = 0u8.wrapping_sub((value_bytes[0] & 0x80) >> 7);
    let mut result: [u8; 8] = [0; 8];
    result[value_bytes.len() - 1] = value_bytes[0];

    for i in 1 .. value_bytes.len() {
        result[value_bytes.len() - i - 1] = value_bytes[i];
    }

    for i in value_bytes.len() .. 8 {
        result[i] = skip;
    }

    Ok((i64::from_le_bytes(result), total_encoded_size, &bytes[total_encoded_size .. ]))
}

pub fn decode_big_integer(bytes: &[u8]) -> Result<(Box<[u8]>, usize, &[u8]), X509Error> {
    let (_, tag_complexity, tag_number, length, encoded_size) = decode_tag_and_length(bytes)?;
    if UniversalType::from_u64(tag_number)? != UniversalType::Integer || tag_complexity != TagComplexity::Primitive {
        return Err(X509ErrorKind::Decode.into())
    }

    let total_encoded_size = encoded_size + length;
    let result = &bytes[encoded_size..total_encoded_size];

    Ok((Box::from(result), total_encoded_size, &bytes[total_encoded_size ..]))
}

pub fn decode_big_unsigned_integer(bytes: &[u8]) -> Result<(Box<[u8]>, usize, &[u8]), X509Error> {
    let (result, total_encoded_size, remain_bytes) = decode_big_integer(bytes)?;
    let result:&[u8] = if result[0] == 0 && result.len() > 1 && result[1] & 0x80 != 0 {
        &result[1..]
    }
    else {
        &result
    };

    Ok((Box::from(result), total_encoded_size, remain_bytes))
}

pub fn uncover_sequence(bytes:&[u8]) -> Result<(&[u8], usize), X509Error>{
    let (_, tag_complexity, tag_number, length, encoded_size) = decode_tag_and_length(bytes)?;
    if UniversalType::from_u64(tag_number)? != UniversalType::Sequence || tag_complexity != TagComplexity::Constructed{
        return Err(X509ErrorKind::Decode.into())
    }

    let total_encoded_size = encoded_size + length;
    Ok((&bytes[encoded_size .. total_encoded_size], total_encoded_size))
}

pub fn uncover_set(bytes: &[u8]) -> Result<(&[u8], usize), X509Error> {
    let (_, tag_complexity, tag_number, length, encoded_size) = decode_tag_and_length(bytes)?;
    if UniversalType::from_u64(tag_number)? != UniversalType::Set || tag_complexity != TagComplexity::Constructed {
        return Err(X509ErrorKind::Decode.into())
    }

    let total_encoded_size = encoded_size + length;
    Ok((&bytes[encoded_size..total_encoded_size], total_encoded_size))
}

fn decode_universal_octets(bytes: &[u8], universal_type:UniversalType) -> Result<(Box<[u8]>, usize, &[u8]), X509Error> {
    let (_, tag_complexity, tag_number, length, encoded_size) = decode_tag_and_length(bytes)?;
    if UniversalType::from_u64(tag_number)? != universal_type || tag_complexity != TagComplexity::Primitive {
        return Err(X509ErrorKind::Decode.into())
    }
    let total_encoded_size = encoded_size + length;
    let result = (&bytes[encoded_size..total_encoded_size]).to_vec().into_boxed_slice();
    Ok((result, total_encoded_size, &bytes[total_encoded_size..]))
}

pub fn decode_bytes(bytes: &[u8], tag_class:TagClass, tag_number: u64) -> Result<(Box<[u8]>, usize, &[u8]), X509Error> {
    let (defined_tag_class, tag_complexity, defined_tag_number, length, encoded_size) = decode_tag_and_length(bytes)?;
    if tag_number != defined_tag_number || tag_complexity != TagComplexity::Primitive || tag_class != defined_tag_class {
        return Err(X509ErrorKind::Decode.into())
    }
    let total_encoded_size = encoded_size + length;
    let result = (&bytes[encoded_size..total_encoded_size]).to_vec().into_boxed_slice();
    Ok((result, total_encoded_size, &bytes[total_encoded_size..]))
}

pub fn decode_octet_string(bytes: &[u8]) -> Result<(Box<[u8]>, usize, &[u8]), X509Error> {
    decode_universal_octets(bytes, UniversalType::OctetString)
}

pub fn decode_bit_string(bytes: &[u8]) -> Result<(Box<[u8]>, usize, &[u8]), X509Error> {
    let (mut result, encoded_size, bytes) = decode_universal_octets(bytes, UniversalType::BitString)?;
    result[result.len() - 1] &= 0xff << result[0];
    Ok(((&result[1 ..]).to_vec().into_boxed_slice(), encoded_size, bytes))
}

pub fn decode_printable_string(bytes: &[u8]) -> Result<(Box<[u8]>, usize, &[u8]), X509Error> {
    decode_universal_octets(bytes, UniversalType::PrintableString)
}

pub fn decode_utf8_string(bytes: &[u8]) -> Result<(Box<[u8]>, usize, &[u8]), X509Error> {
    decode_universal_octets(bytes, UniversalType::Utf8String)
}

pub fn decode_ia5_string(bytes: &[u8]) -> Result<(Box<[u8]>, usize, &[u8]), X509Error> {
    decode_universal_octets(bytes, UniversalType::IA5String)
}

pub fn decode_visible_string(bytes: &[u8]) -> Result<(Box<[u8]>, usize, &[u8]), X509Error> {
    decode_universal_octets(bytes, UniversalType::VisibleString)
}

pub fn decode_string(bytes: &[u8]) -> Result<(Box<[u8]>, usize, &[u8]), X509Error> {
    let (_, _, tag_number, _) = decode_tag_identifier(bytes)?;
    match UniversalType::from_u64(tag_number)? {
        UniversalType::PrintableString => decode_printable_string(bytes),
        UniversalType::Utf8String => decode_utf8_string(bytes),
        UniversalType::IA5String => decode_ia5_string(bytes),
        UniversalType::VisibleString => decode_visible_string(bytes),
        _ => Err(X509ErrorKind::Decode.into()),
    }
}

pub fn decode_object_identifier(bytes: &[u8]) -> Result<(ObjectIdentifier, usize, &[u8]), X509Error> {
    let (_, tag_complexity, tag_number, length, encoded_size) = decode_tag_and_length(bytes)?;
    if UniversalType::from_u64(tag_number)? != UniversalType::ObjectIdentifier || tag_complexity != TagComplexity::Primitive {
        return Err(X509ErrorKind::Decode.into())
    }

    let total_length = encoded_size + length;
    let mut value_bytes = &bytes[encoded_size..];
    value_bytes = &value_bytes[.. length];
    let mut result:Vec<u64> = Vec::new();
    let (sub_identifier, encoded_size) = decode_7_bit_be_to_u64(value_bytes);
    result.push(sub_identifier/40);
    result.push(sub_identifier % 40);
    value_bytes = &value_bytes[encoded_size..];

    while value_bytes.len() > 0 {
        let (sub_identifier, encoded_size) = decode_7_bit_be_to_u64(value_bytes);
        result.push(sub_identifier);
        value_bytes = &value_bytes[encoded_size ..];
    }

    Ok((result.into_boxed_slice(), total_length, &bytes[total_length..]))
}

pub fn decode_time(bytes:&[u8]) -> Result<(DateTime, usize, &[u8]), X509Error> {
    let (_, tag_complexity, tag_number, length, encoded_size) = decode_tag_and_length(bytes)?;
    let total_encoded_size = encoded_size + length;
    let data = &bytes[encoded_size .. total_encoded_size];
    if tag_complexity != TagComplexity::Primitive || data[length - 1] as char != 'Z' {
        return Err(X509ErrorKind::Decode.into());
    }
    let universal_type = UniversalType::from_u64(tag_number)?;
    let (year, data) = if universal_type == UniversalType::GeneralizedTime {
        (i64::from_str_radix(str::from_utf8(&data[..4])?, 10)?, &data[4..])
    }
    else if universal_type == UniversalType::UtcTime {
        let year = i64::from_str_radix(str::from_utf8(&data[..2])?, 10)?;
        let year = if year < 50 {
            2000 + year
        }
        else {
            1900 + year
        };
        (year, &data[2..])
    }
    else {
        return Err(X509ErrorKind::Decode.into());
    };

    let month = u8::from_str_radix(str::from_utf8(&data[..2])?, 10)?;
    let day = u8::from_str_radix(str::from_utf8(&data[2..4])?, 10)?;
    let hour = u8::from_str_radix(str::from_utf8(&data[4..6])?, 10)?;
    let minutes = u8::from_str_radix(str::from_utf8(&data[6..8])?, 10)?;
    let seconds = u8::from_str_radix(str::from_utf8(&data[8..10])?, 10)?;
    Ok((DateTime::new(year, month, day, hour, minutes, seconds), total_encoded_size, &bytes[total_encoded_size..]))
}

pub fn decode_bool(bytes:&[u8]) -> Result<(bool, usize, &[u8]), X509Error> {
    let (_, tag_complexity, tag_number, length, encoded_size) = decode_tag_and_length(bytes)?;
    if UniversalType::from_u64(tag_number)? != UniversalType::Boolean || tag_complexity != TagComplexity::Primitive || length != 1 {
        return Err(X509ErrorKind::Decode.into())
    }
    let total_encoded_size = encoded_size + length;
    let data = &bytes[encoded_size..];

    let result = match data[0] {
        0xffu8 => true,
        0x00u8 => false,
        _ => return Err(X509ErrorKind::Decode.into())
    };
    Ok((result, total_encoded_size, &bytes[total_encoded_size..]))
}

pub fn decode_null(bytes: &[u8]) -> Result<((), usize, &[u8]), X509Error> {
    let (_, tag_complexity, tag_number, length, encoded_size) = decode_tag_and_length(bytes)?;
    if UniversalType::from_u64(tag_number)? != UniversalType::Null || tag_complexity != TagComplexity::Primitive || length != 0 {
        return Err(X509ErrorKind::Decode.into())
    }

    let total_size = encoded_size + length;

    Ok(((), total_size, &bytes[total_size ..]))
}

pub fn uncover_explicit(bytes: &[u8], expected_tag_number:u64) -> Result<(&[u8], usize), X509Error> {
    let (tag_class, tag_complexity, tag_number, length, encoded_size) = decode_tag_and_length(bytes)?;
    if tag_class != TagClass::ContextSpecific || tag_complexity != TagComplexity::Constructed || tag_number != expected_tag_number {
        return Err(X509ErrorKind::Decode.into());
    }

    let total_encoded_size = encoded_size + length;
    let bytes = &bytes[encoded_size..];

    Ok((bytes, total_encoded_size))
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_encode_length_long_form() {
        assert_eq!(decode_length_long_form(&[0x81, 0x1]).unwrap(), (1, 2));
        assert_eq!(decode_length_long_form(&[0x82, 0x1, 0x0]).unwrap(), (256, 3));
        assert_eq!(decode_length_long_form(&[0x81, 0xc9]).unwrap(), (201, 2));
    }

    #[test]
    fn test_decode_integer() {
        let result = decode_integer(&[2, 1, 0]).unwrap();
        assert_eq!(result.0, 0);
        assert_eq!(result.1, 3);

        let result = decode_integer(&[2, 1, 0x7f]).unwrap();
        assert_eq!(result.0, 127);
        assert_eq!(result.1, 3);

        let result = decode_integer(&[2, 2, 0, 0x80]).unwrap();
        assert_eq!(result.0, 128);
        assert_eq!(result.1, 4);

        let result = decode_integer(&[2, 2, 1, 0]).unwrap();
        assert_eq!(result.0, 256);
        assert_eq!(result.1, 4);

        let result = decode_integer(&[2, 1, 0x80]).unwrap();
        assert_eq!(result.0, -128);
        assert_eq!(result.1, 3);

        let result = decode_integer(&[2, 2, 0xff, 0x7f]).unwrap();
        assert_eq!(result.0, -129);
        assert_eq!(result.1, 4);

        let result = decode_integer(&[2, 8, 0x80, 0, 0, 0, 0, 0, 0, 0]).unwrap();
        assert_eq!(result.0, i64::min_value());
        assert_eq!(result.1, 10);
    }

    #[test]
    fn test_encode_u64_7_bit() {
        assert_eq!(decode_7_bit_be_to_u64(&[0x81, 0x34]), (180, 2));
        assert_eq!(decode_7_bit_be_to_u64(&[0x0]), (0, 1));
        assert_eq!(decode_7_bit_be_to_u64(&[0x71]), (0x71, 1));
        assert_eq!(decode_7_bit_be_to_u64(&[0x82, 0x81, 0x0]), (0x8080, 3));
    }

    #[test]
    fn test_decode_time() {
        let date_time = DateTime {
            year: 2000,
            month: 1,
            day: 2,
            hours: 3,
            minutes: 4,
            seconds: 5
        };

        let mut result: Vec<u8> = Vec::with_capacity(20);
        result.push(23);
        result.push(13);
        result.extend_from_slice("000102030405Z".as_bytes());

        assert_eq!(decode_time(&result).unwrap(), (date_time, 15, &[] as &[u8]));

        let date_time = DateTime {
            year: 3000,
            month: 10,
            day: 2,
            hours: 23,
            minutes: 34,
            seconds: 45
        };

        let mut result: Vec<u8> = Vec::with_capacity(20);
        result.push(24);
        result.push(15);
        result.extend_from_slice("30001002233445Z".as_bytes());

        assert_eq!(decode_time(&result).unwrap(), (date_time, 17, &[] as &[u8]));
    }
}