read/validate/generate keys/certificates/primes


read-certificate -file filepath [--dump] [--verify] [--signer-certificate signer_certificate.cer]  -->  reads public certificate from file

read-prime -file filepath [--dump]

read-public-key -file filepath [--dump]

read-private-key [rsa|ecdsa] -file filepath [--dump]

dump-certificate --> dumps certificate in context to standard output

verify [--signer-certificate signer_certificate.cer] -->  verifies certificate in context

generate-prime --bits 1024 [--dump] [--file filepath]

generate-rsa-keys --bits 2048 [--primes-count 2] [--prime-files file1.prime file2.prime] [--file-pub-key savepublickey.pem] [--file-private-key saveprivatekey.pem]

generate-certificate [rsa|ecdsa] --json res/certificate.json [--public-key generated/pubkey.pem] [--private-key generated/privatekey.pem] --dump [--file saveto.cer] [--verify]

generate-ecdsa-keys --bits 256 [--file-pub-key savepublickey.pem] [--file-private-key saveprivatekey.pem]
